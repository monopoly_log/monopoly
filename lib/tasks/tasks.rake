# -*- encoding : utf-8 -*-

namespace :sms do

task :send => :environment do
  require 'digest/md5'
  require 'net/http'


  @sms = Sms.not_sent
  pass = Digest::MD5.hexdigest('79119887257').to_s.strip

  puts @sms.size
  

  @sms.each do |sms|
    File.open('log/report.sms', 'a'){ |file| file.write sms.sms_text; file.write "\n" }

    sleep(0.1)
    @number = sms.phone_number
    @text = sms.sms_text

    url = "http://mcommunicator.ru/m2m/m2m_api.asmx/SendMessage"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    parameters =  {"msid" => "#{@number}", "message" => "#{@text}", "naming" => "79857707575", "login" => "79119887257", "password" => "#{pass}"}
    request.set_form_data(parameters)
    response = http.request(request)

    content = response.body
    pos1 = content.index('</long>')
    num = content[pos1-8..pos1-1] if pos1
    sms.message_id = num
    sms.save
    puts @text
  end

puts Rails.env
puts "ok"
end

end


namespace :wialon do

  task :test => :environment do
    require 'net/http'
    require 'json'
    require 'date'

    time = Time.now
    puts "Wialon test start, #{time.to_s}"

    s_date = 1393279200
    e_date = 1393568760

    10.times do |i|
      resp = wialon_request_test 2, 3573842, s_date - i*1000, e_date + i*100
      #File.open('log/report.wialon', 'a'){ |file| file.write "#{i}"; file.write resp; file.write "\n" }
      puts "#{i}:  #{resp}" 
    end

    puts "Wialon test Ok, #{Time.now} - #{time}.to_s}"

  end


  task :can_report => :environment do
    require 'net/http'
    require 'json'
    require 'date'

    puts "Wialon start, #{Time.now.to_s}"

    resp = wialon_request('core/login',"{'user':'monopoly','password':'123456'}")
    @eid = resp['eid']

    yd = 1.day.ago

    s_date = DateTime.new(yd.year, yd.month, yd.day, 0, 0, 0).to_time.to_i
    e_date = DateTime.new(yd.year, yd.month, yd.day, 23, 59, 59).to_time.to_i

    core = 'core/search_items'
    par = '{"spec":{"itemsType":"avl_unit","propName":"sys_name","propValueMask":"*","sortType":""},"force":1,"flags":1,"from":0,"to":0}'
    
    resp = wialon_request core, par, @eid
    
    
    @ids = []
    resp["items"].each{|i| @ids << i["id"]}
    
    core = 'report/exec_report'

    #шаблон отчета 2
    @ids.each do |id|
      par = "{'reportResourceId':1222731,'reportTemplateId':2,'reportObjectId':#{id},'reportObjectSecId':1,'interval':{'from':#{s_date},'to':#{e_date},'flags':0}}"
      resp = wialon_request core, par, @eid
      Wialon.create({rec_data: resp, template: 2})
      #File.open('report.js', 'a'){ |file| file.write resp }
    end

    #шаблон отчета 1
    @ids.each do |id|
      par = "{'reportResourceId':1222731,'reportTemplateId':1,'reportObjectId':#{id},'reportObjectSecId':1,'interval':{'from':#{s_date},'to':#{e_date},'flags':0}}"
      resp = wialon_request core, par, @eid
      Wialon.create({rec_data: resp, template: 1})
    end

    #шаблон отчета 8
    @ids.each do |id|
      par = "{'reportResourceId':1222731,'reportTemplateId':8,'reportObjectId':#{id},'reportObjectSecId':1,'interval':{'from':#{s_date},'to':#{e_date},'flags':0}}"
      resp = wialon_request core, par, @eid
      Wialon.create({rec_data: resp, template: 8})
    end


    puts "Wialon Ok, #{Time.now.to_s}"

  end

end


def wialon_request svc, params=nil, sid=nil
  http = Net::HTTP.new("hst-api.wialon.com", 443)

  http.use_ssl = true
  http.verify_mode = OpenSSL::SSL::VERIFY_NONE

  request = Net::HTTP::Post.new("/wialon/ajax.html?svc=#{svc}#{params ? '&params='+params : nil}&sid=#{sid}")
  #http.set_debug_output($stdout)

  request["Content-Type"] = "application/x-www-form-urlencoded"

  response = http.request(request)
  JSON.parse(response.body)
end


def wialon_request_test reportTemplateId, reportObjectId, from, to

  http = Net::HTTP.new("192.168.0.15", 80)

  request = Net::HTTP::Get.new("/wialon?reportTemplateId=#{reportTemplateId}&reportObjectId=#{reportObjectId}&from=#{from}&to=#{to}")
  #http.set_debug_output($stdout)
  response = http.request(request)
  response.body
end