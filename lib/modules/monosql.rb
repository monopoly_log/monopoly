# -*- encoding : utf-8 -*-

require 'activerecord-sqlserver-adapter'
class Monosql < ActiveRecord::Base
  
#ActiveRecord::Base.default_timezone = :local

  self.abstract_class = true
  establish_connection configurations["monosql"]

  def self.get_ts
     connection.select_all "exec [MonopolySun].[dbo].[ms_Transport_Get_ForMS]"
  end


  
end
