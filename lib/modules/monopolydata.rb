# -*- encoding : utf-8 -*-

require 'activerecord-sqlserver-adapter'
class Monopolydata < ActiveRecord::Base

  self.abstract_class = true
  establish_connection configurations["monopolydata"]
end
