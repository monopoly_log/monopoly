# -*- encoding : utf-8 -*-

require 'activerecord-sqlserver-adapter'
class Wialondata < ActiveRecord::Base

  self.abstract_class = true
  establish_connection configurations["wialondata"]
end
