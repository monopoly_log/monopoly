# -*- encoding : utf-8 -*-
class CreateEnginetypes < ActiveRecord::Migration
  def change
    create_table :enginetypes, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.string :name
      t.boolean :is_close, default: false
      t.timestamps
    end
  end
end
