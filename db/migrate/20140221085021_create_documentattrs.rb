# -*- encoding : utf-8 -*-
class CreateDocumentattrs < ActiveRecord::Migration
  def change
    create_table :documentattrs, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.boolean :series
      t.boolean :number
      t.boolean :category
      t.boolean :registration
      t.boolean :issued_by
      t.boolean :issued_date
      t.boolean :start_date
      t.boolean :end_date
      t.uuid :documentkind_id
      t.timestamps
    end
  end
end
