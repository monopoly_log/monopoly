# -*- encoding : utf-8 -*-
class CreateObjecttypepropertycolumns < ActiveRecord::Migration
  def change
    create_table :objecttypepropertycolumns do |t|
      t.uuid :id, null: false, primary_key: true
      t.uuid :objecttypeproperty_id
      t.uuid :column_id
      t.date :start
      t.date :end
      t.boolean :is_close

      t.timestamps
    end
  end
end
