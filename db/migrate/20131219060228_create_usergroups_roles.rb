# -*- encoding : utf-8 -*-
class CreateUsergroupsRoles < ActiveRecord::Migration
  def change
    create_table :roles_usergroups, id: false do |t|
      t.uuid :usergroup_id
      t.uuid :role_id

    end
  end
end
