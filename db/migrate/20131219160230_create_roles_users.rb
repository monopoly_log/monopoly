# -*- encoding : utf-8 -*-
class CreateRolesUsers < ActiveRecord::Migration
  def change
    create_table :roles_users, id: false do |t|
      t.uniqueidentifier :user_id
      t.uniqueidentifier :role_id

    end
  end
end
