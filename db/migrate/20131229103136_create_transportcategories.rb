# -*- encoding : utf-8 -*-
class CreateTransportcategories < ActiveRecord::Migration
  def change
    create_table :transportcategories, id: false do |t|
      t.uuid :id, null: false, :primary_key => true
      t.string :name
      t.descr :text
      t.boolean :is_close, default: false
      t.timestamps
    end
  end
end
