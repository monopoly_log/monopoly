# -*- encoding : utf-8 -*-
class CreateWialons < ActiveRecord::Migration
  def change
    create_table :wialons do |t|
      t.integer :template
      t.text :rec_data

      t.timestamps
    end
  end
end
