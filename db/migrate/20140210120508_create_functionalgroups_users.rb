# -*- encoding : utf-8 -*-
class CreateFunctionalgroupsUsers < ActiveRecord::Migration
  def change
    create_table :functionalgroups_users, id: false do |t|
      t.uuid :functionalgroup_id
      t.uuid :user_id
    end
  end
end
