# -*- encoding : utf-8 -*-
class CreateTables < ActiveRecord::Migration
  def change
    create_table :tables, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.string :name
      t.text :description
      t.date :start
      t.date :end
      t.boolean :is_close

      t.timestamps
    end
  end
end
