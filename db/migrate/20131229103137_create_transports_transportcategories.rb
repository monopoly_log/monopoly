# -*- encoding : utf-8 -*-
class CreateTransportsTransportcategories < ActiveRecord::Migration
  def change
    create_table :transportcategories_transports, id: false do |t|
      t.uuid :transport_id
      t.uuid :transportcategory_id

    end
end
end
