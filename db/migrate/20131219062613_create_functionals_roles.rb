# -*- encoding : utf-8 -*-
class CreateFunctionalsRoles < ActiveRecord::Migration
  def change
    create_table :functionals_roles , id: false do |t|
      t.uniqueidentifier :functional_id
      t.uniqueidentifier :role_id

    end
  end
end
