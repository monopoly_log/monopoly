# -*- encoding : utf-8 -*-
class CreateTransporttanks < ActiveRecord::Migration
  def change
    create_table :transporttanks, id: false  do |t|
      t.uuid :id, null: false, primary_key: true
      t.integer :v_tank
      t.integer :v_tank_min
      t.uuid :transportmodel_id
      t.boolean :is_close, default: false
      t.timestamps
    end
  end
end
