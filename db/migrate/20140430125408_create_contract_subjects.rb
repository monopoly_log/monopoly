class CreateContractSubjects < ActiveRecord::Migration
  def change
    create_table :contract_subjects, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.uuid :contract_type_id
      t.string :name
      t.text :descr
      t.boolean :is_close

      t.timestamps
    end
  end
end
