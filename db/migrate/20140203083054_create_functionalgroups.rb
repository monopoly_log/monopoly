# -*- encoding : utf-8 -*-
class CreateFunctionalgroups < ActiveRecord::Migration
  def change
    create_table :functionalgroups, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.string :name
      t.text :descr
      t.string :part
      t.string :link
      t.uuid :parent_id
      t.uuid :lft
      t.uuid :rgt
      t.boolean :is_close
      t.timestamps
    end
  end
end
