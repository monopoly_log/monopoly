# -*- encoding : utf-8 -*-
class CreateEnginemodels < ActiveRecord::Migration
  def change
    create_table :enginemodels, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.string :name
      t.numeric :power, precision: 18, scale: 2
      t.numeric :power_kvt, precision: 18, scale: 2
      t.numeric :displacement, precision: 18, scale: 2
      t.uuid :enginetype_id
      t.boolean :is_close, default: false
      t.timestamps
    end
  end
end
