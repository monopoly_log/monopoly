class CreateAgrees < ActiveRecord::Migration
  def change
    create_table :agrees, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.text :note
      t.uuid :user_id
      t.uuid :OrderFromKey
      t.uuid :OrderToKey

      t.timestamps
    end
  end
end
