class CreateContractorRepresentatives < ActiveRecord::Migration
  def change
    create_table :contractor_representatives, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.uuid :person_id
      t.uuid :contractor_id
      t.uuid :position_id
      t.date :start_date
      t.date :end_date
      t.boolean :is_close

      t.timestamps
    end
  end
end
