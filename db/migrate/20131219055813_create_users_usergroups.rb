# -*- encoding : utf-8 -*-
class CreateUsersUsergroups < ActiveRecord::Migration
  def change
    create_table :usergroups_users, id: false do |t|
      t.uuid :usergroup_id
      t.uuid :user_id
    end
  end
end
