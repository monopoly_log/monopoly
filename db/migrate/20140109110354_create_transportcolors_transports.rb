# -*- encoding : utf-8 -*-
class CreateTransportcolorsTransports < ActiveRecord::Migration
  def change
    create_table :transportcolors_transports, id: false do |t|
      t.uuid :transport_id
      t.uuid :transportcolor_id
    end
  end
end
