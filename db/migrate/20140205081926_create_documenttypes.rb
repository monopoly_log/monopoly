# -*- encoding : utf-8 -*-
class CreateDocumenttypes < ActiveRecord::Migration
  def change
    create_table :documenttypes, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.string :name
      t.text :descr
      t.boolean :is_close, default: false

      t.timestamps
    end
  end
end
