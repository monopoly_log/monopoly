# -*- encoding : utf-8 -*-
class CreatePhoneuses < ActiveRecord::Migration
  def change
    create_table :phoneuses, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.uuid :phone_id
      t.uuid :person_id
      t.boolean :is_corporate
      t.date :issue_date
      t.date :take_date
      t.boolean :is_close

      t.timestamps
    end
  end
end
