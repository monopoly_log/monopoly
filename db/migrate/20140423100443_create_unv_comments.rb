class CreateUnvComments < ActiveRecord::Migration
  def change
    create_table :unv_comments, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.text :comment
      t.uuid :user_id
      t.uuid :OrderFromKey
      t.uuid :OrderToKey

      t.timestamps
    end
  end
end
