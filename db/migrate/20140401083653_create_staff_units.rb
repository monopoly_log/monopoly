class CreateStaffUnits < ActiveRecord::Migration
  def change
    create_table :staff_units, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.boolean :head
      t.uuid :department_id
      t.uuid :position_id
      t.text :descr
      t.boolean :is_close
      t.integer :lft
      t.integer :rgt
      t.uuid :parent_id
      t.timestamps
    end
  end
end
