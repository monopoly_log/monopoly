# -*- encoding : utf-8 -*-
class CreateTransportsTransportbrands < ActiveRecord::Migration
  def change
    create_table :transportbrands_transports, id: false do |t|
      t.uuid :transport_id
      t.uuid :transportbrand_id
    end
  end
end
