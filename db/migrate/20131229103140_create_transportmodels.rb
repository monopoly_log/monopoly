# -*- encoding : utf-8 -*-
class CreateTransportmodels < ActiveRecord::Migration
  def change
    create_table :transportmodels, id: false do |t|
      t.uuid :id, null: false, :primary_key => true
      t.uuid :transportbrand_id
      t.string :name
      t.numeric :weight, precision: 16, scale: 2
      t.numeric :capacity, precision: 16, scale: 2
      t.numeric :overall_height, precision: 16, scale: 2
      t.numeric :overall_width, precision: 16, scale: 2
      t.numeric :overall_length, precision: 16, scale: 2
      t.numeric :useful_height, precision: 16, scale: 2
      t.numeric :useful_width, precision: 16, scale: 2
      t.numeric :useful_length, precision: 16, scale: 2
      t.numeric :useful_volume, precision: 16, scale: 2
      t.boolean :is_engine
      t.boolean :is_loading
      t.boolean :is_fuelling
      t.boolean :is_close, default: false
      t.timestamps
    end
  end
end
