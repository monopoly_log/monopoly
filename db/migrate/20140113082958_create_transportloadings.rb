# -*- encoding : utf-8 -*-
class CreateTransportloadings < ActiveRecord::Migration
  def change
    create_table :transportloadings, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.string :loading
      t.text :descr
      t.boolean :is_loading
      t.boolean :is_close, default: false
      t.timestamps
    end
  end
end
