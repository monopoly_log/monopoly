# -*- encoding : utf-8 -*-
class CreateSms < ActiveRecord::Migration
  def change
    create_table :sms do |t|
      t.string :phone_number
      t.text :sms_text
      t.string :message_id
      t.timestamps
    end
  end
end
