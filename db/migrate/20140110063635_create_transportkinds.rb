# -*- encoding : utf-8 -*-
class CreateTransportkinds < ActiveRecord::Migration
  def change
    create_table :transportkinds, id: false do |t|
      t.uuid :id, null: false, :primary_key => true
      t.string :name
      t.text :descr
      t.uuid :transporttype_id
      t.boolean :is_close, default: false
      t.timestamps
    end
  end
end
