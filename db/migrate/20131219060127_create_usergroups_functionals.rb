# -*- encoding : utf-8 -*-
class CreateUsergroupsFunctionals < ActiveRecord::Migration
  def change
    create_table :functionals_usergroups, id: false do |t|
      t.uuid :usergroup_id
      t.uuid :functional_id

    end
  end
end
