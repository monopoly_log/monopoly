# -*- encoding : utf-8 -*-
class CreateFunctionals < ActiveRecord::Migration
  def change
    create_table :functionals, id: false do |t|
      t.uuid  :id, null: false, :primary_key => true
      t.string :name
      t.text :description
      t.string :part
      t.date :start
      t.date :end
      t.boolean :is_close, default: false
      t.timestamps
    end
  end
end
