# -*- encoding : utf-8 -*-
class CreateUsergroups < ActiveRecord::Migration
  def change
    create_table :usergroups, id: false do |t|
      t.uuid  :id, null: false, :primary_key => true
      t.string :name
      t.text :description
      t.date :start_time
      t.date :end_time
      t.boolean :is_close, default: false
      t.timestamps
    end
  end
end
