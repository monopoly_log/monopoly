# -*- encoding : utf-8 -*-
class CreateTstanks < ActiveRecord::Migration
  def change
    create_table :tstanks, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.uuid :transport_id
      t.integer :v_tank
      t.integer :v_tank_min
      t.boolean :is_close

      t.timestamps
    end
  end
end
