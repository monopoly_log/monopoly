# -*- encoding : utf-8 -*-
class CreateDocumentsTransports < ActiveRecord::Migration
  def change
    create_table :documents_transports, id: false do |t|
      t.uuid :document_id
      t.uuid :transport_id
    end
  end
end
