# -*- encoding : utf-8 -*-
class CreateObjecttypeproperties < ActiveRecord::Migration
  def change
    create_table :objecttypeproperties do |t|
      t.uuid :id, null: false, primary_key: true
      t.uuid :objecttype_id
      t.uuid :system_id
      t.string :name
      t.text :description
      t.date :start
      t.date :end
      t.boolean :is_close

      t.timestamps
    end
  end
end
