# -*- encoding : utf-8 -*-
class CreateTransportbrands < ActiveRecord::Migration
  def change
    create_table :transportbrands, id: false do |t|
      t.uuid :id, null: false, :primary_key => true
      t.string :name
      t.boolean :is_close, default: false
      t.timestamps
    end
  end
end
