class CreateCouplers < ActiveRecord::Migration
  def change
    create_table :couplers, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.uuid :truck_id
      t.uuid :trailer_id
      t.datetime :start_date
      t.datetime :end_date
      t.boolean :is_close
      t.timestamps
    end
  end
end
