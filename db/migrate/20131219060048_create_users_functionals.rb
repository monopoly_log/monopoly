# -*- encoding : utf-8 -*-
class CreateUsersFunctionals < ActiveRecord::Migration
  def change
    create_table :functionals_users, id: false do |t|
      t.uuid :user_id
      t.uuid :functional_id
    end
  end
end
