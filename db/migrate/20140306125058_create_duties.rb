# -*- encoding : utf-8 -*-
class CreateDuties < ActiveRecord::Migration
  def change
    create_table :msSubscriberTemp do |t|
      t.uuid :id
      t.datetime :MomentFrom
      t.datetime :MomentTo
      t.boolean :IsLog
      t.boolean :IsMan
      t.uuid :EmployeeKey
      t.uuid :user_id
      t.datetime :created_at

    end
  end
end
