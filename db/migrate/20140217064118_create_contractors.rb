# -*- encoding : utf-8 -*-
class CreateContractors < ActiveRecord::Migration
  def change
    create_table :contractors, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.uuid :parent_id
      t.integer :lft
      t.integer :rgt
      t.uuid :contractortype_id
      t.buulean :is_legal
      t.string :name
      t.integer :inn
      t.string :kpp
      t.date :start_date
      t.date :end_date
      t.boolean :is_close
      t.timestamps
    end
  end
end
