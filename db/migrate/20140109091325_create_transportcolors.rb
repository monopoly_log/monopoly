# -*- encoding : utf-8 -*-
class CreateTransportcolors < ActiveRecord::Migration
  def change
    create_table :transportcolors do |t|
      t.string :name
      t.boolean :is_close, default: false
      t.timestamps
    end
  end
end
