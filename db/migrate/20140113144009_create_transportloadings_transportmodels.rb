# -*- encoding : utf-8 -*-
class CreateTransportloadingsTransportmodels < ActiveRecord::Migration
  def change
    create_table :transportloadings_transportmodels, id: false do |t|
      t.uuid :transportloading_id
      t.uuid :transportmodel_id
    end
  end
end
