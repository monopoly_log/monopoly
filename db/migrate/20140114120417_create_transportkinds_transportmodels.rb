# -*- encoding : utf-8 -*-
class CreateTransportkindsTransportmodels < ActiveRecord::Migration
  def change
    create_table :transportkinds_transportmodels, id: false do |t|
      t.uuid :transportkind_id
      t.uuid :transportmodel_id
    end
  end
end
