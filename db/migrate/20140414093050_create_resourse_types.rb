class CreateResourseTypes < ActiveRecord::Migration
  def change
    create_table :resourse_types, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.string :name
      t.text :descr

      t.timestamps
    end
  end
end
