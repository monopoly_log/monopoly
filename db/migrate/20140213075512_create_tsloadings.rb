# -*- encoding : utf-8 -*-
class CreateTsloadings < ActiveRecord::Migration
  def change
    create_table :tsloadings, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.uuid :transport_id
      t.string :loading
      t.boolean :is_close

      t.timestamps
    end
  end
end
