# -*- encoding : utf-8 -*-
class DocumentsPeople < ActiveRecord::Migration
  def change
    create_table :documents_people, id: false do |t|
      t.uuid :document_id
      t.uuid :person_id
    end
  end
end
