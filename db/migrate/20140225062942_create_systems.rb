# -*- encoding : utf-8 -*-
class CreateSystems < ActiveRecord::Migration
  def change
    create_table :systems do |t|
      t.uuid :id, null: false, primary_key: true
      t.string :name
      t.text :descr
      t.date :start
      t.date :end
      t.boolean :is_close

      t.timestamps
    end
  end
end
