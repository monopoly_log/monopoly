# -*- encoding : utf-8 -*-
class CreateDocumentkinds < ActiveRecord::Migration
  def change
    create_table :documentkinds, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.uuid :documenttype_id
      t.string :name
      t.text :descr
      t.boolean :is_needed, default: false
      t.boolean :is_close, default: false

      t.timestamps
    end
  end
end
