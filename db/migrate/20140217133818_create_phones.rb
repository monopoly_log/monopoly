# -*- encoding : utf-8 -*-
class CreatePhones < ActiveRecord::Migration
  def change
    create_table :phones, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.string :phone
      t.date :start_date
      t.date :end_date
      t.boolean :is_close

      t.timestamps
    end
  end
end
