# -*- encoding : utf-8 -*-
class CreateEnginetypesTransportmodels < ActiveRecord::Migration
  def change
    create_table :enginetypes_transportmodels, id: false do |t|
      t.uuid :enginetype_id
      t.uuid :transportmodel_id
    end
  end
end
