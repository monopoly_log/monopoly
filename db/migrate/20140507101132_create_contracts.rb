class CreateContracts < ActiveRecord::Migration
  def change
    create_table :contracts, id: false do |t|
      t.uuid :id, primary_key: true, null: false
      t.uuid :contract_subject_select_id
      t.string :contract_number
      t.date :start_date
      t.date :end_date
      t.boolean :is_close

      t.timestamps
    end
  end
end
