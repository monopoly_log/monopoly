# -*- encoding : utf-8 -*-
class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.string :name
      t.string :surname
      t.string :last_name
      t.string :mail
      t.boolean :gender, default: true
      t.date :start_date
      t.date :end_date
      t.date :birthdate
      t.text :description
      t.boolean :is_close, default: false
      t.timestamps
    end
  end
end
