# -*- encoding : utf-8 -*-
class CreateTestguids < ActiveRecord::Migration
  def change
    create_table :testguids, :id => false do |t|
      t.uuid :id, :primary_key => true
      t.string :str

      t.timestamps
    end
  end
end
