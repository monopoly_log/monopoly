# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140508071102) do

  create_table "Transport_From1C", :id => false, :force => true do |t|
    t.string   "Key",              :limit => nil
    t.string   "Category",         :limit => 200
    t.string   "Type",             :limit => 200
    t.string   "Group",            :limit => 200
    t.string   "Model",            :limit => 200
    t.string   "Color",            :limit => 200
    t.string   "EcoClass",         :limit => 200
    t.string   "ExploitationType", :limit => 200
    t.string   "OrganizationKey",  :limit => nil
    t.string   "RegNumber",        :limit => 200
    t.string   "VinNumber",        :limit => 200
    t.string   "BodyNumber",       :limit => 200
    t.string   "ChassisNumber",    :limit => 200
    t.decimal  "TankVolume",                      :precision => 18, :scale => 2
    t.integer  "StartYear"
    t.decimal  "Weight",                          :precision => 18, :scale => 2
    t.decimal  "Capacity",                        :precision => 18, :scale => 2
    t.decimal  "GabHeight",                       :precision => 18, :scale => 2
    t.decimal  "GabWidth",                        :precision => 18, :scale => 2
    t.decimal  "GabLen",                          :precision => 18, :scale => 2
    t.decimal  "Height",                          :precision => 18, :scale => 2
    t.decimal  "Width",                           :precision => 18, :scale => 2
    t.decimal  "Len",                             :precision => 18, :scale => 2
    t.decimal  "Volume",                          :precision => 18, :scale => 2
    t.string   "TypeLoading",      :limit => 200
    t.string   "EngineNumber",     :limit => 200
    t.string   "EngineType",       :limit => 200
    t.string   "EngineModel",      :limit => 200
    t.decimal  "EnginePowerLS",                   :precision => 18, :scale => 2
    t.decimal  "EnginePowerKWT",                  :precision => 18, :scale => 2
    t.decimal  "EngineVolume",                    :precision => 18, :scale => 2
    t.datetime "Moment"
  end

  create_table "_temp_sms_for_send", :force => true do |t|
    t.string "phone_number"
    t.text   "sms_text"
    t.string "message_id"
  end

  create_table "_temp_sms_for_send_2014-1", :force => true do |t|
    t.string "phone_number"
    t.text   "sms_text"
    t.string "message_id"
  end

  create_table "admins", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0,  :null => false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "admins", ["email"], :name => "index_admins_on_email", :unique => true

  create_table "agrees", :id => false, :force => true do |t|
    t.string   "id",           :limit => nil, :null => false
    t.text     "note"
    t.string   "user_id",      :limit => nil
    t.string   "OrderFromKey", :limit => nil
    t.string   "OrderToKey",   :limit => nil
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "audits", :force => true do |t|
    t.integer  "auditable_id"
    t.string   "auditable_type"
    t.integer  "associated_id"
    t.string   "associated_type"
    t.integer  "user_id"
    t.string   "user_type"
    t.string   "username"
    t.string   "action"
    t.text     "audited_changes"
    t.integer  "version",         :default => 0
    t.string   "comment"
    t.string   "remote_address"
    t.datetime "created_at"
  end

  add_index "audits", ["associated_id", "associated_type"], :name => "associated_index"
  add_index "audits", ["auditable_id", "auditable_type"], :name => "auditable_index"
  add_index "audits", ["created_at"], :name => "index_audits_on_created_at"
  add_index "audits", ["user_id", "user_type"], :name => "user_index"

  create_table "contract_relationships", :id => false, :force => true do |t|
    t.binary   "id",                       :null => false
    t.binary   "contract_id"
    t.binary   "contract_subject_role_id"
    t.binary   "contractor_id"
    t.boolean  "is_close"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "contract_subject_roles", :id => false, :force => true do |t|
    t.string   "id",                  :limit => nil, :null => false
    t.string   "contract_subject_id", :limit => nil
    t.string   "name"
    t.text     "descr"
    t.boolean  "is_close"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
  end

  create_table "contract_subject_selects", :id => false, :force => true do |t|
    t.string   "id",                  :limit => nil, :null => false
    t.string   "contract_subject_id", :limit => nil
    t.string   "name"
    t.text     "descr"
    t.boolean  "is_close"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
  end

  create_table "contract_subjects", :id => false, :force => true do |t|
    t.string   "id",               :limit => nil, :null => false
    t.string   "contract_type_id", :limit => nil
    t.string   "name"
    t.text     "descr"
    t.boolean  "is_close"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  create_table "contract_types", :id => false, :force => true do |t|
    t.string   "id",         :limit => nil, :null => false
    t.string   "name"
    t.text     "descr"
    t.boolean  "is_close"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "contractor_representatives", :id => false, :force => true do |t|
    t.string   "id",            :limit => nil, :null => false
    t.string   "person_id",     :limit => nil
    t.string   "contractor_id", :limit => nil
    t.string   "position_id",   :limit => nil
    t.date     "start_date"
    t.date     "end_date"
    t.boolean  "is_close"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "contractors", :id => false, :force => true do |t|
    t.string   "id",                :limit => nil,                    :null => false
    t.string   "parent_id",         :limit => nil
    t.integer  "lft"
    t.integer  "rgt"
    t.string   "contractortype_id", :limit => nil
    t.boolean  "is_legal"
    t.string   "name"
    t.integer  "inn"
    t.string   "kpp"
    t.date     "start_date"
    t.date     "end_date"
    t.boolean  "is_close",                         :default => false
    t.datetime "created_at",                                          :null => false
    t.datetime "updated_at",                                          :null => false
  end

  create_table "contractors_people", :id => false, :force => true do |t|
    t.string "contractor_id", :limit => nil
    t.string "person_id",     :limit => nil
  end

  create_table "contractortypes", :id => false, :force => true do |t|
    t.string   "id",         :limit => nil,                    :null => false
    t.string   "name"
    t.text     "descr"
    t.boolean  "is_close",                  :default => false
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
  end

  create_table "contracts", :id => false, :force => true do |t|
    t.binary   "id",                         :null => false
    t.binary   "contract_subject_select_id"
    t.string   "contract_number"
    t.date     "start_date"
    t.date     "end_date"
    t.boolean  "is_close"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "couplers", :id => false, :force => true do |t|
    t.string   "id",         :limit => nil, :null => false
    t.string   "truck_id",   :limit => nil
    t.string   "trailer_id", :limit => nil
    t.datetime "start_date"
    t.datetime "end_date"
    t.boolean  "is_close"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "departments", :id => false, :force => true do |t|
    t.string   "id",            :limit => nil,                    :null => false
    t.string   "parent_id",     :limit => nil
    t.integer  "rgt"
    t.integer  "lft"
    t.string   "contractor_id", :limit => nil
    t.string   "name"
    t.text     "descr"
    t.boolean  "is_close",                     :default => false
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
  end

  create_table "documentattrs", :id => false, :force => true do |t|
    t.string   "id",              :limit => nil,                   :null => false
    t.string   "documentkind_id", :limit => nil,                   :null => false
    t.boolean  "series",                         :default => true, :null => false
    t.boolean  "number",                         :default => true, :null => false
    t.boolean  "category",                       :default => true, :null => false
    t.boolean  "registration",                   :default => true, :null => false
    t.boolean  "issued_by",                      :default => true, :null => false
    t.boolean  "issued_date",                    :default => true, :null => false
    t.boolean  "start_date",                     :default => true, :null => false
    t.boolean  "end_date",                       :default => true, :null => false
    t.datetime "created_at",                                       :null => false
    t.datetime "updated_at",                                       :null => false
  end

  create_table "documentfiles", :id => false, :force => true do |t|
    t.string   "id",          :limit => nil,                    :null => false
    t.string   "document_id", :limit => nil
    t.string   "fname"
    t.string   "descr"
    t.boolean  "is_close",                   :default => false
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
  end

  create_table "documentkinds", :id => false, :force => true do |t|
    t.string   "id",              :limit => nil, :null => false
    t.string   "documenttype_id", :limit => nil
    t.string   "name"
    t.text     "descr"
    t.boolean  "is_needed"
    t.boolean  "is_close"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
  end

  create_table "documents", :id => false, :force => true do |t|
    t.string   "id",              :limit => nil,                    :null => false
    t.string   "parent_id",       :limit => nil
    t.string   "documentkind_id", :limit => nil
    t.string   "contract_id",     :limit => nil
    t.string   "contractor_id",   :limit => nil
    t.string   "series"
    t.string   "number"
    t.string   "category"
    t.string   "registration"
    t.string   "issued_by"
    t.date     "issued_date"
    t.date     "start_date"
    t.date     "end_date"
    t.boolean  "is_close",                       :default => false
    t.integer  "lft"
    t.integer  "rgt"
    t.string   "type",            :limit => 50
    t.datetime "created_at",                                        :null => false
    t.datetime "updated_at",                                        :null => false
  end

  create_table "documents_people", :id => false, :force => true do |t|
    t.string "document_id", :limit => nil
    t.string "person_id",   :limit => nil
  end

  create_table "documents_transports", :id => false, :force => true do |t|
    t.string "document_id",  :limit => nil
    t.string "transport_id", :limit => nil
  end

  create_table "documenttypes", :id => false, :force => true do |t|
    t.string   "id",         :limit => nil,                    :null => false
    t.string   "name"
    t.text     "descr"
    t.boolean  "is_close",                  :default => false
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
  end

  create_table "employee_histories", :id => false, :force => true do |t|
    t.string   "id",              :limit => nil, :null => false
    t.string   "employee_id",     :limit => nil
    t.date     "employment_date"
    t.date     "leaving_date"
    t.date     "start_date"
    t.date     "end_date"
    t.string   "contractor_id",   :limit => nil
    t.string   "contractor_name"
    t.string   "department_id",   :limit => nil
    t.string   "department_name"
    t.string   "position_id",     :limit => nil
    t.string   "position_name"
    t.string   "person_id",       :limit => nil
    t.string   "staff_unit_id",   :limit => nil
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
  end

  create_table "employees", :id => false, :force => true do |t|
    t.string   "id",              :limit => nil, :null => false
    t.string   "person_id",       :limit => nil
    t.string   "staff_unit_id",   :limit => nil
    t.date     "employment_date"
    t.date     "leaving_date"
    t.date     "start_date"
    t.date     "end_date"
    t.boolean  "is_close"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
  end

  create_table "enginemodels", :id => false, :force => true do |t|
    t.string   "id",            :limit => nil,                                                   :null => false
    t.string   "name"
    t.decimal  "power",                        :precision => 18, :scale => 2
    t.decimal  "power_kvt",                    :precision => 18, :scale => 2
    t.decimal  "displacement",                 :precision => 18, :scale => 2
    t.string   "enginetype_id", :limit => nil,                                                   :null => false
    t.boolean  "is_close",                                                    :default => false, :null => false
    t.datetime "created_at",                                                                     :null => false
    t.datetime "updated_at",                                                                     :null => false
  end

  create_table "enginemodels_transports", :id => false, :force => true do |t|
    t.string "enginemodel_id", :limit => nil
    t.string "transport_id",   :limit => nil
  end

  create_table "enginetypes", :id => false, :force => true do |t|
    t.string   "id",         :limit => nil,                    :null => false
    t.string   "name"
    t.boolean  "is_close",                  :default => false, :null => false
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
  end

  create_table "enginetypes_transportmodels", :id => false, :force => true do |t|
    t.string "enginetype_id",     :limit => nil
    t.string "transportmodel_id", :limit => nil
  end

  create_table "functionalgroups", :id => false, :force => true do |t|
    t.string   "id",         :limit => nil,                    :null => false
    t.string   "name"
    t.text     "descr"
    t.string   "part"
    t.string   "link"
    t.string   "parent_id",  :limit => nil
    t.integer  "lft"
    t.integer  "rgt"
    t.boolean  "is_close",                  :default => false, :null => false
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
  end

  create_table "functionalgroups_roles", :id => false, :force => true do |t|
    t.string "functionalgroup_id", :limit => nil
    t.string "role_id",            :limit => nil
  end

  create_table "functionalgroups_users", :id => false, :force => true do |t|
    t.string "functionalgroup_id", :limit => nil
    t.string "user_id",            :limit => nil
  end

  create_table "functionals", :id => false, :force => true do |t|
    t.string   "id",          :limit => nil,                    :null => false
    t.string   "name"
    t.text     "description"
    t.string   "part"
    t.datetime "start"
    t.datetime "end"
    t.boolean  "is_close",                   :default => false, :null => false
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
  end

  create_table "functionals_roles", :id => false, :force => true do |t|
    t.string "functional_id", :limit => nil
    t.string "role_id",       :limit => nil
  end

  create_table "functionals_usergroups", :id => false, :force => true do |t|
    t.string "usergroup_id",  :limit => nil
    t.string "functional_id", :limit => nil
  end

  create_table "functionals_users", :id => false, :force => true do |t|
    t.string "functional_id", :limit => nil
    t.string "user_id",       :limit => nil
  end

  create_table "msSubscriberTemp", :force => true do |t|
    t.datetime "MomentFrom"
    t.datetime "MomentTo"
    t.boolean  "IsLog"
    t.boolean  "IsMan"
    t.binary   "EmployeeKey"
    t.binary   "user_id"
    t.datetime "created_at"
  end

  create_table "people", :id => false, :force => true do |t|
    t.string   "id",          :limit => nil,                    :null => false
    t.string   "name"
    t.string   "surname"
    t.string   "last_name"
    t.date     "birthdate"
    t.string   "mail"
    t.boolean  "gender"
    t.date     "start_date"
    t.date     "end_date"
    t.boolean  "is_close",                   :default => false, :null => false
    t.text     "description"
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
  end

  create_table "phones", :id => false, :force => true do |t|
    t.string   "id",         :limit => nil,                    :null => false
    t.string   "phone"
    t.date     "start_date"
    t.date     "end_date"
    t.boolean  "is_close",                  :default => false
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
  end

  create_table "phoneuses", :id => false, :force => true do |t|
    t.string   "id",           :limit => nil,                    :null => false
    t.string   "phone_id",     :limit => nil
    t.string   "person_id",    :limit => nil
    t.boolean  "is_corporate"
    t.date     "issue_date"
    t.date     "take_date"
    t.boolean  "is_close",                    :default => false
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
  end

  create_table "positions", :id => false, :force => true do |t|
    t.string   "id",         :limit => nil,                    :null => false
    t.string   "name"
    t.text     "descr"
    t.boolean  "is_close",                  :default => false
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at"
  end

  create_table "resourse_types", :id => false, :force => true do |t|
    t.string   "id",         :limit => nil, :null => false
    t.string   "name"
    t.text     "descr"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "roles", :id => false, :force => true do |t|
    t.string   "id",         :limit => nil,                    :null => false
    t.string   "name"
    t.text     "descr"
    t.boolean  "is_close",                  :default => false, :null => false
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
  end

  create_table "roles_usergroups", :id => false, :force => true do |t|
    t.string "usergroup_id", :limit => nil
    t.string "role_id",      :limit => nil
  end

  create_table "roles_users", :id => false, :force => true do |t|
    t.string "user_id", :limit => nil
    t.string "role_id", :limit => nil
  end

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "sms", :force => true do |t|
    t.string   "phone_number"
    t.text     "sms_text"
    t.string   "message_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "staff_units", :id => false, :force => true do |t|
    t.string   "id",            :limit => nil, :null => false
    t.boolean  "head"
    t.string   "department_id", :limit => nil
    t.string   "position_id",   :limit => nil
    t.text     "descr"
    t.boolean  "is_close"
    t.integer  "lft"
    t.integer  "rgt"
    t.string   "parent_id",     :limit => nil
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "sysdiagrams", :primary_key => "diagram_id", :force => true do |t|
    t.string  "name",         :limit => 128, :null => false
    t.integer "principal_id",                :null => false
    t.integer "version"
    t.binary  "definition"
  end

  add_index "sysdiagrams", ["principal_id", "name"], :name => "UK_principal_name", :unique => true

  create_table "temps", :id => false, :force => true do |t|
    t.string   "id",         :limit => nil, :null => false
    t.integer  "num1"
    t.string   "str1"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "testguids", :id => false, :force => true do |t|
    t.string   "id",         :limit => nil, :null => false
    t.string   "str"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "transportbrands", :id => false, :force => true do |t|
    t.string   "id",         :limit => nil,                    :null => false
    t.string   "name"
    t.boolean  "is_close",                  :default => false, :null => false
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
  end

  create_table "transportbrands_transports", :id => false, :force => true do |t|
    t.string "transport_id",      :limit => nil
    t.string "transportbrand_id", :limit => nil
  end

  create_table "transportcategories", :id => false, :force => true do |t|
    t.string   "id",         :limit => nil,                    :null => false
    t.string   "name"
    t.text     "descr"
    t.boolean  "is_close",                  :default => false, :null => false
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
  end

  create_table "transportcategories_transports", :id => false, :force => true do |t|
    t.string "transport_id",         :limit => nil, :null => false
    t.string "transportcategory_id", :limit => nil, :null => false
  end

  create_table "transportcolors", :id => false, :force => true do |t|
    t.string   "id",         :limit => nil,                    :null => false
    t.string   "name"
    t.boolean  "is_close",                  :default => false, :null => false
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
  end

  create_table "transportcolors_transports", :id => false, :force => true do |t|
    t.string "transport_id",      :limit => nil
    t.string "transportcolor_id", :limit => nil
  end

  create_table "transportecologicals", :id => false, :force => true do |t|
    t.string   "id",         :limit => nil,                    :null => false
    t.string   "name"
    t.boolean  "is_close",                  :default => false, :null => false
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
  end

  create_table "transportexploitations", :id => false, :force => true do |t|
    t.string   "id",         :limit => nil,                    :null => false
    t.string   "name"
    t.text     "descr"
    t.boolean  "is_close",                  :default => false, :null => false
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
  end

  create_table "transportexploitations_transports", :id => false, :force => true do |t|
    t.string "transportexploitation_id", :limit => nil
    t.string "transport_id",             :limit => nil
  end

  create_table "transportkinds", :id => false, :force => true do |t|
    t.string   "id",               :limit => nil,                    :null => false
    t.string   "name"
    t.text     "descr"
    t.string   "transporttype_id", :limit => nil
    t.boolean  "is_close",                        :default => false, :null => false
    t.datetime "created_at",                                         :null => false
    t.datetime "updated_at",                                         :null => false
  end

  create_table "transportkinds_transportmodels", :id => false, :force => true do |t|
    t.string "transportkind_id",  :limit => nil
    t.string "transportmodel_id", :limit => nil
  end

  create_table "transportloadings", :id => false, :force => true do |t|
    t.string   "id",         :limit => nil,                    :null => false
    t.string   "loading"
    t.text     "descr"
    t.boolean  "is_close",                  :default => false, :null => false
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
  end

  create_table "transportloadings_transportmodels", :id => false, :force => true do |t|
    t.string "transportloading_id", :limit => nil
    t.string "transportmodel_id",   :limit => nil
  end

  create_table "transportmodels", :id => false, :force => true do |t|
    t.string   "id",                :limit => nil,                                                   :null => false
    t.string   "transportbrand_id", :limit => nil
    t.string   "name"
    t.decimal  "weight",                           :precision => 16, :scale => 2
    t.decimal  "capacity",                         :precision => 16, :scale => 2
    t.decimal  "overall_height",                   :precision => 16, :scale => 2
    t.decimal  "overall_width",                    :precision => 16, :scale => 2
    t.decimal  "overall_length",                   :precision => 16, :scale => 2
    t.decimal  "useful_height",                    :precision => 16, :scale => 2
    t.decimal  "useful_width",                     :precision => 16, :scale => 2
    t.decimal  "useful_length",                    :precision => 16, :scale => 2
    t.decimal  "useful_volume",                    :precision => 16, :scale => 2
    t.boolean  "is_engine",                                                       :default => false, :null => false
    t.boolean  "is_loading",                                                      :default => false, :null => false
    t.boolean  "is_fuelling",                                                     :default => false, :null => false
    t.boolean  "is_close",                                                        :default => false, :null => false
    t.datetime "created_at",                                                                         :null => false
    t.datetime "updated_at",                                                                         :null => false
  end

  create_table "transportmodels_transports", :id => false, :force => true do |t|
    t.string "transport_id",      :limit => nil
    t.string "transportmodel_id", :limit => nil
  end

  create_table "transports", :id => false, :force => true do |t|
    t.string   "id",                  :limit => nil,                                                   :null => false
    t.string   "name"
    t.text     "description"
    t.string   "vin"
    t.string   "reg_num"
    t.string   "body_num"
    t.string   "chassis_num"
    t.string   "engine_num"
    t.integer  "year"
    t.integer  "weight"
    t.decimal  "capacity",                           :precision => 16, :scale => 2
    t.decimal  "overall_height",                     :precision => 16, :scale => 2
    t.decimal  "overall_width",                      :precision => 16, :scale => 2
    t.decimal  "overall_length",                     :precision => 16, :scale => 2
    t.decimal  "useful_height",                      :precision => 16, :scale => 2
    t.decimal  "useful_width",                       :precision => 16, :scale => 2
    t.decimal  "useful_length",                      :precision => 16, :scale => 2
    t.decimal  "useful_volume",                      :precision => 16, :scale => 2
    t.date     "registration_date"
    t.date     "deregistration_date"
    t.boolean  "is_close",                                                          :default => false
    t.datetime "created_at",                                                                           :null => false
    t.datetime "updated_at",                                                                           :null => false
  end

  create_table "transporttanks", :id => false, :force => true do |t|
    t.string   "id",                :limit => nil,                    :null => false
    t.integer  "v_tank"
    t.integer  "v_tank_min"
    t.string   "transportmodel_id", :limit => nil
    t.boolean  "is_close",                         :default => false, :null => false
    t.datetime "created_at",                                          :null => false
    t.datetime "updated_at",                                          :null => false
  end

  create_table "transporttypes", :id => false, :force => true do |t|
    t.string   "id",                   :limit => nil,                    :null => false
    t.string   "transportcategory_id", :limit => nil
    t.string   "name"
    t.text     "descr"
    t.boolean  "is_close",                            :default => false, :null => false
    t.datetime "created_at",                                             :null => false
    t.datetime "updated_at",                                             :null => false
  end

  create_table "truck_drivers", :id => false, :force => true do |t|
    t.string   "id",           :limit => nil, :null => false
    t.string   "transport_id", :limit => nil
    t.string   "employee_id",  :limit => nil
    t.boolean  "is_close"
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "tsloadings", :id => false, :force => true do |t|
    t.string   "id",           :limit => nil,                    :null => false
    t.string   "transport_id", :limit => nil
    t.string   "loading"
    t.boolean  "is_close",                    :default => false
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
  end

  create_table "tstanks", :id => false, :force => true do |t|
    t.string   "id",           :limit => nil, :null => false
    t.string   "transport_id", :limit => nil
    t.integer  "v_tank"
    t.integer  "v_tank_min"
    t.boolean  "is_close"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "unv_comments", :id => false, :force => true do |t|
    t.string   "id",           :limit => nil, :null => false
    t.text     "comment"
    t.string   "user_id",      :limit => nil
    t.string   "OrderFromKey", :limit => nil
    t.string   "OrderToKey",   :limit => nil
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "unv_employees", :id => false, :force => true do |t|
    t.string   "id",            :limit => nil, :null => false
    t.string   "employee_id",   :limit => nil
    t.string   "unv_type_id",   :limit => nil
    t.datetime "planned_start"
    t.datetime "planned_end"
    t.datetime "actual_start"
    t.datetime "actual_end"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "unv_types", :id => false, :force => true do |t|
    t.string   "id",               :limit => nil, :null => false
    t.string   "resourse_type_id", :limit => nil
    t.string   "name"
    t.text     "descr"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  create_table "usergroups", :id => false, :force => true do |t|
    t.string   "id",          :limit => nil,                    :null => false
    t.string   "name"
    t.text     "description"
    t.date     "start_time"
    t.date     "end_time"
    t.boolean  "is_close",                   :default => false, :null => false
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
  end

  create_table "usergroups_users", :id => false, :force => true do |t|
    t.string "usergroup_id", :limit => nil
    t.string "user_id",      :limit => nil
  end

  create_table "users", :id => false, :force => true do |t|
    t.string   "id",                     :limit => nil,                    :null => false
    t.string   "email",                                 :default => "",    :null => false
    t.string   "encrypted_password",                    :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                         :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "name"
    t.string   "surname"
    t.string   "last_name"
    t.text     "advanced"
    t.string   "person_id",              :limit => nil
    t.boolean  "admin"
    t.boolean  "is_close",                              :default => false, :null => false
    t.datetime "created_at",                                               :null => false
    t.datetime "updated_at",                                               :null => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true

  create_table "wialons", :force => true do |t|
    t.integer  "template"
    t.text     "rec_data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
