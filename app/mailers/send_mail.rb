# -*- encoding : utf-8 -*-
class SendMail < ActionMailer::Base
  default from: "message@monopoly.su"

  def msg(text)
    @text = text
    mail(to: "mihail.rogov@monopoly.su", subject: text)
  end

end
