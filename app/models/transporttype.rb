# -*- encoding : utf-8 -*-
class Transporttype < ActiveRecord::Base
  attr_accessible :name, :transportcategory_id , :descr, :is_close

  audited

  belongs_to :transportcategory
  has_many :transportkinds

  validates :name, presence: true

  scope :visible, where("is_close != ?", true)

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end


end
