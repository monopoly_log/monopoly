class MsPerson < Monosql

  self.table_name = 'msEmployee'
  self.primary_key = :EmployeeKey

  attr_accessible :EmployeeKey, :LastName, :FirstName, :MiddleName, :Mail, :Moment, :BirthDate, :MomentEdit, :EmploymentDate, :DismissalDate, :EmployeeType
  
end
