# -*- encoding : utf-8 -*-
class Objecttype < Monopolydata
  attr_accessible :descr, :end_date, :id, :is_close, :name, :start_date

  has_many :objecttypeproperties


  scope :visible, where("[objecttypes].[is_close] != ?", true)


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end
  

end
