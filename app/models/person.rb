# -*- encoding : utf-8 -*-
class Person < ActiveRecord::Base
  attr_accessible :birthdate, :last_name, :name, :surname, :gender, :start_date, :end_date,  :mail, :is_close, :id

  has_many :phoneuses
  has_many :phones, through: :phoneuses
  has_many :users
  has_many :contractor_representatives
  has_and_belongs_to_many :documents
  has_and_belongs_to_many :contractors

  audited

  validates :birthdate, :last_name, :name, :surname, :id, presence: true
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :mail, allow_blank: true, format: { with: VALID_EMAIL_REGEX , message: "Неверный формат электронной почты." }


  
  before_validation :ensure_uuid

  scope :visible, where("[people].[is_close] != ?", true).order('last_name, name, surname')

  def document
     self.documents.any? ? self.documents.first : false
  end

  def fullname
   [last_name, name, surname].compact.join(' ').strip
  end

  def fullname_g_b
   [last_name, name, surname, gender ? "(М)" : "(Ж)", birthdate].compact.join(' ').strip
  end


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

 def ensure_uuid; self.id ||= SecureRandom.uuid end

end
