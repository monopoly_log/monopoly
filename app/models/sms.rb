# -*- encoding : utf-8 -*-
class Sms < ActiveRecord::Base
  attr_accessible :phone_number, :sms_text, :message_id

  audited

  self.table_name = "_temp_sms_for_send"


  scope :not_sent, where("message_id is NULL")

end
