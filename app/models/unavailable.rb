class Unavailable < Monosql

    def self.getdata(user_id = nil)
      #connection.select_all "exec [mono-sql].[MonopolySun].[dbo].[ms_Order_OLTO_Get]"
      user_id = "'" + user_id + "'" if user_id
      connection.select_all "exec [MonopolySun].[dbo].[ms_Order_OLTO_Get] @idUser = #{user_id ? user_id : 'NULL'}"
    end

    def self.getdata_ol
      connection.select_all "exec [MonopolySun].[dbo].[ms_Order_OLTO_Get_OL]"
    end

    def self.repair_request(tr_key, chain=nil)
      tr_key = "'" + tr_key + "'"
      connection.select_all "exec [MonopolySun].[dbo].[ms_TransportRepairRequest_Get] @TransportKey = #{tr_key}, @ForChain = #{chain}"
    end

    def self.relocate_request(tr_key)
      tr_key = "'" + tr_key + "'"
      connection.select_all "exec [MonopolySun].[dbo].[ms_TransportPereezd_Get] @TransportKey = #{tr_key}"
    end

    def self.connection_request(from, to)
      connection.select_all "exec [MonopolySun].[dbo].[ms_CheckConcat_List] @MomentFrom = #{from ? from : 'NULL'}, @MomentTo = #{to ? to : 'NULL'}"
    end

    def self.connections_unv(key)
      key = "'" + key + "'"
      connection.select_all "exec [MonopolySun].[dbo].[ms_Inaccessibility_Get] @OrderKey = #{key}"
    end

#
#  def self.getdata
#    self.connection.execute("[mono-sql].[MonopolySun].[dbo].[ms_Order_OLTO_Get]")
#  end


end
