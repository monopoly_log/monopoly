# -*- encoding : utf-8 -*-
class Usergroup < ActiveRecord::Base
  set_primary_key :id
  attr_accessible :end_time,  :name, :start_time, :is_close

  audited

  has_and_belongs_to_many :functionalgroups
  has_and_belongs_to_many :functionals
  has_and_belongs_to_many :users
  has_and_belongs_to_many :roles

  validates :id, presence: true
  before_validation :ensure_uuid


  scope :visible, where("is_close != ?", true)

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end



  def ensure_uuid; self.id ||= SecureRandom.id end

end
