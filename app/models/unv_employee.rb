class UnvEmployee < ActiveRecord::Base
  attr_accessible :actual_end, :actual_start, :employee_id, :id, :planned_end, :planned_start, :unv_type_id


  belongs_to :employee
  belongs_to :unv_type

  validates :planned_start, :employee_id, :unv_type_id,  presence: true

end
