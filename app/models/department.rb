# -*- encoding : utf-8 -*-
class Department < ActiveRecord::Base
  attr_accessible :contractor_id, :descr, :id,  :name, :parent_id, :is_close

  acts_as_nested_set

  belongs_to :contractor
  has_many :staff_units

  audited

  before_validation :ensure_uuid , :delete_empty_parameters

  validates :contractor_id, :descr, :name, presence: true

  scope :visible, where("departments.is_close != ?", true).joins(:contractor).order('contractors.name')

  def employees_sum
    self.staff_units.inject(0){ |r, s| r += s.employee ? 1 : 0 }
  end

  def employees
    self.staff_units.map{ |s| s.employee }.compact
  end


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

  def contractor_name
    self.contractor ? "Контрагент: #{self.contractor.name}, Подразделение: #{name}" : ''
  end


  private


  def ensure_uuid
    self.id ||= SecureRandom.uuid
  end

  def delete_empty_parameters
     Department.accessible_attributes.each do |c|
       self[c.to_sym] = nil if self[c.to_sym].blank? and  c!= 'is_close'
     end
  end

end
