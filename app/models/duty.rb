# -*- encoding : utf-8 -*-
class Duty < Monosql
self.table_name = "dbo.msSubscriberTemp"
#self.primary_key = :id

  attr_accessible :EmployeeKey, :IsLog, :IsMan, :MomentFrom, :MomentTo, :created_at, :user_id, :fio

  validates :EmployeeKey, presence: {:message => 'Выберите дежурного'}
  validates :MomentFrom, :MomentTo, presence: {:message => 'Выберите дату и время'}
  audited


  #before_validation :parse_date
  

  def self.people
    Monosql.find_by_sql(["SELECT e.EmployeeKey
     , e.LastName + ' ' + e.FirstName + ' ' + e.MiddleName FIO
FROM
  [MonopolySun].[dbo].[msEmployee] e
  JOIN [MonopolySun].[dbo].[msEmployeeInternal] ei
    ON e.[EmployeeKey] = ei.EmployeeKey
ORDER BY
  e.LastName + ' ' + e.FirstName + ' ' + e.MiddleName "])#.map {|m| [m.FIO, m.EmployeeKey]}
  end

  def fio
   Monosql.find_by_sql(["SELECT top (1) e.LastName + ' ' + e.FirstName + ' ' + e.MiddleName FIO from msEmployee e where e.EmployeeKey like ?", self.EmployeeKey]).map {|m| m.FIO}[0]

  end

private

  def parse_date
    from = self.MomentFrom.split(/\D/)
    to = self.MomentTo.split(/\D/)
    self.MomentFrom = DateTime.new(from[2], from[1], from[0], from[3], from[4]).to_s(:db)
    self.MomentTo = DateTime.new(to[2], to[1], to[0], to[3], to[4]).to_s(:db)
  end



end
