# -*- encoding : utf-8 -*-
class Objecttypeproperty < Monopolydata
  attr_accessible :description, :end, :id, :is_close, :name, :objecttype_id, :start, :system_id

  belongs_to :objecttype
  belongs_to :system


  scope :visible, where("[objecttypeproperties].[is_close] != ?", true)


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end
  

end
