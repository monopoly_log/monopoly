# -*- encoding : utf-8 -*-
class Document < ActiveRecord::Base
  attr_accessible :category, :contract_id, :contractor_id, :documentkind_id, :end_date, :is_close, :issued_by,
                  :issued_date, :number, :registration, :series, :start_date, :parent_id

  acts_as_nested_set

  belongs_to :documentkind
  has_and_belongs_to_many :transports
  has_and_belongs_to_many :people
  has_many :documentfiles

  audited

  validates :documentkind_id, presence: {:message => 'Выберите тип документа'}, on: :update
  validates :series, presence: {:message => 'Заполните серию документа'}
  validates :number, presence: {:message => 'Заполните номер документа'}
  
  #before_validation :ensure_uuid, :delete_empty_parameters
  
  scope :visible, where("is_close != ?", true)

  def self.destroy_invalid
    Document.all.each { |doc| doc.destroy unless doc.valid?  }
  end

#  def parent
#    (self.parent_id and Document.find_by_id(self.parent_id)) ? Document.find(self.parent_id) : false
#  end

  def transport 
     self.transports.any? ? self.transports.first : false
  end

  def person
     self.people.any? ? self.people.first : false
  end

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

  def ensure_uuid
    self.id ||= SecureRandom.uuid
  end
  
  private

  def delete_empty_parameters
     self.parent_id = nil if self.parent_id == ''
  end

  protected

end
