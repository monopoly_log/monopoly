# -*- encoding : utf-8 -*-
class Documenttype < ActiveRecord::Base
  attr_accessible :descr, :is_close, :name

  audited

  has_many :documentkinds


  scope :visible, where("is_close != ?", true)

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end


end
