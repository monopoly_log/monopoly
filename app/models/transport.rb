# -*- encoding : utf-8 -*-
class Transport < ActiveRecord::Base
  attr_accessible :description, :name, :vin, :body_num, :chassis_num, :engine_num, :year, :weight, :capacity, :reg_num, :is_close,
                  :overall_height, :overall_width, :overall_length, :useful_height, :useful_width, :useful_length, :useful_volume,
                  :registration_date, :deregistration_date

  

  validates :reg_num, :vin,  presence: true
  validates :year,  presence: true,
                    numericality: true,
                    length: {minimum: 1, maximum: 4}
  validates :reg_num, :vin, uniqueness: true, on: :update


  has_many :tsloadings
  has_many :tstanks

  has_one :truck, :class_name => 'Coupler', :foreign_key => 'truck_id'
  has_one :trailer, :class_name => 'Coupler', :foreign_key => 'trailer_id'
  has_many :truck_drivers


  has_and_belongs_to_many :transportbrands
  has_and_belongs_to_many :transportmodels
  has_and_belongs_to_many :transportcategories
  has_and_belongs_to_many :transportcolors
  has_and_belongs_to_many :transportexploitations
  has_and_belongs_to_many :enginemodels

  has_and_belongs_to_many :documents
  
  audited

  before_validation :ensure_uuid


  def truck
    Coupler.where({truck_id: self.id, is_close: false}).first
  end

  def trailer
    Coupler.where({trailer_id: self.id, is_close: false}).first
  end


  def used_truck?
    self.truck && !self.truck.is_close
  end

  def used_trailer?
    self.trailer && !self.trailer.is_close
  end


  scope :visible, where("transports.is_close != ?", true)
  scope :trucks, visible.joins(transportmodels: [ transportkinds: :transporttype ] ).where("[transporttypes].id = ?", 'eee53245-a100-42d3-8b36-c4b891dca119')
  scope :trailers, visible.joins(transportmodels: [ transportkinds: :transporttype ] )
            .where("[transporttypes].id = ? or [transporttypes].id = ? or [transporttypes].id = ? or [transporttypes].id = ? ",
              '1e475762-b77b-427b-99de-e9350b455b13' , 'f8902d5a-762b-4188-b698-b893fbc0553c', '1d7b592f-8d12-4c4d-bc5a-71484efc1fff', '2c505e83-7074-46b1-ad55-5254a7067476')
  scope :used_trucks, lambda { trucks.map { |t| t if t.used_truck? }.compact }
  scope :used_trailers, lambda { trailers.map { |t| t if t.used_trailer? }.compact }
  scope :free_trucks, lambda { trucks - used_trucks }
  scope :free_trailers, lambda { trailers - used_trailers }






  def document
     self.documents.any? ? self.documents.first : false
  end

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

  def transportmodel
    self.transportmodels.any? ? self.transportmodels.first : false
  end

  def transportexploitation
    self.transportexploitations.any? ? self.transportexploitations.first : false
  end

  def transporttype
    self.transportmodel && self.transportmodel.transportkinds.first.any?  && self.transportmodel.transportkinds.first.transporttype ? self.transportmodel.transportkinds.first.transporttype : false
  end


  def enginemodel
    self.enginemodels.any? ? self.enginemodels.first : false
  end


  def transportcolor
    self.transportcolors.any? ? self.transportcolors.first : false
  end


 def ensure_uuid
    self.id ||= SecureRandom.uuid
 end



end
