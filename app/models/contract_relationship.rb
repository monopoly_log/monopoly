class ContractRelationship < ActiveRecord::Base
  attr_accessible :contract_id, :contract_subject_role_id, :contractor_id, :id, :is_close

  belongs_to :contract_subject_role
  belongs_to :contract
  belongs_to :contractor

  audited

  validates :contract_id, :contract_subject_role_id, :contractor_id, presence: true

  scope :visible, where("contract_relationships.is_close != ?", true)

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end


end
