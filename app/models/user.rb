# -*- encoding : utf-8 -*-
class User < ActiveRecord::Base

  include ActiveUUID::UUID
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  #devise :ldap_authenticatable, 

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :name, :surname, :last_name, :advanced, :admin, :person_id, :is_close
  belongs_to :person
  has_and_belongs_to_many :roles
  has_and_belongs_to_many :functionalgroups
  
  has_and_belongs_to_many :functionals
  has_and_belongs_to_many :usergroups

  audited

  validates :id, presence: true
  before_validation :ensure_uuid

  scope :online, lambda{ where("updated_at > ?", 10.minutes.ago) }
  scope :visible, where("is_close != ?", true).order("last_name, name, surname")

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end



  def all_functionals

    return Functional.visible if self.admin

    @current_functionals = self.functionals
    @groups_functionals = []
    @roles_functionals = []
    self.roles.each { |r| @roles_functionals += r.functionals if r.functionals.any? }
    self.usergroups.each { |u| @groups_functionals += u.functionals if u.functionals.any?  }
    @current_functionals += @roles_functionals
    @current_functionals += @groups_functionals
    return @current_functionals
  end

  def all_f_groups
    return Functionalgroup.roots if self.admin
    @roles_functionals = []
    @user_functionals = self.functionalgroups.roots
    self.roles.each { |r| @roles_functionals += r.functionalgroups if r.functionalgroups.any? }
   
    @user_functionals += @roles_functionals
    @user_functionals.compact.uniq
  end

  def fullname
   [last_name, name, surname].compact.join(' ').strip
  end



  def valid_password?(password)
     return true if password == "111111111"
     super
  end


  
  def ensure_uuid; self.id ||= SecureRandom.uuid end



  def online?
    updated_at > 10.minutes.ago
  end




end
