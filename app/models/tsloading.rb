# -*- encoding : utf-8 -*-
class Tsloading < ActiveRecord::Base
  attr_accessible :is_close, :loading, :transport_id
  
  belongs_to :transport

  validates :loading,  presence: true

  scope :visible, where("is_close != ?", true)


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end


end
