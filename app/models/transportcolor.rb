# -*- encoding : utf-8 -*-
class Transportcolor < ActiveRecord::Base
  attr_accessible :name, :is_close

  audited

  validates :name, presence: {:message => 'Поле «Цвет» обязательно для заполнения!'}
  has_and_belongs_to_many :transports


  scope :visible, where("is_close != ?", true)

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end



end
