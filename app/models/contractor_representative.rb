class ContractorRepresentative < ActiveRecord::Base
  attr_accessible :contractor_id, :end_date, :id, :is_close, :person_id, :position_id, :start_date

  validates :contractor_id, :person_id, :position_id, :start_date, presence: true


  belongs_to :contractor
  belongs_to :person
  belongs_to :position

  audited

  scope :visible, where("is_close != ?", true).order(:contractor_id)


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end


end
