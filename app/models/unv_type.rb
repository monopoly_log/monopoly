class UnvType < ActiveRecord::Base
  attr_accessible :descr, :id, :name, :resourse_type_id

  belongs_to :resourse_type
  has_many :env_employees

  validates :name, :resourse_type_id, presence: true


end
