# -*- encoding : utf-8 -*-
class StaffUnit < ActiveRecord::Base
  attr_accessible :department_id, :descr, :head, :id, :position_id, :is_close, :rgt, :lft, :parent_id

  belongs_to :department
  belongs_to :position
  has_one :employee

  audited
  
  #acts_as_nested_set

  before_validation :ensure_uuid, :delete_empty_parameters

  validates :department_id, :descr, :position_id, presence: true

  scope :visible, where("is_close != ?", true)
  scope :heads, where("head = ?", true).visible


  def employee #переопределение 
    self.is_close ? nil : super
  end


  def contractor
    self.department and self.department.contractor ? self.department.contractor : false
  end

  def cdp #contractor + department + position
    self.contractor and self.position ?  "Контрагент:#{self.department.contractor.name}. Подразделение: #{self.department.name}. Должность: #{self.position.name}." : false
  end

  def self.free_units
    self.visible.find_all{|f| !f.employee}.sort_by(&:cdp).compact
  end


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end


  private


  def ensure_uuid
    self.id ||= SecureRandom.uuid
  end

  def delete_empty_parameters
     Department.accessible_attributes.each do |c|
       self[c.to_sym] = nil if self[c.to_sym].blank? and  c!= 'is_close'
     end
  end


end
