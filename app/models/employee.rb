class Employee < ActiveRecord::Base
  attr_accessible :employment_date, :end_date, :id, :leaving_date, :person_id, :start_date, :staff_unit_id, :is_close

  belongs_to :person
  belongs_to :staff_unit
  has_many :unv_amployees
  has_many :truck_drivers

  audited

  before_validation :ensure_uuid, :delete_empty_parameters

  validates :employment_date, :person_id, :staff_unit_id,  presence: true

  scope :visible, where("employees.is_close != ?", true)
  scope :closed_employees, where("employees.is_close = ?", true)
  scope :driver_expeditors, visible.joins(:staff_unit).where("staff_units.is_close != ? and staff_units.position_id = ?", true, '279cb6eb-0cac-4b70-bc37-bad1c08dddb8')
  scope :truck_drivers, lambda { unscoped.map { |e| e if e.truck_drivers.any? }.compact.uniq.sort_by(&:person_info) }

  def close_employee
    self.update_attribute( :is_close, true )
    self.update_attribute( :leaving_date, Time.now ) unless self.leaving_date
    EmployeeHistory.create({employee_id: self.id,
                            employment_date: self.employment_date,
                            leaving_date: self.leaving_date,
                            start_date: self.start_date,
                            end_date: self.end_date,
                            contractor_id: self.staff_unit.department.contractor_id,
                            contractor_name: self.staff_unit.department.contractor.name,
                            department_id: self.staff_unit.department_id,
                            department_name: self.staff_unit.department.name,
                            position_id: self.staff_unit.position_id,
                            position_name: self.staff_unit.position.name,
                            person_id: self.person_id,
                            staff_unit_id: self.staff_unit_id })
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

#  def cdp #contractor + department + position
#    self.staff_unit ?  "Контрагент:#{self.staff_unit.department.contractor.name}. Подразделение: #{self.staff_unit.department.name}. Должность: #{self.staff_unit.position.name}." : false
#  end

  def person_info
    person.fullname + " #{person.birthdate}"
  end


  private


  def ensure_uuid
    self.id ||= SecureRandom.uuid
  end

  def delete_empty_parameters
     Employee.accessible_attributes.each do |c|
       self[c.to_sym] = nil if self[c.to_sym].blank? and  c!= 'is_close'
     end
  end


end
