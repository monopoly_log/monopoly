# -*- encoding : utf-8 -*-
class Enginetype < ActiveRecord::Base
  attr_accessible  :name, :is_close

  audited

  has_many :enginemodels
  has_and_belongs_to_many :transportmodels

  validates :name, presence: true
  
  scope :visible, where("is_close != ?", true)

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

end
