class ContractSubjectRole < ActiveRecord::Base
  attr_accessible :contract_subject_id, :descr, :id, :is_close, :name


  belongs_to :contract_subject

  audited

  validates :contract_subject_id, :name, presence: true

  scope :visible, where("contract_subject_roles.is_close != ?", true)
  scope :invisible, where("contract_subject_roles.is_close = ?", true)

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

end
