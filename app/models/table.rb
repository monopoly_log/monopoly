# -*- encoding : utf-8 -*-
class Table < Monopolydata
  attr_accessible :description, :end, :is_close, :name, :start

  has_many :columns

  scope :visible, where("[tables].[is_close] != ?", true)


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end


end
