# -*- encoding : utf-8 -*-
class Tstank < ActiveRecord::Base
  attr_accessible :is_close, :transport_id, :v_tank, :v_tank_min

  belongs_to :transport

  validates :v_tank, :v_tank_min,  presence: true

  scope :visible, where("is_close != ?", true)


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end


end
