# -*- encoding : utf-8 -*-
class Transportmodel < ActiveRecord::Base
  attr_accessible :name, :transportbrand_id, :weight, :capacity, :is_engine, :is_loading, :is_fuelling, :id, :is_close,
                  :overall_height, :overall_width, :overall_length, :useful_height, :useful_width, :useful_length, :useful_volume
 

  validates :name, :transportbrand_id, :weight, :capacity, presence: true
  validates :weight, :capacity,  numericality: true
  validates :overall_height, :overall_width, :overall_length, :useful_height, :useful_width, :useful_length, :useful_volume,  numericality: true, allow_blank: true
    

  validate :has_transportkind, :engine_loading_tank, on: :update

  def has_transportkind
    errors.add(:transportkind, "Не указаны категория, тип и вид ТС") unless self.transportkind
  end

  def engine_loading_tank
    errors.add(:transportloading, "Не указан тип погрузки") if self.is_loading and self.transportloadings.blank?
    errors.add(:transporttank, "Не указан топливный бак") if self.is_fuelling and self.transporttanks.blank?
    errors.add(:enginetype, "Не указан тип двигателя") if self.is_engine and self.enginetypes.blank?
  end


  belongs_to :transportbrand
  has_many :transporttanks
  has_and_belongs_to_many :transportloadings
  has_and_belongs_to_many :transportkinds
  has_and_belongs_to_many :transports
  has_and_belongs_to_many :enginetypes
  audited

 before_validation :ensure_uuid

 def ensure_uuid
    self.id ||= SecureRandom.uuid
 end

  scope :visible, where("is_close != ?", true)

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end



  def enginetype
    return self.enginetypes.any? ? self.enginetypes.first : false
  end

  def transportkind
    return self.transportkinds.any? ? self.transportkinds.first : false
  end


end
