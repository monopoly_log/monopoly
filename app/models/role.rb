# -*- encoding : utf-8 -*-
class Role < ActiveRecord::Base
  include ActiveUUID::UUID

  audited

  set_primary_key :id
  attr_accessible  :name, :is_close, :descr

  #has_many :users
  has_and_belongs_to_many :users
  has_and_belongs_to_many :functionalgroups
  
  has_and_belongs_to_many :functionals
  has_and_belongs_to_many :usergroups

  validates :id, presence: true

  before_validation :ensure_uuid

  scope :visible, where("is_close != ?", true)

  


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end


  def ensure_uuid; self.id ||= SecureRandom.uuid end

end
