# -*- encoding : utf-8 -*-
class Contractortype < ActiveRecord::Base
  attr_accessible :descr, :name

   has_many :contractors

  scope :visible, where("is_close != ?", true)

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end



end
