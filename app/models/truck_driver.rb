class TruckDriver < ActiveRecord::Base
  attr_accessible :employee_id, :end_date, :id, :is_close, :start_date, :transport_id

  belongs_to :transport
  belongs_to :employee

  validates  :start_date, :employee_id, :transport_id, presence: true

  audited

  scope :chained, where("[truck_drivers].[is_close] != ?", true)
  scope :unchained, where("[truck_drivers].[is_close] != ?", false)
  #scope :drivers, lambda { all.map { |t| t.employee }.uniq }
  scope :trucks, lambda { joins(:transport).select("distinct truck_drivers.transport_id, transports.reg_num").order(:reg_num) } # возможно лучше перенести в Transport
  #scope :drivers, lambda { joins(:employee).select('employees.* ').uniq } # Убрано в Employee


  def person_info
    employee.person.fullname + " #{person.birthdate}"
  end

  def close_truck_driver
    self.update_attributes({ is_close: true, end_date: Time.now })
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

end
