# -*- encoding : utf-8 -*-
class Column < Monopolydata
  attr_accessible :description, :end, :is_close, :name, :start, :table_id

  has_many :objecttypepropertycolumns
  belongs_to :table

    scope :visible, where("[columns].[is_close] != ?", true)


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end
  

end
