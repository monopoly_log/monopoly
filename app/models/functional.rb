# -*- encoding : utf-8 -*-
class Functional < ActiveRecord::Base
  self.primary_key = :id
  attr_accessible :description, :end, :name, :part, :start, :is_close

  has_and_belongs_to_many :users
  has_and_belongs_to_many :usergroups
  has_and_belongs_to_many :roles
  audited

  #validates :id, presence: true
  before_validation :ensure_uuid

  scope :visible, where("is_close != ?", true)

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end


  def ensure_uuid; self.id ||= SecureRandom.uuid end
end
