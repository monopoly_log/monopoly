# -*- encoding : utf-8 -*-
class Transportcategory < ActiveRecord::Base
  attr_accessible  :name, :descr, :is_close

  audited


  has_and_belongs_to_many :transports
  has_many :transporttypes

  validates :name, presence: true

  scope :visible, where("is_close != ?", true)

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end



end
