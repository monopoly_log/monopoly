# -*- encoding : utf-8 -*-
class Transportecological < ActiveRecord::Base
  attr_accessible :id, :name, :is_close

  audited

  validates :name, presence: {:message => 'Поле «Эклогический класс ТС» обязательно для заполнения!'}

  scope :visible, where("is_close != ?", true)

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end



end
