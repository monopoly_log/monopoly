class UnvComment < ActiveRecord::Base
  attr_accessible :OrderFromKey, :OrderToKey, :comment, :id, :user_id


  scope :find_comments, ->(order_from_key, order_to_key) { where("OrderFromKey = ? and OrderToKey = ?", order_from_key, order_to_key).order('created_at desc') }
  scope :last_comment, ->(order_from_key, order_to_key) { where("OrderFromKey = ? and OrderToKey = ?", order_from_key, order_to_key).order('created_at desc').limit(1) }
  scope :comments_count, ->(order_from_key, order_to_key) { where("OrderFromKey = ? and OrderToKey = ?", order_from_key, order_to_key).select(:id)}

  audited

end
