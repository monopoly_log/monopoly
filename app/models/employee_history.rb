class EmployeeHistory < ActiveRecord::Base
  attr_accessible :contractor_id, :contractor_name, :department_id, :department_name, :employee_id, :employment_date, :end_date, :id,
                  :leaving_date, :person_id, :position_id, :position_name, :staff_unit_id, :start_date


  belongs_to :employee
  belongs_to :contractor
  belongs_to :department
  belongs_to :person
  belongs_to :position
  belongs_to :staff_unit

  audited

  before_validation :ensure_uuid, :delete_empty_parameters

  private

  def ensure_uuid
    self.id ||= SecureRandom.uuid
  end

  def delete_empty_parameters
     EmployeeHistory.accessible_attributes.each do |c|
       self[c.to_sym] = nil if self[c.to_sym].blank? and  c!= 'is_close'
     end
  end




end
