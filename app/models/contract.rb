class Contract < ActiveRecord::Base
  attr_accessible :contract_number, :contract_subject_select_id, :end_date, :id, :is_close, :start_date

  belongs_to :contract_subject_select

  audited

  validates :contract_subject_select_id, :contract_number, :start_date, presence: true

  scope :visible, where("is_close != ?", true)

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end




end
