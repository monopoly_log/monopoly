# -*- encoding : utf-8 -*-
class Phone < ActiveRecord::Base
  

  attr_accessible :end_date, :is_close, :phone, :start_date, :id

  validates  :phone, :start_date, presence: true

  has_many :phoneuses
  belongs_to :person
  audited

  extend ModelsMixin

  scope :visible, where("[phones].[is_close] != ?", true)

#  def self.delete_closed
#    delete_all({is_close: true})
#  end


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

end
