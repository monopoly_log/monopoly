class ContractType < ActiveRecord::Base
  attr_accessible :descr, :id, :name

  has_many :contract_subjects
  
  audited

  validates :name, presence: true

  scope :visible, where("is_close != ?", true)

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

end
