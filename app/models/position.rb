# -*- encoding : utf-8 -*-
class Position < ActiveRecord::Base
  attr_accessible :descr,  :name, :is_close

  has_many :staff_units
  has_many :contractor_representatives
  audited

  scope :visible, where("is_close != ?", true).order(:name)


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

end
