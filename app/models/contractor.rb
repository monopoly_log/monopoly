# -*- encoding : utf-8 -*-
class Contractor < ActiveRecord::Base
  attr_accessible :contractortype_id, :end_date, :inn, :kpp, :lft, :name, :parent_id, :rgt, :start_date, :is_legal

  validates :contractortype_id, :name, :start_date, presence: true

  acts_as_nested_set

  belongs_to :contractortype
  has_many :departments
  has_many :contractor_representatives
  has_and_belongs_to_many :people

  
  scope :visible, where("contractors.is_close != ?", true).order(:name)
  scope :outside, visible.joins(:contractortype).where("contractortypes.name = ?", 'Внешний')
  scope :inside, visible.joins(:contractortype).where("contractortypes.name = ?", 'Внутренний')


  def staff_units
    @staff_units = []
    self.departments.each { |d| @staff_units += d.staff_units  }
    @staff_units.compact
  end


  def employees
    self.staff_units.map{|su| su.employee }.compact
  end


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end


end
