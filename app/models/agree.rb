class Agree < ActiveRecord::Base
  attr_accessible :OrderFromKey, :OrderToKey, :id, :note, :user_id


  scope :find_agree, ->(order_from_key, order_to_key) { where("OrderFromKey = ? and OrderToKey = ?", order_from_key, order_to_key).limit(1) }

  audited

  def self.agree(order_from_key, order_to_key)
    find_agree(order_from_key, order_to_key).first
  end

end
