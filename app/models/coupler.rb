class Coupler < ActiveRecord::Base
  attr_accessible :end_date, :id, :start_date, :trailer_id, :truck_id, :is_close


  belongs_to :truck, class_name: "Transport", foreign_key: "truck_id"
  belongs_to :trailer, class_name: "Transport", foreign_key: "trailer_id"

  validates  :start_date, :trailer_id, :truck_id, presence: true

  audited


  scope :chained, where("[couplers].[is_close] != ?", true)
  scope :unchained, where("[couplers].[is_close] != ?", false)


  def close_coupler
    self.update_attributes({ is_close: true, end_date: Time.now })
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

end
