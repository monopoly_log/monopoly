# -*- encoding : utf-8 -*-
class Transportloading < ActiveRecord::Base
  attr_accessible :descr, :loading, :is_close

  audited


  has_and_belongs_to_many :transportmodels

  validates :loading, presence: true

  scope :visible, where("is_close != ?", true)

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end
  
end
