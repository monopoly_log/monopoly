# -*- encoding : utf-8 -*-
class Transportbrand < ActiveRecord::Base
  attr_accessible  :name, :is_close

  audited

  has_many :transportmodels, order: "name"
  has_and_belongs_to_many :transports

  validates :name, presence: true


  scope :visible, where("is_close != ?", true)

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end


end
