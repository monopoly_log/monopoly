# -*- encoding : utf-8 -*-
class Transporttank < ActiveRecord::Base
  attr_accessible  :v_tank, :v_tank_min, :transportmodel_id, :is_close

  audited

  belongs_to :transportmodel

  validates :v_tank, :v_tank_min, presence: true

  scope :visible, where("is_close != ?", true)

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end


end
