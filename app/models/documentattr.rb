# -*- encoding : utf-8 -*-
class Documentattr < ActiveRecord::Base
  attr_accessible :category, :end_date, :id, :issued_by, :issued_date, :number, :registration, :series, :start_date, :documentkind_id

  belongs_to :documentkind

end
