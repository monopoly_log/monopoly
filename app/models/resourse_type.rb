class ResourseType < ActiveRecord::Base
  attr_accessible :descr, :id, :name

  has_many :unv_types

  validates :name, presence: true

end
