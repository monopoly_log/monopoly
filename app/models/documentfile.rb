class Documentfile < ActiveRecord::Base
  attr_accessible :document_id, :fname, :is_close, :descr

  belongs_to :document

end
