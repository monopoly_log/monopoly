# -*- encoding : utf-8 -*-
class Functionalgroup < ActiveRecord::Base
  self.primary_key = :id
  attr_accessible :descr, :link, :name, :part, :is_close , :rgt, :lft, :parent_id
 

  acts_as_nested_set
  
  has_and_belongs_to_many :users

  audited

  scope :visible, where("is_close != ?", true)

  before_validation :delete_empty_parameters
  before_create :ensure_uuid

  def ensure_uuid; self.id ||= SecureRandom.uuid end

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

  private

  def delete_empty_parameters
     Department.accessible_attributes.each do |c|
       self[c.to_sym] = nil if self[c.to_sym].blank? and  c!= 'is_close'
     end
  end


end
