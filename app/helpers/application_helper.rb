# -*- encoding : utf-8 -*-
module ApplicationHelper
  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  # вывод марки, модели, vin и рег. номера ТС
  def transport(transport)
    if transport
      @transport = transport
      if transport.transportmodel
        @transportmodel = transport.transportmodel.name
        @transportbrand = transport.transportmodel.transportbrand.name
      end
      render "shared/transport"
    end
  end

  # вывод физ. лица
  def person(person)
    if person
      @person = person
      render "shared/person"
    end
  end

  def registration_coupon?(document)
    return (document.documentkind and document.documentkind.id.to_s == '3be7c8f6-fae8-4090-b67a-697d000c930b'.upcase) ? true : false

  end

  def person_phones(person)
    @person = person
    @phones = @person.phones
    render "shared/person_phones"
  end



  def  document_attr(documentkind, attr)
    return {value: 1, checked: "checked",} if documentkind.documentattr and documentkind.documentattr.send(attr)
    0
  end

  def  document_attr?(document, attr)
    return true if document.documentkind and document.documentkind.documentattr and document.documentkind.documentattr.send(attr)
    false
  end

  
  def letter_pagination letters
    render partial: "shared/letter_pagination" , locals: {letters: letters, current_letter: params[:letter]}
  end



end
