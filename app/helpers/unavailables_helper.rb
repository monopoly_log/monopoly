# -*- encoding : utf-8 -*-
module UnavailablesHelper

  def link_to_comment(unavailable)
    order_from_key = unavailable[0]
    order_to_key = unavailable[1]
    truck = unavailable[2]
    link_to image_tag('comment.png'),  {action: 'new_comment', order_from_key: order_from_key, order_to_key: order_to_key, truck: truck, format: :js} , remote: true
  end

  def comments_count(unavailable)
    order_from_key = unavailable[0]
    order_to_key = unavailable[1]
    UnvComment.unscoped.comments_count(order_from_key, order_to_key).size
    
  end

  def last_comment(unavailable)
    order_from_key = unavailable[0]
    order_to_key = unavailable[1]
    comment = UnvComment.unscoped.last_comment(order_from_key, order_to_key).first
    comment.comment if comment
  end

  def referrer_action
    rec = request.referrer[(request.referrer.rindex('/')+1)..(request.referrer.rindex('?') ? request.referrer.rindex('?')-1 : -1)]
    return 'index' if rec == 'unavailables' or rec.blank?
  end

  def user_ability
    current_user.id == '82eef7ae-5d9b-43f6-94ee-9473059cb8b3'.upcase or current_user.id == 'a58d41fb-5055-4241-9201-147c852df985'.upcase  or current_user.admin
  end


  def agree_con(from_key, to_key)
    if user_ability
    @agree = Agree.agree(from_key, to_key)
    @from_key, @to_key = from_key, to_key
    render partial: 'unavailables/agree'
    end
  end

  def agree?(unavailable)
    !Agree.agree(unavailable[0], unavailable[1]).nil?
  end


end
