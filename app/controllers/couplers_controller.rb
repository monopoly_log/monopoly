# -*- encoding : utf-8 -*-


class CouplersController < ApplicationController


  def index
 #   @couplers = Coupler.all
    @couplers_count = Coupler.count
    case params[:vis]
    when 'chain'
      @couplers = Coupler.chained.order('start_date desc')
    when 'unchain'
      @couplers = Coupler.unchained.order('start_date desc')
    else
      @couplers = Coupler.order('start_date desc')
    end
    @couplers =  @couplers.paginate(page: params[:page], per_page: 20)
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @couplers }
    end
  end


  def show
    @coupler = Coupler.find(params[:id])

    respond_to do |format|
      format.js { }
    end
  end


  def new
    @coupler = Coupler.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @coupler }
    end
  end


  def edit
    @coupler = Coupler.find(params[:id])
  end


  def create
    @coupler = Coupler.new(params[:coupler])

    respond_to do |format|
      if @coupler.save
        format.html { redirect_to couplers_url, notice: 'Сцепка добавлена' }
        format.json { render json: @coupler, status: :created, location: @coupler }
      else
        format.html { render action: "new" }
        format.json { render json: @coupler.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @coupler = Coupler.find(params[:id])

    respond_to do |format|
      if @coupler.update_attributes(params[:coupler])
        format.html { redirect_to couplers_url, notice: 'Сцепка сохранена' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @coupler.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @coupler = Coupler.find(params[:id])
    @coupler.close_coupler
    #@coupler.destroy

    respond_to do |format|
      format.html { redirect_to couplers_url }
      format.json { head :no_content }
    end
  end
end

