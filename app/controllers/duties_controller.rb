# -*- encoding : utf-8 -*-
class DutiesController < ApplicationController


  def index
    case params[:sort]
    when "fio"
      @duties = Duty.all.sort{ |a,b| a.fio <=> b.fio }.paginate(page: params[:page], per_page: 20)
    when "to"
       @duties = Duty.order("MomentTo desc").paginate(page: params[:page], per_page: 20)
    else
      @duties = Duty.order("MomentFrom desc").paginate(page: params[:page], per_page: 20)
    end
    

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @duties }
    end
  end


  def new
    @duty = Duty.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @duty }
    end
  end


  def edit
    @duty = Duty.find(params[:id])
  end


  def create
    @duty = Duty.new(params[:duty])

    @duty.id = SecureRandom.uuid
    respond_to do |format|
      if @duty.save
        format.html { redirect_to duties_url, notice: 'Дежурный добавлен.' }
        format.json { render json: @duty, status: :created, location: @duty }
      else
        format.html { render action: "new" }
        format.json { render json: @duty.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @duty = Duty.find(params[:id])

    respond_to do |format|
      if @duty.update_attributes(params[:duty])
        format.html { redirect_to duties_url, notice: 'Дежурный сохранен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @duty.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @duty = Duty.find(params[:id])
    @duty.destroy

    respond_to do |format|
      format.html { redirect_to duties_url }
      format.json { head :no_content }
    end
  end
end

