# -*- encoding : utf-8 -*-
class ContractorsController < ApplicationController


  def index
    @contractors = Contractor.paginate(page: params[:page], per_page: 20).order(:name)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @contractors }
    end
  end


  def show
    @contractor = Contractor.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @contractor }
    end
  end


  def new
    @contractor = Contractor.new
    @contractor.contractortype_id = params[:contractortype] if params[:contractortype]

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @contractor }
    end
  end


  def edit
    @contractor = Contractor.find(params[:id])
  end


  def create
    params[:contractor].delete(:parent_id) if params[:contractor][:parent_id].blank?
    @contractor = Contractor.new(params[:contractor])

    respond_to do |format|
      if @contractor.save
        format.html { redirect_to contractors_url, notice: 'Контрагент добавлен.' }
        format.json { render json: @contractor, status: :created, location: @contractor }
      else
        format.html { render action: "new" }
        format.json { render json: @contractor.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @contractor = Contractor.find(params[:id])
    params[:contractor].delete(:parent_id) if params[:contractor][:parent_id].blank?
    respond_to do |format|
      if @contractor.update_attributes(params[:contractor])
        format.html { redirect_to contractors_url, notice: 'Контрагент сохранен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @contractor.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @contractor = Contractor.find(params[:id])
    @contractor.close

    respond_to do |format|
      format.html { redirect_to contractors_url }
      format.json { head :no_content }
    end
  end
end
