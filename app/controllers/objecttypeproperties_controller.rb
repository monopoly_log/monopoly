# -*- encoding : utf-8 -*-
class ObjecttypepropertiesController < ApplicationController


  def index
    @objecttypeproperties = Objecttypeproperty.paginate(page: params[:page], per_page: 30)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @objecttypeproperties }
    end
  end


  def show
    @objecttypeproperty = Objecttypeproperty.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @objecttypeproperty }
    end
  end


  def new
    @objecttypeproperty = Objecttypeproperty.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @objecttypeproperty }
    end
  end


  def edit
    @objecttypeproperty = Objecttypeproperty.find(params[:id])
  end


  def create
    @objecttypeproperty = Objecttypeproperty.new(params[:objecttypeproperty])

    respond_to do |format|
      if @objecttypeproperty.save
        format.html { redirect_to objecttypeproperties_url, notice: 'Objecttypeproperty добавлен.' }
        format.json { render json: @objecttypeproperty, status: :created, location: @objecttypeproperty }
      else
        format.html { render action: "new" }
        format.json { render json: @objecttypeproperty.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @objecttypeproperty = Objecttypeproperty.find(params[:id])

    respond_to do |format|
      if @objecttypeproperty.update_attributes(params[:objecttypeproperty])
        format.html { redirect_to objecttypeproperties_url, notice: 'Objecttypeproperty сохранен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @objecttypeproperty.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @objecttypeproperty = Objecttypeproperty.find(params[:id])
    @objecttypeproperty.destroy

    respond_to do |format|
      format.html { redirect_to objecttypeproperties_url }
      format.json { head :no_content }
    end
  end
end

