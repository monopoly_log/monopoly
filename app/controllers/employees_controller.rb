# -*- encoding : utf-8 -*-

class EmployeesController < ApplicationController


  def index

    @contractors = Contractor.inside.find(Department.select('distinct contractor_id').map(&:contractor_id))
    @departments = Department.visible

    unless params[:contractor].blank? 
      @contractor = Contractor.find(params[:contractor])
      unless params[:department].blank?
        @department = Department.find(params[:department])
        @employees = @department.employees.sort { |a, b| a.staff_unit.department <=> b.staff_unit.department }.paginate(page: params[:page], per_page: 20)
      else
        @employees = @contractor.employees.sort { |a, b| a.staff_unit.department <=> b.staff_unit.department }.paginate(page: params[:page], per_page: 20)
      end    
    else
      @employees = Employee.visible.joins(:staff_unit).order('staff_units.department_id').paginate(page: params[:page], per_page: 20)
    end 

    @employees = Employee.closed_employees.paginate(page: params[:page], per_page: 20) unless params[:close].blank?

    respond_to do |format|
      format.html # index.html.erb
      format.js { render json: @employees }
    end
  end


  def show
    @employee = Employee.find(params[:id])

    respond_to do |format|
      format.js {}
    end
    
  end


  def new
    @employee = Employee.new

    @contractors = Contractor.find(Department.select('distinct contractor_id').map(&:contractor_id))
    @departments = Department.visible

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @employee }
    end
  end


  def edit
    @employee = Employee.find(params[:id])

    @contractors = Contractor.find(Department.select('distinct contractor_id').map(&:contractor_id))
    @departments = Department.visible
  end


  def create
    @employee = Employee.new(params[:employee])

    @contractors = Contractor.find(Department.select('distinct contractor_id').map(&:contractor_id))
    @departments = Department.visible

    respond_to do |format|
      if @employee.save
        format.html { redirect_to employees_url, notice: 'Сотрудник добавлен.' }
        format.json { render json: @employee, status: :created, location: @employee }
      else
        format.html { render action: "new" }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @employee = Employee.find(params[:id])

    @contractors = Contractor.find(Department.select('distinct contractor_id').map(&:contractor_id))
    @departments = Department.visible

    respond_to do |format|
      if @employee.update_attributes(params[:employee])
        format.html { redirect_to employees_url, notice: 'Сотрудник сохранен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @employee = Employee.find(params[:id])
    @employee.close_employee
    
    #@employee.destroy

    respond_to do |format|
      format.html { redirect_to employees_url }
      format.json { head :no_content }
    end
  end
end

