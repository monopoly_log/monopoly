# -*- encoding : utf-8 -*-
class TransportbrandsController < ApplicationController
  # GET /transportbrands
  # GET /transportbrands.json
  def index
    #@transportbrands = Transportbrand.paginate(page: params[:page], per_page: 30).order("name")
    @transportbrands_all = Transportbrand.all
    @letters = @transportbrands_all.map{|t| t.name[0]}.uniq.sort
    unless params[:letter]
      @transportbrands = @transportbrands_all.sort_by(&:name)
    else
      @transportbrands = @transportbrands_all.find_all{|t| t.name[0] == params[:letter]}.compact
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @transportbrands }
    end
  end


  # GET /transportbrands/new
  # GET /transportbrands/new.json
  def new
    @transportbrand = Transportbrand.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @transportbrand }
    end
  end

  # GET /transportbrands/1/edit
  def edit
    @transportbrand = Transportbrand.find(params[:id])
  end

  # POST /transportbrands
  # POST /transportbrands.json
  def create
    @transportbrand = Transportbrand.new(params[:transportbrand])

    respond_to do |format|
      if @transportbrand.save
        format.html { redirect_to transportbrands_path, notice: 'Марка добавлена' }
        format.json { render json: @transportbrand, status: :created, location: @transportbrand }
      else
        format.html { render action: "new" }
        format.json { render json: @transportbrand.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /transportbrands/1
  # PUT /transportbrands/1.json
  def update
    @transportbrand = Transportbrand.find(params[:id])

    respond_to do |format|
      if @transportbrand.update_attributes(params[:transportbrand])
        format.html { redirect_to transportbrands_url, notice: 'Марка сохранена' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @transportbrand.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transportbrands/1
  # DELETE /transportbrands/1.json
  def destroy
    @transportbrand = Transportbrand.find(params[:id])
    @transportbrand.close

    respond_to do |format|
      format.html { redirect_to transportbrands_url, notice: 'Марка удалена' }
      format.json { head :no_content }
    end
  end
end
