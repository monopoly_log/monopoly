# -*- encoding : utf-8 -*-

class ContractSubjectsController < ApplicationController


  def index
 #   @contract_subjects = ContractSubject.all
    @contract_subjects_count = ContractSubject.count
    @contract_subjects = ContractSubject
          .joins(:contract_type)
          .order("contract_types.name")
          .paginate(page: params[:page], per_page: 20)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @contract_subjects }
    end
  end


  def show
    @contract_subject = ContractSubject.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @contract_subject }
    end
  end


  def new
    @contract_subject = ContractSubject.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @contract_subject }
    end
  end


  def edit
    @contract_subject = ContractSubject.find(params[:id])
  end


  def create
    @contract_subject = ContractSubject.new(params[:contract_subject])

    respond_to do |format|
      if @contract_subject.save
        format.html { redirect_to contract_subjects_url, notice: 'Предмет договора добавлен.' }
        format.json { render json: @contract_subject, status: :created, location: @contract_subject }
      else
        format.html { render action: "new" }
        format.json { render json: @contract_subject.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @contract_subject = ContractSubject.find(params[:id])

    respond_to do |format|
      if @contract_subject.update_attributes(params[:contract_subject])
        format.html { redirect_to contract_subjects_url, notice: 'Предмет договора сохранен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @contract_subject.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @contract_subject = ContractSubject.find(params[:id])
    @contract_subject.close
    #@contract_subject.destroy

    respond_to do |format|
      format.html { redirect_to contract_subjects_url }
      format.json { head :no_content }
    end
  end
end

