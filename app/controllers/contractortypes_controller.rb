# -*- encoding : utf-8 -*-
class ContractortypesController < ApplicationController


  def index
    @contractortypes = Contractortype.paginate(page: params[:page], per_page: 30)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @contractortypes }
    end
  end



  def new
    @contractortype = Contractortype.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @contractortype }
    end
  end


  def edit
    @contractortype = Contractortype.find(params[:id])
  end


  def create
    @contractortype = Contractortype.new(params[:contractortype])
    @contractortype.id ||= SecureRandom.uuid
    respond_to do |format|
      if @contractortype.save
        format.html { redirect_to edit_contractortype_url(@contractortype), notice: 'Contractortype добавлен.' }
        format.json { render json: @contractortype, status: :created, location: @contractortype }
      else
        format.html { render action: "new" }
        format.json { render json: @contractortype.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @contractortype = Contractortype.find(params[:id])

    respond_to do |format|
      if @contractortype.update_attributes(params[:contractortype])
        format.html { redirect_to edit_contractortype_url(@contractortype), notice: 'Contractortype сохранен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @contractortype.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @contractortype = Contractortype.find(params[:id])
    @contractortype.close

    respond_to do |format|
      format.html { redirect_to contractortypes_url }
      format.json { head :no_content }
    end
  end
end
