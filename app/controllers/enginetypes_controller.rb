# -*- encoding : utf-8 -*-
class EnginetypesController < ApplicationController
  # GET /enginetypes
  # GET /enginetypes.json
  def index
    @enginetypes = Enginetype.paginate(page: params[:page], per_page: 20)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @enginetypes }
    end
  end


  # GET /enginetypes/new
  # GET /enginetypes/new.json
  def new
    @enginetype = Enginetype.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @enginetype }
    end
  end

  # GET /enginetypes/1/edit
  def edit
    @enginetype = Enginetype.find(params[:id])
  end

  # POST /enginetypes
  # POST /enginetypes.json
  def create
    @enginetype = Enginetype.new(params[:enginetype])

    respond_to do |format|
      if @enginetype.save
        format.html { redirect_to enginetypes_path, notice: 'Тип двигателя добавлен' }
        format.json { render json: @enginetype, status: :created, location: @enginetype }
      else
        format.html { render action: "new" }
        format.json { render json: @enginetype.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /enginetypes/1
  # PUT /enginetypes/1.json
  def update
    @enginetype = Enginetype.find(params[:id])

    respond_to do |format|
      if @enginetype.update_attributes(params[:enginetype])
        format.html { redirect_to enginetypes_path, notice: 'Тип двигателя сохранен' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @enginetype.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /enginetypes/1
  # DELETE /enginetypes/1.json
  def destroy
    @enginetype = Enginetype.find(params[:id])
    @enginetype.close

    respond_to do |format|
      format.html { redirect_to enginetypes_url }
      format.json { head :no_content }
    end
  end
end
