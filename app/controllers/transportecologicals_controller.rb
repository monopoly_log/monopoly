# -*- encoding : utf-8 -*-
class TransportecologicalsController < ApplicationController


  def index

    @transportecologicals = Transportecological.paginate(page: params[:page], per_page: 30).order('name')

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @transportecologicals }
    end
  end



  def new
    @transportecological = Transportecological.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @transportecological }
    end
  end

  # GET /transportecologicals/1/edit
  def edit
    @transportecological = Transportecological.find(params[:id])
  end

  # POST /transportecologicals
  # POST /transportecologicals.json
  def create
    @transportecological = Transportecological.new(params[:transportecological])

    respond_to do |format|
      if @transportecological.save
        format.html { redirect_to transportecologicals_path, notice: 'Экологический класс добавлен' }
        format.json { render json: @transportecological, status: :created, location: @transportecological }
      else
        format.html { render action: "new" }
        format.json { render json: @transportecological.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /transportecologicals/1
  # PUT /transportecologicals/1.json
  def update
    @transportecological = Transportecological.find(params[:id])

    respond_to do |format|
      if @transportecological.update_attributes(params[:transportecological])
        format.html { redirect_to transportecologicals_url, notice: 'Экологический класс сохранен' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @transportecological.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transportecologicals/1
  # DELETE /transportecologicals/1.json
  def destroy
    @transportecological = Transportecological.find(params[:id])
    @transportecological.close

    respond_to do |format|
      format.html { redirect_to transportecologicals_url }
      format.json { head :no_content }
    end
  end
end
