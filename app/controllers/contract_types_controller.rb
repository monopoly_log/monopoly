# -*- encoding : utf-8 -*-


class ContractTypesController < ApplicationController


  def index
 #   @contract_types = ContractType.all
    @contract_types_count = ContractType.count
    @contract_types = ContractType.order(:name).paginate(page: params[:page], per_page: 20)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @contract_types }
    end
  end


  def show
    @contract_type = ContractType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @contract_type }
    end
  end


  def new
    @contract_type = ContractType.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @contract_type }
    end
  end


  def edit
    @contract_type = ContractType.find(params[:id])
  end


  def create
    @contract_type = ContractType.new(params[:contract_type])

    respond_to do |format|
      if @contract_type.save
        format.html { redirect_to contract_types_url, notice: 'Тип договора добавлен.' }
        format.json { render json: @contract_type, status: :created, location: @contract_type }
      else
        format.html { render action: "new" }
        format.json { render json: @contract_type.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @contract_type = ContractType.find(params[:id])

    respond_to do |format|
      if @contract_type.update_attributes(params[:contract_type])
        format.html { redirect_to contract_types_url, notice: 'Тип договора сохранен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @contract_type.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @contract_type = ContractType.find(params[:id])
    @contract_type.close
    #@contract_type.destroy

    respond_to do |format|
      format.html { redirect_to contract_types_url }
      format.json { head :no_content }
    end
  end
end

