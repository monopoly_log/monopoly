# -*- encoding : utf-8 -*-
class TransportsController < ApplicationController

  def index
    if params[:showdel] == "true"
      @transports = Transport.order('reg_num desc').paginate(page: params[:page], per_page: 30)
    else
      @transports = Transport.visible.order('reg_num desc').paginate(page: params[:page], per_page: 30)
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @transports }
    end
  end



  def new
    @transport = Transport.new

    respond_to do |format|
      format.html # new.html.erb
      #format.json { render json: @transport }
    end
  end


  def edit
    @transport = Transport.find(params[:id])
    
    if params[:copy]
      @copy = Marshal.load(Marshal.dump(@transport.attributes))
      @copy.delete('id')
      @copy.delete('created_at')
      @copy.delete('updated_at')
      @transport_copy = Transport.create(@copy)
      @transport_copy.transportmodels << @transport.transportmodel if @transport.transportmodel
      @transport_copy.transportexploitations << @transport.transportexploitation if @transport.transportexploitation
      @transport_copy.enginemodels << @transport.enginemodel if @transport.enginemodel
      @transport_copy.transportcolors << @transport.transportcolor if @transport.transportcolor
      @transport = @transport_copy
    end
    transport_stats(@transport)
    
    respond_to do |format|
      format.html # new.html.erb
      #format.json { render json: @transport }
    end
  end


  def create
    @transport = Transport.new(params[:transport])

    respond_to do |format|
      if @transport.save
        format.html { redirect_to edit_transport_path(@transport), notice: 'ТС сохранено.' }
      # format.json { render json: @transport, status: :created, location: @transport }
      else
       # format.js   {}
        transport_stats(@transport)
        flash[:notice] = "Error"
        format.html { render action: "new" }
       # format.json { render json: @transport.errors, status: :unprocessable_entity }
      end
      #render action: "update"
    end
  end


  def update
    @transport = Transport.find(params[:id])
    
    respond_to do |format|
      if @transport.update_attributes(params[:transport])
         transport_stats(@transport)
         flash[:notice] = "Ok"
         format.html { redirect_to edit_transport_url(@transport), notice: 'ТС сохранено.' }
#        format.json { head :no_content }
      else
        format.js   {}
        transport_stats(@transport)
         flash[:notice] = "Error"
        format.html { render action: "edit" }
#        format.json { render json: @transport.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @transport = Transport.find(params[:id])
    current_user.admin ? @transport.destroy : @transport.close

    respond_to do |format|
      format.html { redirect_to transports_url }
      format.json { head :no_content }
    end
  end

  def remove_doc
    @document = Document.find(params[:id])
    tmp = @document
    tmp_tr = tmp.transport
    current_user.admin ? @document.destroy : @document.close

    respond_to do |format|
      format.html { redirect_to transports_url, notice: "Документ #{tmp.documentkind.name} для #{tmp_tr.reg_num} удален" }
      format.json { head :no_content }
    end
  end




  def transport_stats(transport)
    @transportmodel = transport.transportmodel if transport.transportmodel #модель ТС
    if @transportmodel
      @transportmodel_name = @transportmodel.name
      @transportbrand = @transportmodel.transportbrand.name # марка ТС
      @transportengine = @transportmodel.enginetypes.first if @transportmodel.is_engine #наличие двигателя
      @transportloadings = (transport.tsloadings.any? ? transport.tsloadings : @transportmodel.transportloadings) if @transportmodel.is_loading #наличие погрузки
      @transportfuellings = (transport.tstanks.any? ? transport.tstanks : @transportmodel.transporttanks) if @transportmodel.is_fuelling #наличие топл. бака
      @transportkind = @transportmodel.transportkinds.first.name if @transportmodel.transportkinds.any? #Вид ТС
      @transporttype = @transportmodel.transportkinds.first.transporttype.name if @transportkind # Тип ТС
      @transportcategory = @transportmodel.transportkind.transporttype.transportcategory.name if @transporttype # Категория ТС
      @enginetype = @transportmodel.enginetypes.first.name if @transportmodel.enginetypes.any?
    end
    @transportexploitation = transport.transportexploitation.name if transport.transportexploitation
    @enginemodel = transport.enginemodel if transport.enginemodel #модель двигателя
    if @enginemodel
      @enginemodel_name = @enginemodel.name
      @enginemodel_power = @enginemodel.power
      @enginemodel_power_kvt = @enginemodel.power_kvt
      @enginemodel_displacement = @enginemodel.displacement
    end
    @transportcolor = transport.transportcolor.name if transport.transportcolor #цвет тс
  end




  def select_brand
   respond_to do |format|
      format.js   {}
   end
     @transport = Transport.find(params[:id])
     
#    @transportbrand = Transportbrand.find(params[:brand][:id])
#    @transport.transportbrands.delete( @transport.transportbrands )
#    @transport.transportmodels.delete( @transport.transportmodels )
#    @transport.transportbrands << @transportbrand

    @transportmodel = Transportmodel.find(params[:model])
    @transport.transportmodels.delete( @transport.transportmodels )
    @transport.transportmodels << @transportmodel
    transport_stats(@transport)

    flash[:notice] = 'Модель ТС сохранена.'
    render action: "complete"

  end



  def select_category
   respond_to do |format|
      format.js   {}
   end
    @transport = Transport.find(params[:id])
    @transportcategory = Transportcategory.find(params[:category][:id])
    @transport.transportcategories.delete( @transport.transportcategories )
    @transport.transportcategories << @transportcategory
    @div = "select_category#{@transport.id}"
    flash[:notice] = 'Категория сохранена'

  end


  def select_color
   respond_to do |format|
      format.js   {}
   end
    @transport = Transport.find(params[:id])

    @transportcolor = Transportcolor.find(params[:color][:id])
    @transport.transportcolors.delete( @transport.transportcolors )
    @transport.transportcolors << @transportcolor
    @div = "popup_content"
    transport_stats(@transport)
    flash[:notice] = 'Цвет ТС выбран'
    render action: "complete"
  end


  def select_exploitation
   respond_to do |format|
      format.js   {}
   end
    @transport = Transport.find(params[:id])
    
    @exploitation = Transportexploitation.find(params[:exploitation][:id])
    @transport.transportexploitations.delete( @transport.transportexploitations )
    @transport.transportexploitations << @exploitation
    @div = "popup_content"
    transport_stats(@transport)
    flash[:notice] = 'Форма эксплуатации ТС выбрана'
    render action: "complete"
  end

  def select_enginemodel
   respond_to do |format|
      format.js   {}
   end
    @transport = Transport.find(params[:id])
    @enginemodel = Enginemodel.find(params[:enginemodel][:id])

    @transport.enginemodels.delete( @transport.enginemodels )
    @transport.enginemodels << @enginemodel
    transport_stats(@transport)
    flash[:notice] = "сохранена модель двигателя: #{@enginemodel.name}"
    render action: "complete"
  end

  def select_loading
    respond_to do |format|
      format.js   {}
    end
    @transport = Transport.find(params[:id])
    @transport.tsloadings.create(loading: params[:tsloading][:loading])
    @transport.reload
    transport_stats(@transport)
    flash[:notice] = 'Тип загрузки добавлен'
    render action: "complete"
  end

  def del_loading_from_transport
    @transport = Transport.find(params[:id])
    Tsloading.find(params[:loading]).delete
    transport_stats(@transport)
    flash[:notice] = 'Тип загрузки удален'
    render action: "complete"
  end

  def select_tank
    respond_to do |format|
      format.js   {}
    end
    @transport = Transport.find(params[:id])
    @transport.tstanks.create( v_tank: params[:tstank][:v_tank], v_tank_min: params[:tstank][:v_tank_min] )
    @transport.reload
    transport_stats(@transport)
    flash[:notice] = 'Топливный бак добавлен'
    render action: "complete"
  end

  def del_tank_from_transport
    @transport = Transport.find(params[:id])
    Tstank.find(params[:tank]).delete
    transport_stats(@transport)
    flash[:notice] = 'Топливный бак удален'
    render action: "complete"
  end



  def show_part
   respond_to do |format|
      format.js   {}
   end
   @part = params[:part]
   @transport = Transport.find(params[:id]) if params[:id]
  end


  def complete

  end

end
