# -*- encoding : utf-8 -*-
class UsergroupsController < ApplicationController

  def index
    if params[:role]
      @role = Role.find(params[:role])
      @usergroups = @role.usergroups.paginate(page: params[:page], per_page: 30)
      @msg = "Роль #{@role.name}"
    elsif params[:functional]
      @functional = Functional.find(params[:functional])
      @usergroups = @functional.usergroups.paginate(page: params[:page], per_page: 30)
      @msg = "Функционал #{@functional.name}"
    elsif params[:user]
      @user = User.find(params[:user])
      @usergroups = @user.usergroups
      @msg = "Пользователь #{@user.email}"
    else
      @usergroups = Usergroup.paginate(page: params[:page], per_page: 30)
    end
    

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @usergroups }
    end
  end


  def show
    @usergroup = Usergroup.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @usergroup }
    end
  end


  def new
    @usergroup = Usergroup.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @usergroup }
    end
  end


  def edit
    @usergroup = Usergroup.find(params[:id])
  end


  def create
    @usergroup = Usergroup.new(params[:usergroup])

    respond_to do |format|
      if @usergroup.save
        format.html { redirect_to usergroups_url, notice: 'Группа создана.' }
        format.json { render json: @usergroup, status: :created, location: @usergroup }
      else
        format.html { render action: "new" }
        format.json { render json: @usergroup.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @usergroup = Usergroup.find(params[:id])

    respond_to do |format|
      if @usergroup.update_attributes(params[:usergroup])
        format.html { redirect_to usergroups_url, notice: 'Группа соханена.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @usergroup.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @usergroup = Usergroup.find(params[:id])
    current_user.admin ? @usergroup.delete : @usergroup.close

    respond_to do |format|
      format.html { redirect_to usergroups_url }
      format.json { head :no_content }
    end
  end

  def select_role
   respond_to do |format|
      format.js   {}
   end
  @group = Usergroup.find(params[:id])
  @role = Role.find(params[:role][:id])
  @group.roles << @role
  @div = "select_role#{@group.id}"

end

def del_role_from_group
   respond_to do |format|
      format.js   {}
   end
  @group = Usergroup.find(params[:id])
  @role = Role.find(params[:role])
  @group.roles.delete(@role)
  @div = "select_role#{@group.id}"
  flash.now[:notice] = 'Удалено'
end


def select_functional
   respond_to do |format|
      format.js   {}
   end
  @group = Usergroup.find(params[:id])
  @functional = Functional.find(params[:functional][:id])
  @group.functionals << @functional
  @div = "select_functional#{@group.id}"

end

def del_functional_from_group
   respond_to do |format|
      format.js   {}
   end
  @group = Usergroup.find(params[:id])
  @functional = Functional.find(params[:functional])
  @group.functionals.delete(@functional)
  @div = "select_functional#{@group.id}"
  flash[:notice] = 'Удалено'
end



end
