# -*- encoding : utf-8 -*-


class UnvTypesController < ApplicationController


  def index
 #   @unv_types = UnvType.all
    @unv_types_count = UnvType.count
    @unv_types = UnvType.paginate(page: params[:page], per_page: 20)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @unv_types }
    end
  end


  def show
    @unv_type = UnvType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @unv_type }
    end
  end


  def new
    @unv_type = UnvType.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @unv_type }
    end
  end


  def edit
    @unv_type = UnvType.find(params[:id])
  end


  def create
    @unv_type = UnvType.new(params[:unv_type])

    respond_to do |format|
      if @unv_type.save
        format.html { redirect_to unv_types_url, notice: 'Тип недоступности добавлен.' }
        format.json { render json: @unv_type, status: :created, location: @unv_type }
      else
        format.html { render action: "new" }
        format.json { render json: @unv_type.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @unv_type = UnvType.find(params[:id])

    respond_to do |format|
      if @unv_type.update_attributes(params[:unv_type])
        format.html { redirect_to unv_types_url, notice: 'Тип недоступности сохранен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @unv_type.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @unv_type = UnvType.find(params[:id])
    @unv_type.close
    #@unv_type.destroy

    respond_to do |format|
      format.html { redirect_to unv_types_url }
      format.json { head :no_content }
    end
  end
end

