# -*- encoding : utf-8 -*-


class DepartmentsController < ApplicationController


  def index

    @contractors = Contractor.find(Department.select('distinct contractor_id').map(&:contractor_id))
    
    if params[:contractor] and !params[:contractor][:id].blank?
      @departments = Department.where('contractor_id = ?',  params[:contractor][:id]).joins(:contractor).order('contractors.name').paginate(page: params[:page], per_page: 10).roots
    else
      @departments = Department.joins(:contractor).order('contractors.name').paginate(page: params[:page], per_page: 10).roots
    end    

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @departments }
    end
  end


  def show
    @department = Department.find(params[:id])

    respond_to do |format|
      format.js { }
    end
  end


  def new
    @department = Department.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @department }
    end
  end


  def edit
    @department = Department.find(params[:id])
  end


  def create
    @department = Department.new(params[:department])

    respond_to do |format|
      if @department.save
        format.html { redirect_to departments_url, notice: 'Подразделение добавлено' }
        format.json { render json: @department, status: :created, location: @department }
      else
        format.html { render action: "new" }
        format.json { render json: @department.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @department = Department.find(params[:id])

    respond_to do |format|
      if @department.update_attributes(params[:department])
        format.html { redirect_to departments_url, notice: 'Подразделение сохранено' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @department.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @department = Department.find(params[:id])
    @department.close

    respond_to do |format|
      format.html { redirect_to departments_url }
      format.json { head :no_content }
    end
  end
end

