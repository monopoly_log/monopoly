# -*- encoding : utf-8 -*-


class EmployeeHisroriesController < ApplicationController


  def index
 #   @employee_hisrories = EmployeeHisrory.all
    @employee_hisrories_count = EmployeeHisrory.count
    @employee_hisrories = EmployeeHisrory.paginate(page: params[:page], per_page: 20)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @employee_hisrories }
    end
  end


  def show
    @employee_hisrory = EmployeeHisrory.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @employee_hisrory }
    end
  end


  def new
    @employee_hisrory = EmployeeHisrory.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @employee_hisrory }
    end
  end


  def edit
    @employee_hisrory = EmployeeHisrory.find(params[:id])
  end


  def create
    @employee_hisrory = EmployeeHisrory.new(params[:employee_hisrory])

    respond_to do |format|
      if @employee_hisrory.save
        format.html { redirect_to employee_hisrories_url, notice: 'Employee hisrory добавлен.' }
        format.json { render json: @employee_hisrory, status: :created, location: @employee_hisrory }
      else
        format.html { render action: "new" }
        format.json { render json: @employee_hisrory.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @employee_hisrory = EmployeeHisrory.find(params[:id])

    respond_to do |format|
      if @employee_hisrory.update_attributes(params[:employee_hisrory])
        format.html { redirect_to employee_hisrories_url, notice: 'Employee hisrory сохранен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @employee_hisrory.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @employee_hisrory = EmployeeHisrory.find(params[:id])
    @employee_hisrory.close
    #@employee_hisrory.destroy

    respond_to do |format|
      format.html { redirect_to employee_hisrories_url }
      format.json { head :no_content }
    end
  end
end

