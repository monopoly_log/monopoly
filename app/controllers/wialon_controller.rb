# -*- encoding : utf-8 -*-
class WialonController < ApplicationController
  require 'timeout'
  @@eid = nil
  @@i = 0

  skip_before_filter :authenticate_user!, :only => [:index, :time]
  def index

    require 'net/http'
    require 'json'

    if params[:reportTemplateId] and params[:reportObjectId] and params[:from] and params[:to]

      wialon_auth unless @@eid


      template = params[:reportTemplateId]
      obj = params[:reportObjectId]

      s_date = params[:from]
      e_date = params[:to]

      @@i +=1

      core = 'report/exec_report'
      par = "{'reportResourceId':1222731,'reportTemplateId':#{template},'reportObjectId':#{obj},'reportObjectSecId':1,'interval':{'from':#{s_date},'to':#{e_date},'flags':0}}"

      #begin
        #Timeout.timeout(90) do
         resp = wialon_request core, par, @@eid
         if resp['error']
            wialon_auth
            resp = wialon_request core, par, @@eid
         end
       #end
      #rescue Timeout::Error
#         resp = wialon_request core, par, @@eid
#         if resp['error']
#            wialon_auth
#            resp = wialon_request core, par, @@eid
#         end
      #end

      File.open('log/report.wialon', 'a'){ |file| file.write "#{@@i}:  #{Time.now} "; file.write(resp['error'] ? resp : "");  file.write"|||||||| eid: #{@@eid} tmpl: #{template} obj: #{obj} sdate: #{s_date} edate: #{e_date} \n"}
      render text: "#{resp}"
    else
      render text: "incorrect number of params"
    end

  end


def time
 begin
   rnd = rand(20)
  Timeout.timeout(5) do
   sleep(rnd)
   render text: rnd.to_s
  end
  rescue Timeout::Error
  render text: rnd.to_s
 end 
end


private

def wialon_auth
    resp = wialon_request('core/login',"{'user':'monopoly','password':'123456'}","")
    @@eid = resp['eid']
end

def wialon_request svc, params, sid
  http = Net::HTTP.new("hst-api.wialon.com", 443)

  http.use_ssl = true
  http.verify_mode = OpenSSL::SSL::VERIFY_NONE

  request = Net::HTTP::Post.new("/wialon/ajax.html?svc=#{svc}&params=#{params}&sid=#{sid}")
  #http.set_debug_output($stdout)

  request["Content-Type"] = "application/x-www-form-urlencoded"

  response = http.request(request)
  JSON.parse(response.body)
end


end
