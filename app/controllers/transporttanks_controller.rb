# -*- encoding : utf-8 -*-
class TransporttanksController < ApplicationController
  # GET /transporttanks
  # GET /transporttanks.json
  def index
    if params[:model]
      @transportmodel = Transportmodel.find params[:model]
      @transporttanks = @transportmodel.transporttanks.paginate(page: params[:page], per_page: 30).joins(:transportmodel).order('transportmodels.name')

    else
      @transporttanks = Transporttank.paginate(page: params[:page], per_page: 30).joins(:transportmodel).order('transportmodels.name')

    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @transporttanks }
    end
  end


  # GET /transporttanks/new
  # GET /transporttanks/new.json
  def new
    @transportmodel = Transportmodel.find params[:model] if params[:model]
    @transporttank = Transporttank.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @transporttank }
    end
  end

  # GET /transporttanks/1/edit
  def edit
    @transporttank = Transporttank.find(params[:id])
    @transportmodel = @transporttank.transportmodel
  end

  # POST /transporttanks
  # POST /transporttanks.json
  def create
    @transporttank = Transporttank.new(params[:transporttank])
    @transportmodel = @transporttank.transportmodel
    respond_to do |format|
      if @transporttank.save
        format.html { redirect_to transporttanks_url(model: @transportmodel.id), notice: 'Топливный бак добавлен' }
        format.json { render json: @transporttank, status: :created, location: @transporttank }
      else
        format.html { render action: "new" }
        format.json { render json: @transporttank.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /transporttanks/1
  # PUT /transporttanks/1.json
  def update
    @transporttank = Transporttank.find(params[:id])
    @transportmodel = @transporttank.transportmodel
    respond_to do |format|
      if @transporttank.update_attributes(params[:transporttank])
        format.html { redirect_to transporttanks_url(model: @transportmodel.id), notice: 'Топливный бак сохранен' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @transporttank.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transporttanks/1
  # DELETE /transporttanks/1.json
  def destroy
    @transporttank = Transporttank.find(params[:id])
    transportmodel = @transporttank.transportmodel
    @transporttank.close

    respond_to do |format|
      format.html { redirect_to transporttanks_url(model: transportmodel ) }
      format.json { head :no_content }
    end
  end
end
