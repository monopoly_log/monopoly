# -*- encoding : utf-8 -*-
class TransportmodelsController < ApplicationController


  def index
    if params[:brand]
      @transportbrand = Transportbrand.find(params[:brand])
      @transportmodels = @transportbrand.transportmodels.paginate(page: params[:page], per_page: 20)
    else
      @transportmodels = Transportmodel.order(:transportbrand_id).paginate(page: params[:page], per_page: 20)
    end


    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @transportmodels }
    end
  end


  def new
    
    @transportbrand = Transportbrand.find(params[:brand]) if params[:brand]
    @transportmodel = Transportmodel.new

    @transportkind = @transportmodel.transportkind
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @transportmodel }
    end
  end


  def edit
    @transportmodel = Transportmodel.find(params[:id])
    @transportbrand = @transportmodel.transportbrand
    @transportkind = @transportmodel.transportkind

  end


  def create
    @transportmodel = Transportmodel.new(params[:transportmodel])
    #@transportbrand = @transportmodel.transportbrand
    #@transportmodel.id = SecureRandom.uuid
    params[:transportmodel].delete(:transportbrand_id) if params[:transportmodel][:transportbrand_id] == ''

    respond_to do |format|
      if @transportmodel.save
        #@transportmodel.reload
        format.html { redirect_to edit_transportmodel_path(@transportmodel), notice: 'Модель сохранена' }
        #format.html { redirect_to transportmodels_path(brand: @transportbrand), notice: 'Модель сохранена' }
        format.json { render json: @transportmodel, status: :created, location: @transportmodel }
      else
        format.html { render action: "new" }
        format.json { render json: @transportmodel.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @transportmodel = Transportmodel.find(params[:id])
    @transportbrand = @transportmodel.transportbrand
    respond_to do |format|
      if @transportmodel.update_attributes(params[:transportmodel])
        format.html { redirect_to edit_transportmodel_path(@transportmodel), notice: 'Модель сохранена' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @transportmodel.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @transportmodel = Transportmodel.find(params[:id])
    transportbrand = @transportmodel.transportbrand_id
    @transportmodel.close

    respond_to do |format|
      format.html { redirect_to transportmodels_url( brand: transportbrand ) }
      format.json { head :no_content }
    end
  end


  def select_loading
    respond_to do |format|
      format.js   {}
    end
    @transportmodel = Transportmodel.find(params[:id])
    @transportkind = @transportmodel.transportkind
    @transportbrand = Transportbrand.find(@transportmodel.transportbrand_id)
    @transportloading = Transportloading.find(params[:transportloading][:id])
    @transportmodel.transportloadings << @transportloading
    @div = "popup_content"
    flash[:notice] = 'Тип загрузки добавлен'
  end

  def del_loading_from_model
   respond_to do |format|
      format.js   {}
   end
    @transportmodel = Transportmodel.find(params[:id])
    @transportloading = Transportloading.find(params[:loading])
    @transportmodel.transportloadings.delete(@transportloading)
    @div = "popup_content"
    flash[:notice] = 'Тип загрузки удален'
  end


  def select_tank
    respond_to do |format|
      format.js   {}
    end
    @transportmodel = Transportmodel.find(params[:id])
    @transportbrand = Transportbrand.find(@transportmodel.transportbrand_id)
    @transportkind = @transportmodel.transportkind
    @transportmodel.transporttanks.create(v_tank: params[:v_tank], v_tank_min: params[:v_tank_min])
    @transportmodel.transporttanks.reload
    @div = "popup_content"
    flash[:notice] = "Бак ТС изменен"
  end


  def select_engine
    respond_to do |format|
      format.js   {}
    end
    @transportmodel = Transportmodel.find(params[:id])
    @transportbrand = Transportbrand.find(@transportmodel.transportbrand_id)
    @transportkind = @transportmodel.transportkind
    @enginetype = Enginetype.find(params[:enginetype][:id])
    @transportmodel.enginetypes.delete(@transportmodel.enginetypes)
    @transportmodel.enginetypes << @enginetype
    @div = "popup_content"
    @transportbrand = Transportbrand.find(@transportmodel.transportbrand_id)
    flash[:notice] = "Тип двигателя ТС сохранен."
  end

  def del_tank_from_model
   respond_to do |format|
      format.js   {}
   end
    @transportmodel = Transportmodel.find(params[:id])
    Transporttank.find(params[:tank]).delete
    
    @div = "popup_content"
    flash[:notice] = 'Бак ТС удален'
  end

  def select_cat_type_kind
   respond_to do |format|
      format.js   {}
   end
   if params[:id]
    @transportmodel = Transportmodel.find(params[:id])
    @transportbrand = Transportbrand.find(@transportmodel.transportbrand_id)
    @transportkind = @transportmodel.transportkind
   end

   if params[:transportkind]
    @transportkind = Transportkind.find(params[:transportkind])
    @transportmodel.transportkinds.delete_all
    @transportmodel.transportkinds << @transportkind
    flash[:notice] = "Вид ТС выбран: #{@transportkind.name}."
   end
  end



  def show_part
   respond_to do |format|
      format.js   {}
   end
   @part = params[:part]
   @div = "popup_content"
   @transportmodel = Transportmodel.find(params[:id]) if params[:id]
  end

  def chng_attr
   @transportmodel = Transportmodel.find(params[:id])
   @out = params[:attr]
   @transportmodel.toggle!(@out.to_sym)
   respond_to do |format|
      format.js   {}
   end

  end


end
