# -*- encoding : utf-8 -*-
class TransportcolorsController < ApplicationController


  def index
    @transportcolors = Transportcolor.paginate(page: params[:page], per_page: 30)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @transportcolors }
    end
  end



  def new
    @transportcolor = Transportcolor.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @transportcolor }
    end
  end



  def edit
    @transportcolor = Transportcolor.find(params[:id])
  end



  def create
    @transportcolor = Transportcolor.new(params[:transportcolor])

    respond_to do |format|
      if @transportcolor.save
        format.html { redirect_to transportcolors_path, notice: 'Цвет добавлен' }
        format.json { render json: @transportcolor, status: :created, location: @transportcolor }
      else
        format.html { render action: "new" }
        format.json { render json: @transportcolor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /transportcolors/1
  # PUT /transportcolors/1.json
  def update
    @transportcolor = Transportcolor.find(params[:id])

    respond_to do |format|
      if @transportcolor.update_attributes(params[:transportcolor])
        format.html { redirect_to transportcolors_path, notice: 'Цвет сохранен' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @transportcolor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transportcolors/1
  # DELETE /transportcolors/1.json
  def destroy
    @transportcolor = Transportcolor.find(params[:id])
    @transportcolor.close

    respond_to do |format|
      format.html { redirect_to transportcolors_url, notice: 'Цвет удален' }
      format.json { head :no_content }
    end
  end
end
