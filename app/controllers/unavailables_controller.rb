# -*- encoding : utf-8 -*-
class UnavailablesController < ApplicationController
  $INTERVAL = 5000

  #around_filter :silence_logs, only: :upd


  def logger
    params[:action] == 'upd' ? nil : Rails.logger
  end

  def index

    @unavailables = Unavailable.getdata(current_user.id)
    @col_names =  @unavailables[0].keys
    
    @unavailables.map!{|u| u.values.map!{|u2| u2.to_s}}

    @search = params[:search] unless params[:search].blank?
    @col_ind = params[:col_ind] unless params[:col_ind].blank?
    @filter = params[:filter] unless params[:filter].blank?
    @auto = params[:auto] unless params[:auto].blank?


    if @search and @col_ind
      @search_arr = @search[0..-2].split(',').compact
      @tmp = []
      @search_arr.each do |s|
        @tmp += @unavailables.find_all{ |u| u[@col_ind.to_i] == s.strip.delete("'") }.compact
      end
      @unavailables = @tmp
    elsif @filter and @col_ind
      @ind = @col_ind.to_i
      if @filter == 'Не пусто'
        if @ind == 12
          @tmp = @unavailables
          @unavailables = @tmp.find_all{ |u| !( !u[@ind].blank? and u[@ind].index('#') ? u[@ind][0..u[@ind].index('#')-1].strip.delete("'")  : u[@ind].strip.delete("'") ).blank? }.compact
          @unavailables += @tmp.find_all{ |u| !( !u[13].blank? and u[13].index('#') ? u[13][0..u[13].index('#')-1].strip.delete("'")  : u[13].strip.delete("'") ).blank? }.compact
          @unavailables.uniq!
        else
        @unavailables = @unavailables.find_all{ |u| !( !u[@ind].blank? and u[@ind].index('#') ? u[@ind][0..u[@ind].index('#')-1].strip.delete("'") : u[@ind].strip.delete("'") ).blank? }.compact
        end
        @un_count = @unavailables.size
      else
        @unavailables = @unavailables.find_all{ |u| ( !u[@ind].blank? and u[@ind].index('#') ? u[@ind][0..u[@ind].index('#')-1].strip.delete("'") : u[@ind].strip.delete("'") ) == params[:filter]}.compact
      end
      @un_count = @unavailables.size
    else
      @un_count = @unavailables.size
      @unavailables = @unavailables.paginate(page: params[:page].blank? ? 1 : params[:page], per_page: 15)
    end

    respond_to do |format|
      format.html # index.html.erb
      format.js { }
    end
  end

  def ol
    @unavailables = Unavailable.getdata_ol    
    
    @col_names =  @unavailables[0].keys
    @unavailables.map!{|u| u.values.map!{|u2| u2.to_s}}

    @search = params[:search] unless params[:search].blank?
    @col_ind = params[:col_ind] unless params[:col_ind].blank?
    @filter = params[:filter] unless params[:filter].blank?
    @auto = params[:auto] unless params[:auto].blank?

    if @search and @col_ind
      @search_arr = @search[0..-2].split(',').compact
      @tmp = []
      @search_arr.each do |s|
        @tmp += @unavailables.find_all{ |u| u[@col_ind.to_i] == s.strip.delete("'") }.compact
      end
      @unavailables = @tmp
      @un_count = @unavailables.size
    elsif @filter and @col_ind
      @ind = @col_ind.to_i
      if @filter == 'Не пусто'
        if @ind == 4
          @tmp = @unavailables
          @unavailables = @tmp.find_all{ |u| !( !u[@ind].blank? and u[@ind].index('#') ? u[@ind][0..u[@ind].index('#')-1].strip.delete("'")  : u[@ind].strip.delete("'") ).blank? }.compact
          @unavailables += @tmp.find_all{ |u| !( !u[5].blank? and u[5].index('#') ? u[5][0..u[5].index('#')-1].strip.delete("'")  : u[5].strip.delete("'") ).blank? }.compact
          @unavailables.uniq!
        else
        @unavailables = @unavailables.find_all{ |u| !( !u[@ind].blank? and u[@ind].index('#') ? u[@ind][0..u[@ind].index('#')-1].strip.delete("'") : u[@ind].strip.delete("'") ).blank? }.compact
        end
      else
        @unavailables = @unavailables.find_all{ |u| ( !u[@ind].blank? and u[@ind].index('#') ? u[@ind][0..u[@ind].index('#')-1].strip.delete("'") : u[@ind].strip.delete("'") ) == params[:filter]}.compact
      end
      @un_count = @unavailables.size
    else
      @un_count = @unavailables.size
      @unavailables = @unavailables.paginate(page: params[:page].blank? ? 1 : params[:page], per_page: 15) unless @from and @to
    end


    respond_to do |format|
      format.html # index.html.erb
      format.js { }
    end
  end

  def connections

    @search = params[:search] unless params[:search].blank?
    @col_ind = params[:col_ind] unless params[:col_ind].blank?
    @filter = params[:filter] unless params[:filter].blank?
    @auto = params[:auto] unless params[:auto].blank?

    @from = params[:from].blank? ? DateTime.new(Time.now.year, Time.now.month, 1, 0, 0, 0).to_time.to_i : unix_time(params[:from])
    @to = params[:to].blank? ? DateTime.new(Time.now.year, Time.now.month, Time.now.day, 23, 59, 59).to_time.to_i : unix_time(params[:to])

    @from, @to = @to, @from if @to < @from

    @unavailables = Unavailable.connection_request(@from, @to)
    @col_names =  @unavailables[0].try(:keys)
    @col_names << 'Коммент.' if @col_names
    @unavailables.map!{|u| u.values}
    @unavailables = @unavailables.find_all { |u| visible_connection?(u) }.compact
    

    if @search and @col_ind
      @search_arr = @search[0..-2].split(',').compact
      @tmp = []
      @search_arr.each do |s|
        @tmp += @unavailables.find_all{ |u| u[@col_ind.to_i] == s.strip.delete("'") }.compact
      end
      @unavailables = @tmp
      @un_count = @unavailables.size
    elsif @filter and @col_ind
      @ind = @col_ind.to_i
      if @filter == 'Не пусто'
        @unavailables = @unavailables.find_all{ |u| !( !u[@ind].blank? and u[@ind].index('#') ? u[@ind][0..u[@ind].index('#')-1].strip.delete("'") : u[@ind].strip.delete("'") ).blank? }.compact
      else
        @unavailables = @unavailables.find_all{ |u| ( !u[@ind].blank? and u[@ind].index('#') ? u[@ind][0..u[@ind].index('#')-1].strip.delete("'") : u[@ind].strip.delete("'") ) == params[:filter]}.compact
      end
      @un_count = @unavailables.size
    else
      @un_count = @unavailables.size
      @unavailables = @unavailables.paginate(page: params[:page].blank? ? 1 : params[:page], per_page: 15) 
    end

    respond_to do |format|
      format.html # index.html.erb
      format.js { }
    end
  end

  def agree?(unavailable)
    !Agree.agree(unavailable[0], unavailable[1]).nil?
  end

  def visible_connection?(str)
    #str[12].to_s == '1' or ( str[13].blank? or str[14].blank? ? false : UnvComment.find_comments(str[13],str[14]).any? ) or agree?(str)
    !agree?(str)
  end

  def comments
    respond_to do |format|
      format.js {}
    end
  end

  def new_comment
    @order_from_key = params[:order_from_key]
    @order_to_key = params[:order_to_key]
    @truck = params[:truck]
    @comments = UnvComment.find_comments(@order_from_key, @order_to_key)
    render :comments
    respond_to do |format|
      format.js {}
    end
  end

  def create_comment
    UnvComment.create({user_id: current_user.id, OrderFromKey: params[:OrderFromKey], OrderToKey: params[:OrderToKey], comment: params[:comment]})
    @order_from_key = params[:OrderFromKey]
    @order_to_key = params[:OrderToKey]
    @truck = params[:truck]
    @comments = UnvComment.find_comments(@order_from_key, @order_to_key)

    render :comments
  end

  def save_xls

      @invisible_cols = []
      @col_names =  JSON.parse(params[:col_names]).to_a
      @col_names.each_with_index { |cn,ind | @invisible_cols << ind if cn.index('#') }
      @col_names = @col_names.find_all { |u| !u.index('#') }

      @unavailables = JSON.parse(params[:unavailables]).to_a
      @unavailables.each { |unv| @invisible_cols.each { |inv| unv.delete_at(inv) } }
      @unavailables.each { |unv| unv.map! { |u| u && u.to_s.index('#') ? u[0..u.to_s.index('#')-1] : u  } }

      respond_to do |format|
      format.xls {
        unavailables = Spreadsheet::Workbook.new
        list = unavailables.create_worksheet name: 'unavailables'
        list.row(0).concat @col_names
        @unavailables.each_with_index { |unv, i| list.row(i+1).push *unv  }
        header_format = Spreadsheet::Format.new color: :green, weight: :bold
        list.row(0).default_format = header_format
        #output to blob object
        blob = StringIO.new("")
        unavailables.write blob
        #respond with blob object as a file
        send_data blob.string, type: :xls, filename: "Cоcтояние_ТС_#{Time.now.to_i.to_s}.xls"
      }
      end

  end

  def show_filter_and_search
   respond_to do |format|
      format.js   {}
   end
   @unavailables = dataset_query
   @col_names =  @unavailables[0].keys.find_all{|u| u unless u.index('#')}
   @unavailables.map!{|u| u.values.map!{|u2| u2.to_s}}
  end


  def select_filter
   respond_to do |format|
      format.js   {}
   end
   @unavailables = dataset_query
   
   @col_names =  @unavailables[0].keys
   @unavailables.map!{|u| u.values.map!{|u2| u2.to_s}}
   @ind = @col_names.index(params[:col_name])
   @unavailables = @unavailables.find_all { |u| visible_connection?(u) }.compact if request.referrer['connections']
   @for_filter = @unavailables.map { |u| ( u[@ind].index('#') ? u[@ind][0..u[@ind].index('#')-1].strip.delete("'") : u[@ind].strip.delete("'") )  if  u[@ind] and !u[@ind].strip.blank? }.compact.uniq.sort if @ind
   @for_filter.unshift('Не пусто')
   @for_search = @unavailables.map { |u| u[@ind].strip.delete("'") if  u[@ind] and !u[@ind].strip.blank? }.compact.uniq.sort if @ind
  end

  
  def repair_request
   respond_to do |format|
      format.js   {}
   end
   @repair_request = Unavailable.repair_request(params[:id], params[:chain].blank? ? 0 : 1)
   @col_names =  @repair_request[0].keys
   @repair_request.map!{|u| u.values}
   @popup_head = params[:ts]
  end


  def connections_unv
   respond_to do |format|
      format.js   {}
   end
   @conn_unv = Unavailable.connections_unv(params[:id])
   @col_names =  @conn_unv[0].keys
   @conn_unv.map!{|u| u.values}
   @popup_head = params[:ts]
  end

  def relocate_request
   respond_to do |format|
      format.js   {}
   end
   @relocate_request = Unavailable.relocate_request(params[:id])
   @col_names =  @relocate_request[0].keys
   @relocate_request.map!{|u| u.values}
   @popup_head = params[:ts]
  end

  def agree
    Agree.create({user_id: current_user.id, OrderFromKey: params[:OrderFromKey], OrderToKey: params[:OrderToKey], note: params[:note]})
    @order_from_key = params[:OrderFromKey]
    @order_to_key = params[:OrderToKey]
    @truck = params[:truck]
    @comments = UnvComment.find_comments(@order_from_key, @order_to_key)
    render :comments
  end

  def disagree
    @agree = Agree.agree(params[:OrderFromKey], params[:OrderToKey])
    @agree.destroy
    @order_from_key = params[:OrderFromKey]
    @order_to_key = params[:OrderToKey]
    @truck = params[:truck]
    @comments = UnvComment.find_comments(@order_from_key, @order_to_key)
    render :comments
  end

 private

  def unix_time(time)
    time['/'] || time['-'] ? time.to_time.to_i : time if time
  end

  def dataset_query
     if request.referrer['ol']
       Unavailable.getdata_ol
     elsif request.referrer['connections']
       @from ||= unix_time(params[:from])
       @to ||= unix_time(params[:to])
       Unavailable.connection_request(@from, @to)
     else Unavailable.getdata
     end
  end

  def silence_logs
    silence do
      yield
    end
  end




end