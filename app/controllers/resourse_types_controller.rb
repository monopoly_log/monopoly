# -*- encoding : utf-8 -*-


class ResourseTypesController < ApplicationController


  def index
 #   @resourse_types = ResourseType.all
    @resourse_types_count = ResourseType.count
    @resourse_types = ResourseType.paginate(page: params[:page], per_page: 20)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @resourse_types }
    end
  end


  def show
    @resourse_type = ResourseType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @resourse_type }
    end
  end


  def new
    @resourse_type = ResourseType.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @resourse_type }
    end
  end


  def edit
    @resourse_type = ResourseType.find(params[:id])
  end


  def create
    @resourse_type = ResourseType.new(params[:resourse_type])

    respond_to do |format|
      if @resourse_type.save
        format.html { redirect_to resourse_types_url, notice: 'Тип ресурса добавлен.' }
        format.json { render json: @resourse_type, status: :created, location: @resourse_type }
      else
        format.html { render action: "new" }
        format.json { render json: @resourse_type.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @resourse_type = ResourseType.find(params[:id])

    respond_to do |format|
      if @resourse_type.update_attributes(params[:resourse_type])
        format.html { redirect_to resourse_types_url, notice: 'Тип ресурса сохранен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @resourse_type.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @resourse_type = ResourseType.find(params[:id])
    @resourse_type.close
    #@resourse_type.destroy

    respond_to do |format|
      format.html { redirect_to resourse_types_url }
      format.json { head :no_content }
    end
  end
end

