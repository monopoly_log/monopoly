# -*- encoding : utf-8 -*-
class DocumentkindsController < ApplicationController


  def index
    @documentkinds = Documentkind.joins(:documenttype).order('documenttypes.name')
    @tmp1 , @tmp2 = @documentkinds.partition{|sep| sep.documenttype_id == params[:type] }
    @documentkinds = ((@tmp1 ? @tmp1 : []) + @tmp2).paginate(page: params[:page], per_page: 30)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @documentkinds }
    end
  end


  def show
    @documentkind = Documentkind.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @documentkind }
    end
  end


  def new
    @documentkind = Documentkind.new
    @documentkind.id = SecureRandom.uuid

    @documentattr = Documentattr.new({documentkind_id: @documentkind.id})

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @documentkind }
    end
  end


  def edit
    @documentkind = Documentkind.find(params[:id])
    @documentattr = @documentkind.documentattr ? @documentkind.documentattr : Documentattr.new({documentkind_id: @documentkind.id})
  end


  def create
    @documentkind = Documentkind.new(params[:documentkind])
    @documentattr = Documentattr.new(params[:documentattr])
    respond_to do |format|
      if @documentkind.save
        @documentkind = Documentkind.find(@documentkind.id)
        @documentkind.documentattr = @documentattr
        format.html { redirect_to edit_documentkind_url(@documentkind.id), notice: 'Вид документа добавлен.' }
        format.json { render json: @documentkind, status: :created, location: @documentkind }
      else
        format.html { render action: "new" }
        format.json { render json: @documentkind.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @documentkind = Documentkind.find(params[:id])
    @documentattr = Documentattr.new(params[:documentattr])
    respond_to do |format|
      if @documentkind.update_attributes(params[:documentkind])
        @documentkind.documentattr = @documentattr
        format.html { redirect_to edit_documentkind_url(@documentkind), notice: "Вид документа (#{@documentkind.name}) сохранен." }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @documentkind.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @documentkind = Documentkind.find(params[:id])
    @documentkind.close

    respond_to do |format|
      format.html { redirect_to documentkinds_url }
      format.json { head :no_content }
    end
  end


  def show_part
   respond_to do |format|
      format.js   {}
   end
   @part = params[:part]
   @documentkind = Documentkind.find(params[:id]) if params[:id]
  end


  def complete; end

end
