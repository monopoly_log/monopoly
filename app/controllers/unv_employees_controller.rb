# -*- encoding : utf-8 -*-


class UnvEmployeesController < ApplicationController


  def index
 #   @unv_employees = UnvEmployee.all
    @unv_employees_count = UnvEmployee.count
    @unv_employees = UnvEmployee.order(:planned_start).paginate(page: params[:page], per_page: 20)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @unv_employees }
    end
  end


  def show
    @unv_employee = UnvEmployee.find(params[:id])

    respond_to do |format|
      format.js { }
    end
  end


  def new
    @unv_employee = UnvEmployee.new
    
    @contractors = Contractor.find(Department.select('distinct contractor_id').map(&:contractor_id))
    @departments = Department.visible

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @unv_employee }
    end
  end


  def edit
    @unv_employee = UnvEmployee.find(params[:id])
    
    @contractors = Contractor.find(Department.select('distinct contractor_id').map(&:contractor_id))
    @departments = Department.visible
  end


  def create
    @unv_employee = UnvEmployee.new(params[:unv_employee])


    respond_to do |format|
      if @unv_employee.save
        format.html { redirect_to unv_employees_url, notice: 'Недоступность сотрудника добавлена' }
        format.json { render json: @unv_employee, status: :created, location: @unv_employee }
      else
        format.html { render action: "new" }
        format.json { render json: @unv_employee.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @unv_employee = UnvEmployee.find(params[:id])

    respond_to do |format|
      if @unv_employee.update_attributes(params[:unv_employee])
        format.html { redirect_to unv_employees_url, notice: 'Недоступность сотрудника сохранена' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @unv_employee.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @unv_employee = UnvEmployee.find(params[:id])
    @unv_employee.close
    #@unv_employee.destroy

    respond_to do |format|
      format.html { redirect_to unv_employees_url }
      format.json { head :no_content }
    end
  end
end

