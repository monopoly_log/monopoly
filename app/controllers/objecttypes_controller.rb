# -*- encoding : utf-8 -*-

class ObjecttypesController < ApplicationController


  def index
    @objecttypes = Objecttype.paginate(page: params[:page], per_page: 30)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @objecttypes }
    end
  end


  def show
    @objecttype = Objecttype.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @objecttype }
    end
  end


  def new
    @objecttype = Objecttype.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @objecttype }
    end
  end


  def edit
    @objecttype = Objecttype.find(params[:id])
  end


  def create
    @objecttype = Objecttype.new(params[:objecttype])

    respond_to do |format|
      if @objecttype.save
        format.html { redirect_to objecttypes_url, notice: 'Objecttype добавлен.' }
        format.json { render json: @objecttype, status: :created, location: @objecttype }
      else
        format.html { render action: "new" }
        format.json { render json: @objecttype.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @objecttype = Objecttype.find(params[:id])

    respond_to do |format|
      if @objecttype.update_attributes(params[:objecttype])
        format.html { redirect_to objecttypes_url, notice: 'Objecttype сохранен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @objecttype.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @objecttype = Objecttype.find(params[:id])
    @objecttype.destroy

    respond_to do |format|
      format.html { redirect_to objecttypes_url }
      format.json { head :no_content }
    end
  end
end

