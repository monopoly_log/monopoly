# -*- encoding : utf-8 -*-
class TransportcategoriesController < ApplicationController


  def index

    @transportcategories = Transportcategory.paginate(page: params[:page], per_page: 30).order('name')

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @transportcategories }
    end
  end



  def new
    @transportcategory = Transportcategory.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @transportcategory }
    end
  end


  def edit
    @transportcategory = Transportcategory.find(params[:id])
  end


  def create
    @transportcategory = Transportcategory.new(params[:transportcategory])

    respond_to do |format|
      if @transportcategory.save
        format.html { redirect_to transportcategories_path, notice: 'Категория ТС добавлена' }
        format.json { render json: @transportcategory, status: :created, location: @transportcategory }
      else
        format.html { render action: "new" }
        format.json { render json: @transportcategory.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /transportcategories/1
  # PUT /transportcategories/1.json
  def update
    @transportcategory = Transportcategory.find(params[:id])

    respond_to do |format|
      if @transportcategory.update_attributes(params[:transportcategory])
        format.html { redirect_to transportcategories_path, notice: 'Категория ТС сохранена' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @transportcategory.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transportcategories/1
  # DELETE /transportcategories/1.json
  def destroy
    @transportcategory = Transportcategory.find(params[:id])
    @transportcategory.close

    respond_to do |format|
      format.html { redirect_to transportcategories_url }
      format.json { head :no_content }
    end
  end
end
