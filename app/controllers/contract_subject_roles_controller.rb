# -*- encoding : utf-8 -*-


class ContractSubjectRolesController < ApplicationController


  def index
 #   @contract_subject_roles = ContractSubjectRole.all
    @contract_subject_roles_count = ContractSubjectRole.visible.count
    @contract_subject_roles = ContractSubjectRole.try(params[:show_del].blank? ? :visible : :invisible)
        .joins(:contract_subject)
        .order("contract_subjects.name")   
        .paginate(page: params[:page], per_page: 20)


    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @contract_subject_roles }
    end
  end


  def show
    @contract_subject_role = ContractSubjectRole.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @contract_subject_role }
    end
  end


  def new
    @contract_subject_role = ContractSubjectRole.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @contract_subject_role }
    end
  end


  def edit
    @contract_subject_role = ContractSubjectRole.find(params[:id])
  end


  def create
    @contract_subject_role = ContractSubjectRole.new(params[:contract_subject_role])

    respond_to do |format|
      if @contract_subject_role.save
        format.html { redirect_to contract_subject_roles_url, notice: 'Роль предмета договора добавлена' }
        format.json { render json: @contract_subject_role, status: :created, location: @contract_subject_role }
      else
        format.html { render action: "new" }
        format.json { render json: @contract_subject_role.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @contract_subject_role = ContractSubjectRole.find(params[:id])

    respond_to do |format|
      if @contract_subject_role.update_attributes(params[:contract_subject_role])
        format.html { redirect_to contract_subject_roles_url, notice: 'Роль предмета договора сохранена' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @contract_subject_role.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @contract_subject_role = ContractSubjectRole.find(params[:id])
    @contract_subject_role.close
    #@contract_subject_role.destroy

    respond_to do |format|
      format.html { redirect_to contract_subject_roles_url }
      format.json { head :no_content }
    end
  end
end

