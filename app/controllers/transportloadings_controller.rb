# -*- encoding : utf-8 -*-
class TransportloadingsController < ApplicationController
  # GET /transportloadings
  # GET /transportloadings.json
  def index
    @transportloadings = Transportloading.paginate(page: params[:page], per_page: 30)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @transportloadings }
    end
  end


  # GET /transportloadings/new
  # GET /transportloadings/new.json
  def new
    @transportloading = Transportloading.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @transportloading }
    end
  end

  # GET /transportloadings/1/edit
  def edit
    @transportloading = Transportloading.find(params[:id])
  end

  # POST /transportloadings
  # POST /transportloadings.json
  def create
    @transportloading = Transportloading.new(params[:transportloading])

    respond_to do |format|
      if @transportloading.save
        format.html { redirect_to transportloadings_url, notice: 'Способ загрузки добавлен' }
        format.json { render json: @transportloading, status: :created, location: @transportloading }
      else
        format.html { render action: "new" }
        format.json { render json: @transportloading.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /transportloadings/1
  # PUT /transportloadings/1.json
  def update
    @transportloading = Transportloading.find(params[:id])

    respond_to do |format|
      if @transportloading.update_attributes(params[:transportloading])
        format.html { redirect_to transportloadings_url, notice: 'Способ загрузки сохранен' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @transportloading.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transportloadings/1
  # DELETE /transportloadings/1.json
  def destroy
    @transportloading = Transportloading.find(params[:id])
    @transportloading.close

    respond_to do |format|
      format.html { redirect_to transportloadings_url }
      format.json { head :no_content }
    end
  end
end
