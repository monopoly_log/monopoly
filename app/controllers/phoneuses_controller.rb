# -*- encoding : utf-8 -*-
class PhoneusesController < ApplicationController


  def index
    @phoneuses = Phoneuse.paginate(page: params[:page], per_page: 20).joins(:person).order('people.last_name')

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @phoneuses }
    end
  end


  def new
    @phoneuse = Phoneuse.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @phoneuse }
    end
  end


  def edit
    @phoneuse = Phoneuse.find(params[:id])
  end


  def create
    @phoneuse = Phoneuse.new(params[:phoneuse])

    respond_to do |format|
      if @phoneuse.save
        format.html { redirect_to edit_phoneuse_url(@phoneuse), notice: 'Привязка тел. номера добавлена' }
        format.json { render json: @phoneuse, status: :created, location: @phoneuse }
      else
        format.html { render action: "new" }
        format.json { render json: @phoneuse.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @phoneuse = Phoneuse.find(params[:id])

    respond_to do |format|
      if @phoneuse.update_attributes(params[:phoneuse])
        format.html { redirect_to edit_phoneuse_url(@phoneuse), notice: 'Привязка тел. номера сохранена' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @phoneuse.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @phoneuse = Phoneuse.find(params[:id])
    @phoneuse.close

    respond_to do |format|
      format.html { redirect_to phoneuses_url }
      format.json { head :no_content }
    end
  end
end
