# -*- encoding : utf-8 -*-
class TestguidsController < ApplicationController
  # GET /testguids
  # GET /testguids.json
  def index
    @testguids = Testguid.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @testguids }
    end
  end

  # GET /testguids/1
  # GET /testguids/1.json
  def show
    @testguid = Testguid.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @testguid }
    end
  end

  # GET /testguids/new
  # GET /testguids/new.json
  def new
    @testguid = Testguid.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @testguid }
    end
  end

  # GET /testguids/1/edit
  def edit
    @testguid = Testguid.find(params[:id])
  end

  # POST /testguids
  # POST /testguids.json
  def create
    @testguid = Testguid.new(params[:testguid])

    respond_to do |format|
      if @testguid.save
        format.html { redirect_to @testguid, notice: 'Testguid was successfully created.' }
        format.json { render json: @testguid, status: :created, location: @testguid }
      else
        format.html { render action: "new" }
        format.json { render json: @testguid.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /testguids/1
  # PUT /testguids/1.json
  def update
    @testguid = Testguid.find(params[:id])

    respond_to do |format|
      if @testguid.update_attributes(params[:testguid])
        format.html { redirect_to @testguid, notice: 'Testguid was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @testguid.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /testguids/1
  # DELETE /testguids/1.json
  def destroy
    @testguid = Testguid.find(params[:id])
    @testguid.destroy

    respond_to do |format|
      format.html { redirect_to testguids_url }
      format.json { head :no_content }
    end
  end
end
