# -*- encoding : utf-8 -*-
class FunctionalsController < ApplicationController
  before_filter :authenticate_user!
  # GET /functionals
  # GET /functionals.json
  def index
   if params[:group]
      @group = Usergroup.find(params[:group])
      @functionals = @group.functionals
      @msg = "Группа #{@group.name}"
    elsif params[:role]
      @role = Role.find(params[:role])
      @functionals = @role.functionals
      @msg = "Роль #{@role.name}"
    elsif params[:user]
      @user = User.find(params[:user])
      @functionals = @user.functionals
      @msg = "Пользователь #{@user.email}"
    else
      @functionals = Functional.all
    end


    

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @functionals }
    end
  end

  # GET /functionals/1
  # GET /functionals/1.json
  def show
    @functional = Functional.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @functional }
    end
  end

  # GET /functionals/new
  # GET /functionals/new.json
  def new
    @functional = Functional.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @functional }
    end
  end

  # GET /functionals/1/edit
  def edit
    @functional = Functional.find(params[:id])
  end

  # POST /functionals
  # POST /functionals.json
  def create
    @functional = Functional.new(params[:functional])

    respond_to do |format|
      if @functional.save
        format.html { redirect_to @functional, notice: 'Functional was successfully created.' }
        format.json { render json: @functional, status: :created, location: @functional }
      else
        format.html { render action: "new" }
        format.json { render json: @functional.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /functionals/1
  # PUT /functionals/1.json
  def update
    @functional = Functional.find(params[:id])

    respond_to do |format|
      if @functional.update_attributes(params[:functional])
        format.html { redirect_to @functional, notice: 'Функционал сохранен' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @functional.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /functionals/1
  # DELETE /functionals/1.json
  def destroy
    @functional = Functional.find(params[:id])
    @functional.close
    respond_to do |format|
      format.html { redirect_to functionals_url }
      format.json { head :no_content }
    end
  end
end
