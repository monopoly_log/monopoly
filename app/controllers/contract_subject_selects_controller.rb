# -*- encoding : utf-8 -*-

class ContractSubjectSelectsController < ApplicationController


  def index
 #   @contract_subject_selects = ContractSubjectSelect.all
    @contract_subject_selects_count = ContractSubjectSelect.count
    @contract_subject_selects = ContractSubjectSelect.try(params[:show_del].blank? ? :visible : :invisible)
        .joins(:contract_subject)
        .order("contract_subjects.name")
        .paginate(page: params[:page], per_page: 20)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @contract_subject_selects }
    end
  end


  def show
    @contract_subject_select = ContractSubjectSelect.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @contract_subject_select }
    end
  end


  def new
    @contract_subject_select = ContractSubjectSelect.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @contract_subject_select }
    end
  end


  def edit
    @contract_subject_select = ContractSubjectSelect.find(params[:id])
  end


  def create
    @contract_subject_select = ContractSubjectSelect.new(params[:contract_subject_select])

    respond_to do |format|
      if @contract_subject_select.save
        format.html { redirect_to contract_subject_selects_url, notice: 'Вариант предмета договора добавлен.' }
        format.json { render json: @contract_subject_select, status: :created, location: @contract_subject_select }
      else
        format.html { render action: "new" }
        format.json { render json: @contract_subject_select.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @contract_subject_select = ContractSubjectSelect.find(params[:id])

    respond_to do |format|
      if @contract_subject_select.update_attributes(params[:contract_subject_select])
        format.html { redirect_to contract_subject_selects_url, notice: 'Вариант предмета договора сохранен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @contract_subject_select.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @contract_subject_select = ContractSubjectSelect.find(params[:id])
    @contract_subject_select.close
    #@contract_subject_select.destroy

    respond_to do |format|
      format.html { redirect_to contract_subject_selects_url }
      format.json { head :no_content }
    end
  end
end

