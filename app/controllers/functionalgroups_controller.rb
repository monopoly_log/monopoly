# -*- encoding : utf-8 -*-
class FunctionalgroupsController < ApplicationController

  #layout "functionalgroup"

  # GET /functionalgroups
  # GET /functionalgroups.json
  def index
    @functionalgroups = Functionalgroup.all.sort_by(&:lft)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @functionalgroups }
    end
  end

  def set_order

  end


  # GET /functionalgroups/new
  # GET /functionalgroups/new.json
  def new
    @functionalgroup = Functionalgroup.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @functionalgroup }
    end
  end

  # GET /functionalgroups/1/edit
  def edit
    @functionalgroup = Functionalgroup.find(params[:id])
  end

  # POST /functionalgroups
  # POST /functionalgroups.json
  def create
    @functionalgroup = Functionalgroup.new(params[:functionalgroup])

    respond_to do |format|
      if @functionalgroup.save
         @functionalgroup.reload
        format.html { redirect_to functionalgroups_url, notice: 'Функционал добавлен' }
        format.json { render json: @functionalgroup, status: :created, location: @functionalgroup }
      else
        format.html { render action: "new" }
        format.json { render json: @functionalgroup.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /functionalgroups/1
  # PUT /functionalgroups/1.json
  def update
    @functionalgroup = Functionalgroup.find(params[:id])

    respond_to do |format|
      if @functionalgroup.update_attributes(params[:functionalgroup])
        format.html { redirect_to functionalgroups_url, notice: 'Функционал сохранен' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @functionalgroup.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /functionalgroups/1
  # DELETE /functionalgroups/1.json
  def destroy
    @functionalgroup = Functionalgroup.find(params[:id])
    @functionalgroup.close

    respond_to do |format|
      format.html { redirect_to functionalgroups_url }
      format.json { head :no_content }
    end
  end


  def savesort

    neworder = JSON.parse(params[:set])
    prev_item = nil
    neworder.each do |item|
      dbitem = Functionalgroup.find(item['id'])
      prev_item.nil? ? dbitem.move_to_root : dbitem.move_to_right_of(prev_item)
      sort_children(item, dbitem) unless item['children'].nil?
      prev_item = dbitem
    end
    Functionalgroup.rebuild!
    @functionalgroups = Functionalgroup.all.sort_by(&:lft)
    #redirect_to functionalgroups_url
    #render action: "index"
    flash[:notice] = "ОК"
    respond_to do |format|
      format.js {redirect_to functionalgroups_url }
    end

   # render :nothing => true
  end

  def sort_children(element,dbitem)
    prevchild = nil
    element['children'].each do |child|
      childitem = Functionalgroup.find(child['id'])
      prevchild.nil? ? childitem.move_to_child_of(dbitem) : childitem.move_to_right_of(prevchild)
      sort_children(child, childitem) unless child['children'].nil?
      prevchild = childitem
    end
  end

  def show_part
   respond_to do |format|
      format.js   {}
   end
   @part = params[:part]
   @div = "popup_content"
  end


  def update_menu
   respond_to do |format|
      format.js   {}
   end
  end

  def complete;  end


end
