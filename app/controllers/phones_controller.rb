# -*- encoding : utf-8 -*-
class PhonesController < ApplicationController


  def index
    @phones = Phone.order(:phone).paginate(page: params[:page], per_page: 30)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @phones }
    end
  end


  def new
    @phone = Phone.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @phone }
    end
  end


  def edit
    @phone = Phone.find(params[:id])
  end


  def create
    @phone = Phone.new(params[:phone])

    respond_to do |format|
      if @phone.save
        format.html { redirect_to phones_url, notice: 'Телефон добавлен.' }
        format.json { render json: @phone, status: :created, location: @phone }
      else
        format.html { render action: "new" }
        format.json { render json: @phone.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @phone = Phone.find(params[:id])

    respond_to do |format|
      if @phone.update_attributes(params[:phone])
        format.html { redirect_to phones_url, notice: 'Телефон сохранен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @phone.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @phone = Phone.find(params[:id])
    @phone.close

    respond_to do |format|
      format.html { redirect_to phones_url }
      format.json { head :no_content }
    end
  end
end
