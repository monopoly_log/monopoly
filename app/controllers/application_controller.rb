# -*- encoding : utf-8 -*-
class ApplicationController < ActionController::Base
  protect_from_forgery
  require 'will_paginate/array'
  #skip_before_action :verify_authenticity_token #, if: xhr_request?
  
  before_filter :authenticate_user!
  after_filter :user_activity

 # rescue_from ActiveRecord::RecordNotFound, :with => :not_found

  def referrer_controller
    ref = request.referrer
    rec = ref.split('/')[3]
    rec
  end



    private

# def not_found
#     respond_with(nil, :status => 404) do |format|
#      format.html {
#         flash[:error] = I18n.t(:error404, :scope => "system")
#        redirect_to(root_path)
#       }
#      format.xml { render :text => {'error' => 'Resource not found'}.to_xml(:root => 'errors'), :status => 404 }
#      format.js { render :text => {'error' => 'Resource not found'}, :status => 404 }
#      format.json { render :text => {'errors' => ['Resource not found']}.to_json, :status => 404 }
#     end
# end
 
  # Overwriting the sign_out redirect path method
  def after_sign_out_path_for(users)
    root_path
  end
  def after_sign_in_path_for(users)
    root_path
  end

#def after_sign_in_path_for(users)
#  request.referrer
#end



def user_activity
  current_user.try :touch
end

end
