# -*- encoding : utf-8 -*-
class PeopleController < ApplicationController

  def index

    #@people = Person.order('last_name').paginate(page: params[:page], per_page: 30)
    @people_all = Person.all.sort_by(&:fullname)

    @letters = @people_all.map{|t| t.last_name[0] if t.last_name}.compact.uniq.sort
    unless params[:letter]
      @people = @people_all.sort_by(&:fullname)
    else
      @people = @people_all.find_all{|t| t.fullname[0] == params[:letter]}.compact
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @people }
    end
  end


  def show
    redirect_to people_url(letter: params[:letter])
  end

  def new_person
    @person = Person.new
    respond_to do |format|
      format.js { }
    end
  end


  def new
    @person = Person.new
    @phone = Phone.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @person }
    end
  end


  def edit
    @person = Person.find(params[:id])
    @phone = Phone.new
  end


  def create
    @person = Person.new(params[:person])

    respond_to do |format|
      if @person.save
        format.html { redirect_to edit_person_path(@person), notice: 'Физ. лицо добавлено' }
        format.js do
          flash[:notice] = "Физ. лицо добавлено"
          render action: "#{referrer_controller}"
        end
      else
        format.html { render action: "new" }
        format.js { render action: "new_person" }
      end
    end
  end

 
  def update
    @person = Person.find(params[:id])

    respond_to do |format|
      if @person.update_attributes(params[:person])
        format.html { redirect_to edit_person_path(@person), notice: 'Физ. лицо сохранено.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @person = Person.find(params[:id])
    current_user.admin ? @person.destroy : @person.close

    respond_to do |format|
      format.html { redirect_to people_url }
      format.json { head :no_content }
    end
  end

  def remove_doc
    @document = Document.find(params[:id])
    tmp = @document
    tmp_p = @document.person
    current_user.admin ? @document.destroy : @document.close

    respond_to do |format|
      format.html { redirect_to people_url, notice: "Документ #{tmp.documentkind.name} для #{tmp_p.fullname} удален" }
      format.json { head :no_content }
    end
  end

  def show_part
   respond_to do |format|
      format.js   {}
   end
   @part = params[:part]
   @person = Person.find(params[:id]) if params[:id]
  end

  def phone_person
   respond_to do |format|
      format.js   {}
   end
   if params[:id]
     @person = Person.find(params[:id])
     par = params[:phone]
     id = SecureRandom.uuid
     @phone = Phone.create(id: id, phone: par[:phone], start_date: par[:start_date], end_date: par[:end_date])
     @person.phoneuses.create(phone_id: id, id: SecureRandom.uuid, issue_date: Time.now )
     #@person.reload
     flash[:notice] = "Номер #{par[:phone]} добавлен"
   elsif params[:remove]
     @phone = Phone.find(params[:remove])
     @person = Person.find(params[:person])
     Phoneuse.where({phone_id: @phone.id, person_id: @person.id}).delete_all
     flash[:notice] = "Номер удален"
   end

   render action: "complete"
#render text: params[:phone]
  end

  private


  def complete;
  end

  def hide; end

  def contractor_representatives
    @contractor_representative = ContractorRepresentative.new
  end



end
