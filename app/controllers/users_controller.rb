# -*- encoding : utf-8 -*-
class UsersController < ApplicationController
  



  def index
#    if params[:role]
#      @role = Role.find(params[:role])
#      @users = @role.users.paginate(page: params[:page], per_page: 30).order("last_name, name, surname")
#      @msg = "Роль #{@role.name}"
#    end
    

    @users_all = User.all

    @letters = @users_all.map{|t| t.last_name[0] if t.last_name[0]}.compact.uniq.sort
    unless params[:letter]
      @users = @users_all.sort_by(&:fullname)
    else
      @users = @users_all.find_all{|t| t.fullname[0] == params[:letter]}.compact
    end

    if params[:role]
      @role = Role.find(params[:role])
      @users = @role.users.order("last_name, name, surname")
      #@users = @role.users.paginate(page: params[:page], per_page: 30).order("last_name, name, surname")
      @msg = "Роль #{@role.name}"
    end


    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
  end


  def show
    @user = User.find(params[:id])
   
    respond_to do |format|
      format.js { }
    end
  end

  def new
    @user = User.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end


 def edit
    @user = User.find(params[:id])
#      class << @user
#        def one
#          111
#        end
#      end
  end



  def create
    params[:user].delete(:person_id) if params[:user][:person_id].blank? 
    @user = User.new(params[:user]) 

    respond_to do |format|
      if @user.save
        format.html { redirect_to edit_user_path(@user), notice: 'Пользователь добавлен.' }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @user = User.find(params[:id])
    if params[:user][:password].blank?
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
    end
    params[:user][:person_id] = nil if params[:user][:person_id].blank?
    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to edit_user_path(@user), notice: 'Пользователь сохранен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @user = User.find(params[:id])
    @user.close

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end

def select_role
   respond_to do |format|
      format.js   {}
   end
  @user = User.find(params[:id])
  @role = Role.find(params[:role][:id])
  @user.roles << @role
  flash[:notice] = 'Роль добавлена'
  #render action: "complete"

end

def del_role_from_user
   respond_to do |format|
      format.js   {}
   end
  @user = User.find(params[:id])
  @role = Role.find(params[:role])
  @user.roles.delete(@role)
  @div = "popup_content"
  flash[:notice] = 'Удалено'
end

def select_group
   respond_to do |format|
      format.js   {}
   end
  @user = User.find(params[:id])
  @group = Usergroup.find(params[:usergroup][:id])
  @user.usergroups << @group
  flash[:notice] = 'Пользователь в группу добавлен'
  render action: "complete"

end

def del_group_from_user
   respond_to do |format|
      format.js   {}
   end
  @user = User.find(params[:id])
  @group = Usergroup.find(params[:group])
  @user.usergroups.delete(@group)
  @div = "popup_content"
  flash[:notice] = 'Удалено'
end

#------------------------------------------------

def f_group; end


def select_f_group
   respond_to do |format|
      format.js   {}
   end
  @user = User.find(params[:id])
  @functional = Functionalgroup.find(params[:functional][:id])
  @user.functionalgroups << @functional
  flash[:notice] = 'Функцинал добавлен'
  render action: "f_group"
end

def del_f_group_from_user
   respond_to do |format|
      format.js   {}
   end
  @user = User.find(params[:id])
  @functional = Functionalgroup.find(params[:functional])
  @user.functionalgroups.delete(@functional)
  flash[:notice] = 'Удалено'
  render action: "f_group"
  
end


#------------------------------------------------

def select_functional
   respond_to do |format|
      format.js   {}
   end
  @user = User.find(params[:id])
  @functional = Functional.find(params[:functional][:id])
  @user.functionals << @functional
  flash[:notice] = 'Функцинал добавлен'
  render action: "complete"
end


def del_functional_from_user
   respond_to do |format|
      format.js   {}
   end
  @user = User.find(params[:id])
  @functional = Functional.find(params[:functional])
  @user.functionals.delete(@functional)
  @div = "popup_content"
  flash[:notice] = 'Удалено'
end

def select_person
   respond_to do |format|
      format.js   {}
   end
   @person = Person.find(params[:person]) unless params[:person].blank?
end

  def show_part
   respond_to do |format|
      format.js   {}
   end
   @part = params[:part]
   @user = User.find(params[:id]) if params[:id]
   @title = params[:title]
  end

  def complete
    
  end

end
