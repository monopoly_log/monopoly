# -*- encoding : utf-8 -*-
class TransportkindsController < ApplicationController

  def index
    if params[:type]
      @transporttype = Transporttype.find(params[:type])
      @transportkinds = @transporttype.transportkinds.paginate(page: params[:page], per_page: 20)
    else
      @transportkinds = Transportkind.order(:transporttype_id).paginate(page: params[:page], per_page: 20)
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @transportkinds }
    end
  end


  def new

    @transportkind = Transportkind.new
    @transporttype = Transporttype.find(params[:type]) if params[:type]
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @transportkind }
    end
  end


  def edit
    @transportkind = Transportkind.find(params[:id])
    @transporttype = @transportkind.transporttype
  end


  def create
    @transportkind = Transportkind.new(params[:transportkind])
    @transporttype = @transportkind.transporttype
    respond_to do |format|
      if @transportkind.save
        format.html { redirect_to transportkinds_path(type: @transportkind.transporttype_id), notice: 'Transportkind was successfully created.' }
        format.json { render json: @transportkind, status: :created, location: @transportkind }
      else
        format.html { render action: "new" }
        format.json { render json: @transportkind.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @transportkind = Transportkind.find(params[:id])
    @transporttype = @transportkind.transporttype
    respond_to do |format|
      if @transportkind.update_attributes(params[:transportkind])
        format.html { redirect_to transportkinds_path(type: @transportkind.transporttype_id), notice: 'Transportkind was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @transportkind.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @transportkind = Transportkind.find(params[:id])
    transporttype = @transportkind.transporttype_id
    @transportkind.close

    respond_to do |format|
      format.html { redirect_to transportkinds_url( type: transporttype ) }
      format.json { head :no_content }
    end
  end
end
