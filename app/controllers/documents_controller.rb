# -*- encoding : utf-8 -*-
class DocumentsController < ApplicationController


  def index
    #Document.destroy_invalid
    @documents = Document.order("created_at desc").roots.paginate(page: params[:page], per_page: 20)
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @documents }
    end
  end


  def show
    @document = Document.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      #format.json { render json: @document }
    end
  end


  def new
    @document = Document.new
    @documentfile = Documentfile.new
    
    @transport = Transport.find(params[:transport]) if params[:transport]
    @person = Person.find(params[:person]) if params[:person]
    @document.documentkind_id = params[:documentkind][:id] if params[:documentkind][:id]
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @document }
    end
  end


  def edit
    @document = Document.find(params[:id]) if params[:id]
    @documentfiles = @document.documentfiles if @document.documentfiles.any?
    @documentfile = Documentfile.new
  end


  def create
    
    @transport = Transport.find(params[:document][:transport]) if params[:document][:transport]  
    @person = Person.find(params[:document][:person]) if params[:document][:person]
    
    
    @documentfile = Documentfile.new


    params[:document].delete(:transport)
    params[:document].delete(:person)
    params[:document].delete(:parent_id) if params[:document][:parent_id] == ''
    
    @document = Document.new(params[:document])
    @document.id ||= SecureRandom.uuid
    respond_to do |format|
      if @document.save
        @document.transports << @transport if @transport
        @document.people << @person if @person
        upload_file if params[:documentfile] and params[:documentfile][:fname]
        format.html { redirect_to edit_document_path(@document), notice: 'Документ добавлен.' }
       # format.json { render json: @document, status: :created, location: @document }
      else
        format.html { render action: "new" , transport: @transport, person: @person }
       # format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @document = Document.find(params[:id])
    @documentfiles = @document.documentfiles if @document.documentfiles.any?
    @documentfile = Documentfile.new
    params[:document].delete(:parent_id) if params[:document][:parent_id] 
    params[:document].delete(:person) if params[:document][:person]
    params[:document].delete(:transport) if params[:document][:transport]

    respond_to do |format|
      if @document.update_attributes(params[:document])
        upload_file if params[:documentfile] and  params[:documentfile][:fname]

        format.html { redirect_to edit_document_url(@document), notice: 'Документ сохранен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

 def upload_file
        uploaded_io = params[:documentfile][:fname]
        new_name = Time.now.to_time.to_i.to_s + uploaded_io.original_filename[uploaded_io.original_filename.index('.')..-1]
        File.open(Rails.root.join('public', 'documents', new_name), 'wb') do |file|
        file.write(uploaded_io.read)
        end
        @document.documentfiles << Documentfile.new({fname: new_name, descr: uploaded_io.original_filename })
 end


  def destroy
    @document = Document.find(params[:id])
    current_user.admin ? @document.delete : @document.close

    respond_to do |format|
      format.html { redirect_to documents_url }
      format.json { head :no_content }
    end
  end


  def del_file
    @document = Document.find(params[:document])
    @document_file = Documentfile.find(params[:id])
    @document_file.delete

    respond_to do |format|
      format.html { redirect_to edit_document_url(@document) }
      format.json { head :no_content }
    end
  end

  def select_kind
   respond_to do |format|
      format.js   {}
   end
   @document = Document.find(params[:id])
   @document.update_attribute(:documentkind_id, params[:kind])
   @document.documentkind_id = params[:kind]
   flash[:notice] = "Тип и вид документа сохранен"
   render action: "complete"
  end

  def select_transport
   respond_to do |format|
      format.js   {}
   end
   @document = Document.find(params[:id])
   @transport = Transport.find(params[:transport][:id])
   @document.transports.delete( @document.transports )
   @document.transports << @transport
   flash[:notice] = "ТС сохранен"
   render action: "complete"
  end



  def show_part
   respond_to do |format|
      format.js   {}
   end
   @part = params[:part]
   @div = "popup_content"
   @document = Document.find(params[:id]) if params[:id]
  end


  def complete; end



end
