# -*- encoding : utf-8 -*-


class ContractorRepresentativesController < ApplicationController


  def index
 #   @contractor_representatives = ContractorRepresentative.all
    @contractor_representatives_count = ContractorRepresentative.count
    @contractor_representatives = ContractorRepresentative.paginate(page: params[:page], per_page: 20)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @contractor_representatives }
    end
  end


  def show
    @contractor_representative = ContractorRepresentative.find(params[:id])

    respond_to do |format|
      format.js { }
    end
  end


  def new
    @contractor_representative = ContractorRepresentative.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @contractor_representative }
    end
  end


  def edit
    @contractor_representative = ContractorRepresentative.find(params[:id])
  end


  def create
    @contractor_representative = ContractorRepresentative.new(params[:contractor_representative])

    respond_to do |format|
      if @contractor_representative.save
        format.html { redirect_to contractor_representatives_url, notice: 'Contractor representative добавлен.' }
        format.json { render json: @contractor_representative, status: :created, location: @contractor_representative }
      else
        format.html { render action: "new" }
        format.json { render json: @contractor_representative.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @contractor_representative = ContractorRepresentative.find(params[:id])

    respond_to do |format|
      if @contractor_representative.update_attributes(params[:contractor_representative])
        format.html { redirect_to contractor_representatives_url, notice: 'Contractor representative сохранен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @contractor_representative.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @contractor_representative = ContractorRepresentative.find(params[:id])
    @contractor_representative.close
    #@contractor_representative.destroy

    respond_to do |format|
      format.html { redirect_to contractor_representatives_url }
      format.json { head :no_content }
    end
  end
end

