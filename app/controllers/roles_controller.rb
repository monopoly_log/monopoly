# -*- encoding : utf-8 -*-
class RolesController < ApplicationController
  before_filter :authenticate_user!
  # GET /roles
  # GET /roles.json
  def index
    @roles = Role.paginate(page: params[:page], per_page: 30)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @roles }
    end
  end


  # GET /roles/new
  # GET /roles/new.json
  def new
    @role = Role.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @role }
    end
  end

  # GET /roles/1/edit
  def edit
    @role = Role.find(params[:id])
  end

  # POST /roles
  # POST /roles.json
  def create
    @role = Role.new(params[:role])

    respond_to do |format|
      if @role.save
        format.html { redirect_to roles_path, notice: 'Роль создана.' }
        format.json { render json: @role, status: :created, location: @role }
      else
        format.html { render action: "new" }
        format.json { render json: @role.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /roles/1
  # PUT /roles/1.json
  def update
    @role = Role.find(params[:id])

    respond_to do |format|
      if @role.update_attributes(params[:role])
        format.html { redirect_to roles_path, notice: 'Роль сохранена.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /roles/1
  # DELETE /roles/1.json
  def destroy
    @role = Role.find(params[:id])
    @role.close

    respond_to do |format|
      format.html { redirect_to roles_url }
      format.json { head :no_content }
    end
  end

  def select_functional
   respond_to do |format|
      format.js   {}
   end
  @role = Role.find(params[:id])
  @functional = Functionalgroup.find(params[:functional][:id])
  @role.functionalgroups << @functional
  flash.now[:notice] = 'Функционал добавлен'

end

def del_functional_from_role
   respond_to do |format|
      format.js   {}
   end
  @role= Role.find(params[:id])
  @functional = Functionalgroup.find(params[:functional])
  @role.functionalgroups.delete(@functional)
  flash.now[:notice] = 'Удалено'
end


end
