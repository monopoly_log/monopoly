# -*- encoding : utf-8 -*-
class TransporttypesController < ApplicationController
  # GET /transporttypes
  # GET /transporttypes.json
  def index
    if params[:category]
      @transportcategory = Transportcategory.find(params[:category])
      @transporttypes = @transportcategory.transporttypes.paginate(page: params[:page], per_page: 20)
    else
      @transporttypes = Transporttype.order(:transportcategory_id).paginate(page: params[:page], per_page: 20)
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @transporttypes }
    end
  end

  # GET /transporttypes/new
  # GET /transporttypes/new.json
  def new
    @transportcategory = Transportcategory.find(params[:category]) if params[:category]
    @transporttype = Transporttype.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @transporttype }
    end
  end

  # GET /transporttypes/1/edit
  def edit
    @transporttype = Transporttype.find(params[:id])
    @transportcategory = @transporttype.transportcategory
  end

  # POST /transporttypes
  # POST /transporttypes.json
  def create
    @transporttype = Transporttype.new(params[:transporttype])
    @transportcategory = @transporttype.transportcategory
    respond_to do |format|
      if @transporttype.save
        format.html { redirect_to transporttypes_path( category: @transporttype.transportcategory_id ), notice: 'Тип ТС создан' }
        format.json { render json: @transporttype, status: :created, location: @transporttype }
      else
        format.html { render action: "new" }
        format.json { render json: @transporttype.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /transporttypes/1
  # PUT /transporttypes/1.json
  def update
    @transporttype = Transporttype.find(params[:id])
    @transportcategory = @transporttype.transportcategory
    respond_to do |format|
      if @transporttype.update_attributes(params[:transporttype])
        format.html { redirect_to transporttypes_path( category: @transporttype.transportcategory_id ), notice: 'Тип ТС сохранен' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @transporttype.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transporttypes/1
  # DELETE /transporttypes/1.json
  def destroy
    @transporttype = Transporttype.find(params[:id])
    transportcategory = @transporttype.transportcategory_id
    @transporttype.close

    respond_to do |format|
      format.html { redirect_to transporttypes_url( category: transportcategory ) }
      format.json { head :no_content }
    end
  end
end
