require 'test_helper'

class ResourseTypesControllerTest < ActionController::TestCase
  setup do
    @resourse_type = resourse_types(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:resourse_types)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create resourse_type" do
    assert_difference('ResourseType.count') do
      post :create, resourse_type: { descr: @resourse_type.descr, id: @resourse_type.id, name: @resourse_type.name }
    end

    assert_redirected_to resourse_type_path(assigns(:resourse_type))
  end

  test "should show resourse_type" do
    get :show, id: @resourse_type
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @resourse_type
    assert_response :success
  end

  test "should update resourse_type" do
    put :update, id: @resourse_type, resourse_type: { descr: @resourse_type.descr, id: @resourse_type.id, name: @resourse_type.name }
    assert_redirected_to resourse_type_path(assigns(:resourse_type))
  end

  test "should destroy resourse_type" do
    assert_difference('ResourseType.count', -1) do
      delete :destroy, id: @resourse_type
    end

    assert_redirected_to resourse_types_path
  end
end
