require 'test_helper'

class TruckDriversControllerTest < ActionController::TestCase
  setup do
    @truck_driver = truck_drivers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:truck_drivers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create truck_driver" do
    assert_difference('TruckDriver.count') do
      post :create, truck_driver: { employee_id: @truck_driver.employee_id, end_date: @truck_driver.end_date, id: @truck_driver.id, is_close: @truck_driver.is_close, start_date: @truck_driver.start_date, track_id: @truck_driver.track_id }
    end

    assert_redirected_to truck_driver_path(assigns(:truck_driver))
  end

  test "should show truck_driver" do
    get :show, id: @truck_driver
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @truck_driver
    assert_response :success
  end

  test "should update truck_driver" do
    put :update, id: @truck_driver, truck_driver: { employee_id: @truck_driver.employee_id, end_date: @truck_driver.end_date, id: @truck_driver.id, is_close: @truck_driver.is_close, start_date: @truck_driver.start_date, track_id: @truck_driver.track_id }
    assert_redirected_to truck_driver_path(assigns(:truck_driver))
  end

  test "should destroy truck_driver" do
    assert_difference('TruckDriver.count', -1) do
      delete :destroy, id: @truck_driver
    end

    assert_redirected_to truck_drivers_path
  end
end
