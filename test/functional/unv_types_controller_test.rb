require 'test_helper'

class UnvTypesControllerTest < ActionController::TestCase
  setup do
    @unv_type = unv_types(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:unv_types)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create unv_type" do
    assert_difference('UnvType.count') do
      post :create, unv_type: { descr: @unv_type.descr, id: @unv_type.id, name: @unv_type.name, resourse_type_id: @unv_type.resourse_type_id }
    end

    assert_redirected_to unv_type_path(assigns(:unv_type))
  end

  test "should show unv_type" do
    get :show, id: @unv_type
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @unv_type
    assert_response :success
  end

  test "should update unv_type" do
    put :update, id: @unv_type, unv_type: { descr: @unv_type.descr, id: @unv_type.id, name: @unv_type.name, resourse_type_id: @unv_type.resourse_type_id }
    assert_redirected_to unv_type_path(assigns(:unv_type))
  end

  test "should destroy unv_type" do
    assert_difference('UnvType.count', -1) do
      delete :destroy, id: @unv_type
    end

    assert_redirected_to unv_types_path
  end
end
