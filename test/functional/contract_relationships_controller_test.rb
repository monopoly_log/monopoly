require 'test_helper'

class ContractRelationshipsControllerTest < ActionController::TestCase
  setup do
    @contract_relationship = contract_relationships(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:contract_relationships)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create contract_relationship" do
    assert_difference('ContractRelationship.count') do
      post :create, contract_relationship: { contract_id: @contract_relationship.contract_id, contract_subject_role_id: @contract_relationship.contract_subject_role_id, contractor_id: @contract_relationship.contractor_id, id: @contract_relationship.id, is_close: @contract_relationship.is_close }
    end

    assert_redirected_to contract_relationship_path(assigns(:contract_relationship))
  end

  test "should show contract_relationship" do
    get :show, id: @contract_relationship
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @contract_relationship
    assert_response :success
  end

  test "should update contract_relationship" do
    put :update, id: @contract_relationship, contract_relationship: { contract_id: @contract_relationship.contract_id, contract_subject_role_id: @contract_relationship.contract_subject_role_id, contractor_id: @contract_relationship.contractor_id, id: @contract_relationship.id, is_close: @contract_relationship.is_close }
    assert_redirected_to contract_relationship_path(assigns(:contract_relationship))
  end

  test "should destroy contract_relationship" do
    assert_difference('ContractRelationship.count', -1) do
      delete :destroy, id: @contract_relationship
    end

    assert_redirected_to contract_relationships_path
  end
end
