# -*- encoding : utf-8 -*-
require 'test_helper'

class TransportmodelsControllerTest < ActionController::TestCase
  setup do
    @transportmodel = transportmodels(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:transportmodels)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create transportmodel" do
    assert_difference('Transportmodel.count') do
      post :create, transportmodel: { id: @transportmodel.id, name: @transportmodel.name, transportbrand_id: @transportmodel.transportbrand_id }
    end

    assert_redirected_to transportmodel_path(assigns(:transportmodel))
  end

  test "should show transportmodel" do
    get :show, id: @transportmodel
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @transportmodel
    assert_response :success
  end

  test "should update transportmodel" do
    put :update, id: @transportmodel, transportmodel: { id: @transportmodel.id, name: @transportmodel.name, transportbrand_id: @transportmodel.transportbrand_id }
    assert_redirected_to transportmodel_path(assigns(:transportmodel))
  end

  test "should destroy transportmodel" do
    assert_difference('Transportmodel.count', -1) do
      delete :destroy, id: @transportmodel
    end

    assert_redirected_to transportmodels_path
  end
end
