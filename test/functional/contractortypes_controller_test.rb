# -*- encoding : utf-8 -*-
require 'test_helper'

class ContractortypesControllerTest < ActionController::TestCase
  setup do
    @contractortype = contractortypes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:contractortypes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create contractortype" do
    assert_difference('Contractortype.count') do
      post :create, contractortype: { descr: @contractortype.descr, id: @contractortype.id, name: @contractortype.name }
    end

    assert_redirected_to contractortype_path(assigns(:contractortype))
  end

  test "should show contractortype" do
    get :show, id: @contractortype
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @contractortype
    assert_response :success
  end

  test "should update contractortype" do
    put :update, id: @contractortype, contractortype: { descr: @contractortype.descr, id: @contractortype.id, name: @contractortype.name }
    assert_redirected_to contractortype_path(assigns(:contractortype))
  end

  test "should destroy contractortype" do
    assert_difference('Contractortype.count', -1) do
      delete :destroy, id: @contractortype
    end

    assert_redirected_to contractortypes_path
  end
end
