# -*- encoding : utf-8 -*-
require 'test_helper'

class ObjecttypepropertiesControllerTest < ActionController::TestCase
  setup do
    @objecttypeproperty = objecttypeproperties(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:objecttypeproperties)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create objecttypeproperty" do
    assert_difference('Objecttypeproperty.count') do
      post :create, objecttypeproperty: { descr: @objecttypeproperty.descr, end_date: @objecttypeproperty.end_date, id: @objecttypeproperty.id, is_close: @objecttypeproperty.is_close, name: @objecttypeproperty.name, objecttype_id: @objecttypeproperty.objecttype_id, start_date: @objecttypeproperty.start_date, system_id: @objecttypeproperty.system_id }
    end

    assert_redirected_to objecttypeproperty_path(assigns(:objecttypeproperty))
  end

  test "should show objecttypeproperty" do
    get :show, id: @objecttypeproperty
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @objecttypeproperty
    assert_response :success
  end

  test "should update objecttypeproperty" do
    put :update, id: @objecttypeproperty, objecttypeproperty: { descr: @objecttypeproperty.descr, end_date: @objecttypeproperty.end_date, id: @objecttypeproperty.id, is_close: @objecttypeproperty.is_close, name: @objecttypeproperty.name, objecttype_id: @objecttypeproperty.objecttype_id, start_date: @objecttypeproperty.start_date, system_id: @objecttypeproperty.system_id }
    assert_redirected_to objecttypeproperty_path(assigns(:objecttypeproperty))
  end

  test "should destroy objecttypeproperty" do
    assert_difference('Objecttypeproperty.count', -1) do
      delete :destroy, id: @objecttypeproperty
    end

    assert_redirected_to objecttypeproperties_path
  end
end
