# -*- encoding : utf-8 -*-
require 'test_helper'

class FunctionalsControllerTest < ActionController::TestCase
  setup do
    @functional = functionals(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:functionals)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create functional" do
    assert_difference('Functional.count') do
      post :create, functional: { description: @functional.description, end: @functional.end, guid: @functional.guid, name: @functional.name, part: @functional.part, start: @functional.start }
    end

    assert_redirected_to functional_path(assigns(:functional))
  end

  test "should show functional" do
    get :show, id: @functional
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @functional
    assert_response :success
  end

  test "should update functional" do
    put :update, id: @functional, functional: { description: @functional.description, end: @functional.end, guid: @functional.guid, name: @functional.name, part: @functional.part, start: @functional.start }
    assert_redirected_to functional_path(assigns(:functional))
  end

  test "should destroy functional" do
    assert_difference('Functional.count', -1) do
      delete :destroy, id: @functional
    end

    assert_redirected_to functionals_path
  end
end
