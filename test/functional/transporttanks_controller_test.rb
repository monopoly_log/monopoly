# -*- encoding : utf-8 -*-
require 'test_helper'

class TransporttanksControllerTest < ActionController::TestCase
  setup do
    @transporttank = transporttanks(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:transporttanks)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create transporttank" do
    assert_difference('Transporttank.count') do
      post :create, transporttank: { id: @transporttank.id, v_tank: @transporttank.v_tank, v_tank_min: @transporttank.v_tank_min }
    end

    assert_redirected_to transporttank_path(assigns(:transporttank))
  end

  test "should show transporttank" do
    get :show, id: @transporttank
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @transporttank
    assert_response :success
  end

  test "should update transporttank" do
    put :update, id: @transporttank, transporttank: { id: @transporttank.id, v_tank: @transporttank.v_tank, v_tank_min: @transporttank.v_tank_min }
    assert_redirected_to transporttank_path(assigns(:transporttank))
  end

  test "should destroy transporttank" do
    assert_difference('Transporttank.count', -1) do
      delete :destroy, id: @transporttank
    end

    assert_redirected_to transporttanks_path
  end
end
