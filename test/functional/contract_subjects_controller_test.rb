require 'test_helper'

class ContractSubjectsControllerTest < ActionController::TestCase
  setup do
    @contract_subject = contract_subjects(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:contract_subjects)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create contract_subject" do
    assert_difference('ContractSubject.count') do
      post :create, contract_subject: { contract_type_id: @contract_subject.contract_type_id, descr: @contract_subject.descr, id: @contract_subject.id, is_close: @contract_subject.is_close, name: @contract_subject.name }
    end

    assert_redirected_to contract_subject_path(assigns(:contract_subject))
  end

  test "should show contract_subject" do
    get :show, id: @contract_subject
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @contract_subject
    assert_response :success
  end

  test "should update contract_subject" do
    put :update, id: @contract_subject, contract_subject: { contract_type_id: @contract_subject.contract_type_id, descr: @contract_subject.descr, id: @contract_subject.id, is_close: @contract_subject.is_close, name: @contract_subject.name }
    assert_redirected_to contract_subject_path(assigns(:contract_subject))
  end

  test "should destroy contract_subject" do
    assert_difference('ContractSubject.count', -1) do
      delete :destroy, id: @contract_subject
    end

    assert_redirected_to contract_subjects_path
  end
end
