# -*- encoding : utf-8 -*-
require 'test_helper'

class PhoneusesControllerTest < ActionController::TestCase
  setup do
    @phoneuse = phoneuses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:phoneuses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create phoneuse" do
    assert_difference('Phoneuse.count') do
      post :create, phoneuse: { id: @phoneuse.id, is_close: @phoneuse.is_close, is_corporate: @phoneuse.is_corporate, issue_date: @phoneuse.issue_date, person_id: @phoneuse.person_id, phone_id: @phoneuse.phone_id, take_date: @phoneuse.take_date }
    end

    assert_redirected_to phoneuse_path(assigns(:phoneuse))
  end

  test "should show phoneuse" do
    get :show, id: @phoneuse
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @phoneuse
    assert_response :success
  end

  test "should update phoneuse" do
    put :update, id: @phoneuse, phoneuse: { id: @phoneuse.id, is_close: @phoneuse.is_close, is_corporate: @phoneuse.is_corporate, issue_date: @phoneuse.issue_date, person_id: @phoneuse.person_id, phone_id: @phoneuse.phone_id, take_date: @phoneuse.take_date }
    assert_redirected_to phoneuse_path(assigns(:phoneuse))
  end

  test "should destroy phoneuse" do
    assert_difference('Phoneuse.count', -1) do
      delete :destroy, id: @phoneuse
    end

    assert_redirected_to phoneuses_path
  end
end
