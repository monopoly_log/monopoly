# -*- encoding : utf-8 -*-
require 'test_helper'

class TransporttypesControllerTest < ActionController::TestCase
  setup do
    @transporttype = transporttypes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:transporttypes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create transporttype" do
    assert_difference('Transporttype.count') do
      post :create, transporttype: { name: @transporttype.name }
    end

    assert_redirected_to transporttype_path(assigns(:transporttype))
  end

  test "should show transporttype" do
    get :show, id: @transporttype
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @transporttype
    assert_response :success
  end

  test "should update transporttype" do
    put :update, id: @transporttype, transporttype: { name: @transporttype.name }
    assert_redirected_to transporttype_path(assigns(:transporttype))
  end

  test "should destroy transporttype" do
    assert_difference('Transporttype.count', -1) do
      delete :destroy, id: @transporttype
    end

    assert_redirected_to transporttypes_path
  end
end
