# -*- encoding : utf-8 -*-
require 'test_helper'

class EnginemodelsControllerTest < ActionController::TestCase
  setup do
    @enginemodel = enginemodels(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:enginemodels)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create enginemodel" do
    assert_difference('Enginemodel.count') do
      post :create, enginemodel: { displacement: @enginemodel.displacement, enginetype_id: @enginemodel.enginetype_id, id: @enginemodel.id, name: @enginemodel.name, power: @enginemodel.power }
    end

    assert_redirected_to enginemodel_path(assigns(:enginemodel))
  end

  test "should show enginemodel" do
    get :show, id: @enginemodel
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @enginemodel
    assert_response :success
  end

  test "should update enginemodel" do
    put :update, id: @enginemodel, enginemodel: { displacement: @enginemodel.displacement, enginetype_id: @enginemodel.enginetype_id, id: @enginemodel.id, name: @enginemodel.name, power: @enginemodel.power }
    assert_redirected_to enginemodel_path(assigns(:enginemodel))
  end

  test "should destroy enginemodel" do
    assert_difference('Enginemodel.count', -1) do
      delete :destroy, id: @enginemodel
    end

    assert_redirected_to enginemodels_path
  end
end
