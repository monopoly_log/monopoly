# -*- encoding : utf-8 -*-
require 'test_helper'

class DocumentsControllerTest < ActionController::TestCase
  setup do
    @document = documents(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:documents)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create document" do
    assert_difference('Document.count') do
      post :create, document: { category: @document.category, contract_id: @document.contract_id, contractor_id: @document.contractor_id, documentkind_id: @document.documentkind_id, end_date: @document.end_date, id: @document.id, is_close: @document.is_close, issued_by: @document.issued_by, issued_date: @document.issued_date, number: @document.number, parent_id: @document.parent_id, registration: @document.registration, series: @document.series, start_date: @document.start_date, transport_id: @document.transport_id }
    end

    assert_redirected_to document_path(assigns(:document))
  end

  test "should show document" do
    get :show, id: @document
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @document
    assert_response :success
  end

  test "should update document" do
    put :update, id: @document, document: { category: @document.category, contract_id: @document.contract_id, contractor_id: @document.contractor_id, documentkind_id: @document.documentkind_id, end_date: @document.end_date, id: @document.id, is_close: @document.is_close, issued_by: @document.issued_by, issued_date: @document.issued_date, number: @document.number, parent_id: @document.parent_id, registration: @document.registration, series: @document.series, start_date: @document.start_date, transport_id: @document.transport_id }
    assert_redirected_to document_path(assigns(:document))
  end

  test "should destroy document" do
    assert_difference('Document.count', -1) do
      delete :destroy, id: @document
    end

    assert_redirected_to documents_path
  end
end
