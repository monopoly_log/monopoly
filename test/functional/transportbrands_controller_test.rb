# -*- encoding : utf-8 -*-
require 'test_helper'

class TransportbrandsControllerTest < ActionController::TestCase
  setup do
    @transportbrand = transportbrands(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:transportbrands)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create transportbrand" do
    assert_difference('Transportbrand.count') do
      post :create, transportbrand: { id: @transportbrand.id, name: @transportbrand.name }
    end

    assert_redirected_to transportbrand_path(assigns(:transportbrand))
  end

  test "should show transportbrand" do
    get :show, id: @transportbrand
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @transportbrand
    assert_response :success
  end

  test "should update transportbrand" do
    put :update, id: @transportbrand, transportbrand: { id: @transportbrand.id, name: @transportbrand.name }
    assert_redirected_to transportbrand_path(assigns(:transportbrand))
  end

  test "should destroy transportbrand" do
    assert_difference('Transportbrand.count', -1) do
      delete :destroy, id: @transportbrand
    end

    assert_redirected_to transportbrands_path
  end
end
