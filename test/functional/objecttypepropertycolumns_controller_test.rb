# -*- encoding : utf-8 -*-
require 'test_helper'

class ObjecttypepropertycolumnsControllerTest < ActionController::TestCase
  setup do
    @objecttypepropertycolumn = objecttypepropertycolumns(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:objecttypepropertycolumns)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create objecttypepropertycolumn" do
    assert_difference('Objecttypepropertycolumn.count') do
      post :create, objecttypepropertycolumn: { column_id: @objecttypepropertycolumn.column_id, end_date: @objecttypepropertycolumn.end_date, id: @objecttypepropertycolumn.id, is_close: @objecttypepropertycolumn.is_close, objecttypeproperty_id: @objecttypepropertycolumn.objecttypeproperty_id, start_date: @objecttypepropertycolumn.start_date }
    end

    assert_redirected_to objecttypepropertycolumn_path(assigns(:objecttypepropertycolumn))
  end

  test "should show objecttypepropertycolumn" do
    get :show, id: @objecttypepropertycolumn
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @objecttypepropertycolumn
    assert_response :success
  end

  test "should update objecttypepropertycolumn" do
    put :update, id: @objecttypepropertycolumn, objecttypepropertycolumn: { column_id: @objecttypepropertycolumn.column_id, end_date: @objecttypepropertycolumn.end_date, id: @objecttypepropertycolumn.id, is_close: @objecttypepropertycolumn.is_close, objecttypeproperty_id: @objecttypepropertycolumn.objecttypeproperty_id, start_date: @objecttypepropertycolumn.start_date }
    assert_redirected_to objecttypepropertycolumn_path(assigns(:objecttypepropertycolumn))
  end

  test "should destroy objecttypepropertycolumn" do
    assert_difference('Objecttypepropertycolumn.count', -1) do
      delete :destroy, id: @objecttypepropertycolumn
    end

    assert_redirected_to objecttypepropertycolumns_path
  end
end
