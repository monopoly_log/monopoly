# -*- encoding : utf-8 -*-
require 'test_helper'

class TestguidsControllerTest < ActionController::TestCase
  setup do
    @testguid = testguids(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:testguids)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create testguid" do
    assert_difference('Testguid.count') do
      post :create, testguid: { str: @testguid.str }
    end

    assert_redirected_to testguid_path(assigns(:testguid))
  end

  test "should show testguid" do
    get :show, id: @testguid
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @testguid
    assert_response :success
  end

  test "should update testguid" do
    put :update, id: @testguid, testguid: { str: @testguid.str }
    assert_redirected_to testguid_path(assigns(:testguid))
  end

  test "should destroy testguid" do
    assert_difference('Testguid.count', -1) do
      delete :destroy, id: @testguid
    end

    assert_redirected_to testguids_path
  end
end
