# -*- encoding : utf-8 -*-
require 'test_helper'

class ObjecttypesControllerTest < ActionController::TestCase
  setup do
    @objecttype = objecttypes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:objecttypes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create objecttype" do
    assert_difference('Objecttype.count') do
      post :create, objecttype: { descr: @objecttype.descr, end_date: @objecttype.end_date, id: @objecttype.id, is_close: @objecttype.is_close, name: @objecttype.name, start_date: @objecttype.start_date }
    end

    assert_redirected_to objecttype_path(assigns(:objecttype))
  end

  test "should show objecttype" do
    get :show, id: @objecttype
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @objecttype
    assert_response :success
  end

  test "should update objecttype" do
    put :update, id: @objecttype, objecttype: { descr: @objecttype.descr, end_date: @objecttype.end_date, id: @objecttype.id, is_close: @objecttype.is_close, name: @objecttype.name, start_date: @objecttype.start_date }
    assert_redirected_to objecttype_path(assigns(:objecttype))
  end

  test "should destroy objecttype" do
    assert_difference('Objecttype.count', -1) do
      delete :destroy, id: @objecttype
    end

    assert_redirected_to objecttypes_path
  end
end
