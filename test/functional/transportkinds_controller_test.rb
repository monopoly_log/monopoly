# -*- encoding : utf-8 -*-
require 'test_helper'

class TransportkindsControllerTest < ActionController::TestCase
  setup do
    @transportkind = transportkinds(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:transportkinds)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create transportkind" do
    assert_difference('Transportkind.count') do
      post :create, transportkind: { descr: @transportkind.descr, id: @transportkind.id, name: @transportkind.name, transporttype_id: @transportkind.transporttype_id }
    end

    assert_redirected_to transportkind_path(assigns(:transportkind))
  end

  test "should show transportkind" do
    get :show, id: @transportkind
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @transportkind
    assert_response :success
  end

  test "should update transportkind" do
    put :update, id: @transportkind, transportkind: { descr: @transportkind.descr, id: @transportkind.id, name: @transportkind.name, transporttype_id: @transportkind.transporttype_id }
    assert_redirected_to transportkind_path(assigns(:transportkind))
  end

  test "should destroy transportkind" do
    assert_difference('Transportkind.count', -1) do
      delete :destroy, id: @transportkind
    end

    assert_redirected_to transportkinds_path
  end
end
