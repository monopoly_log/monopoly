# -*- encoding : utf-8 -*-
require 'test_helper'

class TransportcolorsControllerTest < ActionController::TestCase
  setup do
    @transportcolor = transportcolors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:transportcolors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create transportcolor" do
    assert_difference('Transportcolor.count') do
      post :create, transportcolor: { name: @transportcolor.name }
    end

    assert_redirected_to transportcolor_path(assigns(:transportcolor))
  end

  test "should show transportcolor" do
    get :show, id: @transportcolor
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @transportcolor
    assert_response :success
  end

  test "should update transportcolor" do
    put :update, id: @transportcolor, transportcolor: { name: @transportcolor.name }
    assert_redirected_to transportcolor_path(assigns(:transportcolor))
  end

  test "should destroy transportcolor" do
    assert_difference('Transportcolor.count', -1) do
      delete :destroy, id: @transportcolor
    end

    assert_redirected_to transportcolors_path
  end
end
