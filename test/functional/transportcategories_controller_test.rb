# -*- encoding : utf-8 -*-
require 'test_helper'

class TransportcategoriesControllerTest < ActionController::TestCase
  setup do
    @transportcategory = transportcategories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:transportcategories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create transportcategory" do
    assert_difference('Transportcategory.count') do
      post :create, transportcategory: { guid: @transportcategory.guid, name: @transportcategory.name }
    end

    assert_redirected_to transportcategory_path(assigns(:transportcategory))
  end

  test "should show transportcategory" do
    get :show, id: @transportcategory
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @transportcategory
    assert_response :success
  end

  test "should update transportcategory" do
    put :update, id: @transportcategory, transportcategory: { guid: @transportcategory.guid, name: @transportcategory.name }
    assert_redirected_to transportcategory_path(assigns(:transportcategory))
  end

  test "should destroy transportcategory" do
    assert_difference('Transportcategory.count', -1) do
      delete :destroy, id: @transportcategory
    end

    assert_redirected_to transportcategories_path
  end
end
