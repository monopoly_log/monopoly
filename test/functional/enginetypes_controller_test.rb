# -*- encoding : utf-8 -*-
require 'test_helper'

class EnginetypesControllerTest < ActionController::TestCase
  setup do
    @enginetype = enginetypes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:enginetypes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create enginetype" do
    assert_difference('Enginetype.count') do
      post :create, enginetype: { id: @enginetype.id, name: @enginetype.name }
    end

    assert_redirected_to enginetype_path(assigns(:enginetype))
  end

  test "should show enginetype" do
    get :show, id: @enginetype
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @enginetype
    assert_response :success
  end

  test "should update enginetype" do
    put :update, id: @enginetype, enginetype: { id: @enginetype.id, name: @enginetype.name }
    assert_redirected_to enginetype_path(assigns(:enginetype))
  end

  test "should destroy enginetype" do
    assert_difference('Enginetype.count', -1) do
      delete :destroy, id: @enginetype
    end

    assert_redirected_to enginetypes_path
  end
end
