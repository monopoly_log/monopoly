# -*- encoding : utf-8 -*-
require 'test_helper'

class DocumentkindsControllerTest < ActionController::TestCase
  setup do
    @documentkind = documentkinds(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:documentkinds)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create documentkind" do
    assert_difference('Documentkind.count') do
      post :create, documentkind: { descr: @documentkind.descr, documenttype_id: @documentkind.documenttype_id, id: @documentkind.id, is_close: @documentkind.is_close, is_needed: @documentkind.is_needed, name: @documentkind.name }
    end

    assert_redirected_to documentkind_path(assigns(:documentkind))
  end

  test "should show documentkind" do
    get :show, id: @documentkind
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @documentkind
    assert_response :success
  end

  test "should update documentkind" do
    put :update, id: @documentkind, documentkind: { descr: @documentkind.descr, documenttype_id: @documentkind.documenttype_id, id: @documentkind.id, is_close: @documentkind.is_close, is_needed: @documentkind.is_needed, name: @documentkind.name }
    assert_redirected_to documentkind_path(assigns(:documentkind))
  end

  test "should destroy documentkind" do
    assert_difference('Documentkind.count', -1) do
      delete :destroy, id: @documentkind
    end

    assert_redirected_to documentkinds_path
  end
end
