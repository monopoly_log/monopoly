require 'test_helper'

class ContractSubjectSelectsControllerTest < ActionController::TestCase
  setup do
    @contract_subject_select = contract_subject_selects(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:contract_subject_selects)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create contract_subject_select" do
    assert_difference('ContractSubjectSelect.count') do
      post :create, contract_subject_select: { contract_subject_id: @contract_subject_select.contract_subject_id, dscr: @contract_subject_select.dscr, id: @contract_subject_select.id, is_close: @contract_subject_select.is_close, name: @contract_subject_select.name }
    end

    assert_redirected_to contract_subject_select_path(assigns(:contract_subject_select))
  end

  test "should show contract_subject_select" do
    get :show, id: @contract_subject_select
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @contract_subject_select
    assert_response :success
  end

  test "should update contract_subject_select" do
    put :update, id: @contract_subject_select, contract_subject_select: { contract_subject_id: @contract_subject_select.contract_subject_id, dscr: @contract_subject_select.dscr, id: @contract_subject_select.id, is_close: @contract_subject_select.is_close, name: @contract_subject_select.name }
    assert_redirected_to contract_subject_select_path(assigns(:contract_subject_select))
  end

  test "should destroy contract_subject_select" do
    assert_difference('ContractSubjectSelect.count', -1) do
      delete :destroy, id: @contract_subject_select
    end

    assert_redirected_to contract_subject_selects_path
  end
end
