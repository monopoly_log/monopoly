# -*- encoding : utf-8 -*-
Project1::Application.routes.draw do




  resources :contract_relationships


  resources :contracts


  resources :contract_subject_roles


  resources :contract_subject_selects


  resources :contract_subjects


  resources :contract_types


  resources :contractor_representatives


  resources :unv_comments


  resources :truck_drivers


  resources :couplers


  resources :unv_employees


  resources :resourse_types


  resources :unv_types


  resources :employee_histories


  resources :employees
  post "employees/index"

  resources :staff_units


  resources :departments do

  end


  get "unavailables/index"
  get "unavailables/ol"
  get "unavailables/upd"
  get "unavailables/connections"


  resources :duties


  get "wialon/index"

  resources :tables


  resources :columns


  resources :objecttypepropertycolumns


  resources :objecttypeproperties


  resources :objecttypes


  resources :systems


  resources :phoneuses


  resources :phones


  resources :positions


  resources :contractortypes


  resources :contractors


  resources :documents do
       collection do
         get 'show_part'
         post 'select_kind'
         get 'select_kind'
         get 'edit'
       end
  end


  resources :documentkinds


  resources :documenttypes


  resources :functionalgroups do
       collection do
         get 'set_order'
         post 'savesort'
         get 'savesort'
         get 'update_menu'
       end
  end


  resources :people do
     collection do
      get 'phone_person'
     end
  end


  resources :transportexploitations


  resources :transporttanks


  resources :transportloadings


  resources :transportecologicals


  resources :enginemodels


  resources :enginetypes


  resources :transportkinds


  resources :transportcolors


  resources :transporttypes


  resources :transports



  get "sms/index"

  resources :testguids, as: 'tst'


  #resources :temps


  devise_for :admins

  #resources :functionals


  #resources :usergroups


  #resources :roles
  #resources :user2

  #devise_for :users

  get "main/index"

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
   root :to => 'main#index'
   match '/' => 'main#index'
   
   #match '/users/sign_in.user' => 'main#index'
   #match '/users/sign_in.user' => redirect('/')
   
#devise_for :users
#as :user do
#  get '/users/edit' => 'devise/registrations#edit', :as => 'edit_user_registration'
#  get '/users/new' => 'devise/registrations#new', :as => 'new_user_registration'
#  put '/users' => 'devise/registrations#update', :as => 'user_registration'
#
#end

devise_for :users, :skip => [:sessions]
as :user do
  get 'signin' => 'devise/sessions#new', :as => :new_user_session
  post 'signin' => 'devise/sessions#create', :as => :user_session
  delete 'signout' => 'devise/sessions#destroy', :as => :destroy_user_session
end

scope "/admin" do
  resources :users, :usergroups, :roles, :functionals
end

scope "/transport" do
  resources :transportbrands
  resources :transportmodels
  resources  :transportcategories
end


  # See how all your routes lay out with "rake routes"
match ':controller(/:action(/:id))(.:format)'
  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  
end
