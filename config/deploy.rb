# -*- encoding : utf-8 -*-

#set :default_stage, "development"
# set :stages, %w(production)



# bundler

require 'bundler/capistrano'

ssh_options[:forward_agent] = true


set :application, "monopoly"
set :repository,  "git@bitbucket.org:monopoly_log/monopoly.git" # Адрес твоего репозитория
set :scm, :git # Твоя система контроля версий
set :rails_env, 'development'
set :branch, 'master'
set :deploy_to, "/var/www/ror/monopoly" # место, куда будет развёрнуто приложение
set :use_sudo, false # в нашем случае, не надо использовать
set :rails_env, "development"

# rvm/capistrano
require 'rvm/capistrano'
set :rvm_ruby_string, 'ruby-1.9.3-p484'
set :rvm_type, :user


# set :scm, :git # You can set :scm explicitly or Capistrano will make an intelligent guess based on known version control directory names
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`


server '192.168.0.15', :web, :app, :db, :primary => true
set :user, 'ror'

# if you want to clean up old releases on each deploy uncomment this:
after "deploy:restart", "deploy:cleanup"
before 'deploy:assets:precompile', 'deploy:symlink_db'


# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
namespace :deploy do
  desc "Restarting the app"
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end

  desc "Symlinks the database.yml"
  task :symlink_db, :roles => :app do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  end
end
