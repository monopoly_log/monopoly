# -*- encoding : utf-8 -*-
require 'net/ldap'
require 'devise/strategies/authenticatable'

module Devise
  module Strategies
    class LdapAuthenticatable < Authenticatable
      def authenticate!
        if params[:user]
          params[:user][:email] = email
          if ldap_init.bind
            @user = User.where('email = ?', email).first
            unless @user
              @user2 = User.create(user_data)
              #@user = User.where('email = ?', email).first
              @role = Role.find('cbfcb992-0685-4530-ae3f-179263d9935d'.upcase)
              @user2.roles << @role
              File.open('log/report.ldap', 'a'){ |file| file.write @role.id; file.write "\n" }
              success!(@user2)
            else
              success!(@user)
            end
            File.open('log/report.ldap', 'a'){ |file| file.write Time.now; file.write email; file.write " - "; file.write login;  file.write "\n" }
          else
            fail(:invalid_login)
          end
        end
      end

      def ldap_init
        @host = 'cd.Monopoly'
        @port = 389
        @base = 'DC=monopoly'
        @domain = 'monopoly.su'
        @login = login
        @password = password
        ldap = Net::LDAP.new
          ldap.host = @host
          ldap.port = @port
          ldap.auth "MONOPOLY\\#{@login}", @password
        ldap
      end

      def email
        (params[:user][:email].index('@monopoly.su') ? params[:user][:email] : params[:user][:email] + '@monopoly.su').force_encoding(Encoding::UTF_8)
      end

      def login
        (params[:user][:email].index('@monopoly.su') ? params[:user][:email][0..params[:user][:email].index('@monopoly.su')-1] : params[:user][:email]).force_encoding(Encoding::UTF_8)
      end

      def password
        params[:user][:password]
      end

      def user_data
        name = ldap_search.givenname.first.force_encoding(Encoding::UTF_8)
        last_name = ldap_search.sn.first.force_encoding(Encoding::UTF_8)
        {email: email, password: password, password_confirmation: password, name: name, last_name: last_name}
      end

      def ldap_search
        ldap_init.search(
        base:        "DC=monopoly",
        #filter:       Net::LDAP::Filter.eq( "mail", 'mihail.rogov@monopoly.su' ),
        filter:      "samaccountname=#{login}",
        return_result: true
        ).first
      end

    end
  end
end

Warden::Strategies.add(:ldap_authenticatable, Devise::Strategies::LdapAuthenticatable)
