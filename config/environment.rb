# -*- encoding : utf-8 -*-
# Load the rails application
require File.expand_path('../application', __FILE__)

my_date_formats = { :default => '%d/%m/%Y' }
my_time_formats = { :default => '%d/%m/%Y %H:%M' }
Time::DATE_FORMATS.merge!(my_time_formats)
Date::DATE_FORMATS.merge!(my_date_formats)
Encoding.default_external = Encoding::UTF_8
Encoding.default_internal = Encoding::UTF_8

# Initialize the rails application
Project1::Application.initialize!
